
int Pin0 = 11;
int Pin1 = 10;
int Pin2 = 12;
int Pin3 = 8; // formerly pin13 which is used as status light


boolean in = false;
boolean fullstep = false;
boolean dir = true;// gre
int _step = 0;
int potPin = 2;
int delayval = 1;
float stepPitch = 1.8;
float cycleAngle = 360;
float cycleSteps = cycleAngle / stepPitch;
float i = 0;

bool State = true;
void setup()
{
  pinMode(13, OUTPUT);
  pinMode(Pin0, OUTPUT);
  pinMode(Pin1, OUTPUT);
  pinMode(Pin2, OUTPUT);
  pinMode(Pin3, OUTPUT);
  pinMode(potPin, INPUT);
  Serial.begin(9600);
}


void loop()
{
  cycleSteps = fullstep?(cycleAngle / stepPitch):(2*cycleAngle / stepPitch);

  State = !State;
  digitalWrite(13, State);
//  Serial.print(cycleSteps);
//  Serial.print(":");
//  Serial.print(i);
//  Serial.print("::");
//  Serial.println(_step);

  switch (_step) {
    case 0:
      digitalWrite(Pin0, LOW);
      digitalWrite(Pin1, LOW);
      digitalWrite(Pin2, LOW);
      digitalWrite(Pin3, HIGH);

      break;
    case 1:
      digitalWrite(Pin0, LOW);
      digitalWrite(Pin1, LOW);
      digitalWrite(Pin2, HIGH);
      digitalWrite(Pin3, HIGH);
      break;
    case 2:
      digitalWrite(Pin0, LOW);
      digitalWrite(Pin1, LOW);
      digitalWrite(Pin2, HIGH);
      digitalWrite(Pin3, LOW);
      break;
    case 3:
      digitalWrite(Pin0, LOW);
      digitalWrite(Pin1, HIGH);
      digitalWrite(Pin2, HIGH);
      digitalWrite(Pin3, LOW);
      break;
    case 4:
      digitalWrite(Pin0, LOW);
      digitalWrite(Pin1, HIGH);
      digitalWrite(Pin2, LOW);
      digitalWrite(Pin3, LOW);
      break;
    case 5:
      digitalWrite(Pin0, HIGH);
      digitalWrite(Pin1, HIGH);
      digitalWrite(Pin2, LOW);
      digitalWrite(Pin3, LOW);
      break;
    case 6:
      digitalWrite(Pin0, HIGH);
      digitalWrite(Pin1, LOW);
      digitalWrite(Pin2, LOW);
      digitalWrite(Pin3, LOW);
      break;
    case 7:
      digitalWrite(Pin0, HIGH);
      digitalWrite(Pin1, LOW);
      digitalWrite(Pin2, LOW);
      digitalWrite(Pin3, HIGH);
      break;
    default:
      digitalWrite(Pin0, LOW);
      digitalWrite(Pin1, LOW);
      digitalWrite(Pin2, LOW);
      digitalWrite(Pin3, LOW);
      break;
  }


  if ((i <= cycleSteps) && (dir == true))
  {
    _step++;
    if (fullstep)
      _step++;
    i++;
  }
  else {
    dir = false;
    _step--;
    if (fullstep)
      _step--;
    i--;
  }
  if (i == 0)
    dir = true;


  if (_step > 7) {
    _step = 0;
  }
  if (_step < 0) {
    _step = 7;
  }

//  if (in)
//  {
//    delayval = analogRead(potPin);
//  }
//  else
//  { if (Serial.available()) {
//      /* read the most recent byte */
//      delayval = Serial.read();
//      // Serial.write(delayval);
//    }
//  }
  delay(2);
}

