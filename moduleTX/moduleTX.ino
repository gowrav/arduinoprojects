
#define TRIGGER_PIN  11  // Arduino pin tied to trigger pin on the ultrasonic sensor.
#define ECHO_PIN     12  // Arduino pin tied to echo pin on the ultrasonic sensor.
#define MAX_DISTANCE 65 // Maximum distance we want to ping for (in centimeters). Maximum sensor distance is rated at 400-500cm.
int dOut_in = 4;
int dOut_out = 5;
int laser_1 = 8;
int laser_2 = 9;

int delayonIntercept = 800;

int avgStrike = 5;
int sonarDist = 0;
int checkClearanceDelayRepeat = 10;

boolean triggerEnabled = false;

long incount; // <-
long outcount;//  ->
long inside; // In - Out
long totalInOut; // In + Out

String DeviceName = "C4";

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);

  initSonar();

  pinMode(laser_1, OUTPUT);
  pinMode(laser_2, OUTPUT);

  pinMode(dOut_in, INPUT);
  pinMode(dOut_out, INPUT);

  digitalWrite(laser_1, HIGH);
  digitalWrite(laser_2, HIGH);

  incount = 0;
  outcount = 0;
  inside = 0;
  totalInOut = 0;
  triggerEnabled = true;

  Serial.println("Init"); // Send ping, get distance in cm and print result (0 = outside set distance range)
  delay(500);
  //  Serial.println("LetsBegin"); // Send ping, get distance in cm and print result (0 = outside set distance range)

}


void loop() {

  while ((digitalRead(dOut_in) && digitalRead(dOut_out)))
  { digitalWrite(laser_1, HIGH);
    digitalWrite(laser_2, HIGH);
  }
  {
    //delayMicroseconds(100);//check for bounce and confirm
    int inTrig = digitalRead(dOut_in);
    int outTrig = digitalRead(dOut_out);
    if (!(inTrig && outTrig))
    {
      //Serial.println("TrappedIN");
      if (timeTrapper())
      {
        //Serial.println("TrappedOUT");
        if (!inTrig)
        {
          incount++;
          sendData(1, 0);
        }
        else
        {
          outcount++;
          sendData(0, 1);
        }
      }
      else
      {
        //          Serial.println("unTrappedOUT-nochange!");
        //while(getSonarDistance() < (0.5*MAX_DISTANCE));
      }

      digitalWrite(laser_1, HIGH);
      digitalWrite(laser_2, HIGH);
      delay(delayonIntercept / 2);

      //      Serial.print("IN,OUT,TOTAL: "); // Send ping, get distance in cm and print result (0 = outside set distance range)
      //      Serial.print(incoming); // Send ping, get distance in cm and print result (0 = outside set distance range)
      //      Serial.print(','); // Send ping, get distance in cm and print result (0 = outside set distance range)
      //      Serial.print(outgoing); // Send ping, get distance in cm and print result (0 = outside set distance range)
      //      Serial.print(','); // Send ping, get distance in cm and print result (0 = outside set distance range)
      //      Serial.println(incoming - outgoing); // Send ping, get distance in cm and print result (0 = outside set distance range)
    }
  }

  delay(2);
}

bool timeTrapper()
{
  digitalWrite(laser_1, LOW);
  digitalWrite(laser_2, LOW);
  // get current time stamp
  // only need one for both if-statements
  unsigned long currentMillis = millis();
  boolean sonarClear = false;
  do
  {
    int curStrike = 0;
    sonarDist = 0;
    while (curStrike < avgStrike)
    {
      int temp = getSonarDistance();
      sonarDist += temp;
      curStrike++;
      delay(5);//check for bounce and confirm
    }
    sonarDist = sonarDist / avgStrike;
    if (sonarDist > (0.75 * MAX_DISTANCE) )
      sonarClear = true;
    else
      sonarClear = false;

    if (millis() >= (currentMillis + (delayonIntercept * checkClearanceDelayRepeat)))
      return false;
  }
  while ((millis() < (currentMillis + delayonIntercept)) || !sonarClear);

  return true;

}

void initSonar() {
  pinMode(TRIGGER_PIN, OUTPUT); // Sets the trigPin as an Output
  pinMode(ECHO_PIN, INPUT); // Sets the echoPin as an Input
}

int getSonarDistance()
{

  long duration;
  int distance;
  // Clears the trigPin
  digitalWrite(TRIGGER_PIN, LOW);
  delayMicroseconds(2);

  // Sets the trigPin on HIGH state for 10 micro seconds
  digitalWrite(TRIGGER_PIN, HIGH);
  delayMicroseconds(10);
  digitalWrite(TRIGGER_PIN, LOW);

  // Reads the echoPin, returns the sound wave travel time in microseconds
  duration = pulseIn(ECHO_PIN, HIGH);

  // Calculating the distance
  distance = duration * 0.034 / 2;

  // Prints the distance on the Serial Monitor
  //Serial.print("Distance: ");
  //Serial.println(distance);

  return distance;
}


void sendData(long incoming, long outgoing)
{
  inside = incount - outcount;
  totalInOut = incount + outcount;
  String data = "{";
  data.concat(DeviceName);
  data.concat(",");
  data.concat(incoming);
  data.concat(",");
  data.concat(outgoing);
  data.concat(",");
  data.concat(incount);
  data.concat(",");
  data.concat(outcount);
  data.concat(",");
  data.concat(inside);
  data.concat(",");
  data.concat(totalInOut);
  data.concat("}");

  char* buf1;
  data.toCharArray(buf1, data.length() + 1);
  Serial.println(data);
  Serial.write(buf1);
}



