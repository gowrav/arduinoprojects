#include <SPI.h>
// pocsag message generation library
#include <Pocsag.h>
#include "si5351.h"
#include "Wire.h"

Si5351 si5351;

Pocsag pocsag;

// frequency
//unsigned long freq = 1500000000ULL;
unsigned long freq = 6960000000ULL;
unsigned long freq_deviation = 4000ULL;//20000ULL;
//unsigned long freq_deviation = 6000ULL;


// timer setup for timer0, timer1, and timer2.
// For arduino uno or any board with ATMEL 328/168.. diecimila, duemilanove, lilypad, nano, mini...

int state; // state machine for CLI input

char bell = 0x07;// bell = ascii 0x07

// data
long int address;
int addresssource;
int repeat;
char textmsg[42]; // to store text message;
int msgsize;

long pocsag_total_bits = 0;
long pocsag_current_pos = 0;
long pocsag_prev_pos = -1;

long pocsag_total_repeat = 0;
long pocsag_current_repeat = 0;

boolean bit_toggle = 0;
boolean prev_bit_toggle = 0;

void setup() {

  // Start serial and initialize the Si5351
  Wire.begin();
  Wire.setClock(400000L);
  Serial.begin(115200);

  Serial.println(F("Init SI5351"));
  si5351.init(SI5351_CRYSTAL_LOAD_6PF, 0, 0);

  // Set CLK0 to output freq MHz
  si5351.set_freq(freq, SI5351_CLK0);
  si5351.output_enable(SI5351_CLK0, 0);

  si5351.update_status();
  delay(500);

  // Read the Status Register and print it every 10 seconds
  si5351.update_status();
  Serial.print(F("SYS_INIT: "));
  Serial.print(si5351.dev_status.SYS_INIT);
  Serial.print(F("  LOL_A: "));
  Serial.print(si5351.dev_status.LOL_A);
  Serial.print(F("  LOL_B: "));
  Serial.print(si5351.dev_status.LOL_B);
  Serial.print(F("  LOS: "));
  Serial.print(si5351.dev_status.LOS);
  Serial.print(F("  REVID: "));
  Serial.println(si5351.dev_status.REVID);

  pinMode(13, OUTPUT);

  state = -999;
  address = 0;
  addresssource = 0;
  msgsize = 0;


}


ISR(TIMER1_COMPA_vect) {
  if (pocsag_total_bits > 0 && (pocsag_current_pos != pocsag_total_bits)) {
    if (bitRead(((uint8_t *)pocsag.GetMsgPointer())[pocsag_current_pos / 8], 7 - (pocsag_current_pos % 8)))
    {
      //si5351.set_freq(freq, SI5351_CLK0);
      //      digitalWrite(13, LOW);
      bit_toggle = 1;
    }
    else
    {
      //si5351.set_freq(freq + freq_deviation, SI5351_CLK0);
      //      digitalWrite(13, HIGH);
      bit_toggle = 0;
    }
    pocsag_current_pos++;
  }
}



void loop() {

  while (state >= 0) {
    char c;
    // loop until we get a character from serial input
    while (!Serial.available()) {
    }; // end while

    c = Serial.read();

    // break out on ESC
    if (c == 0x1b) {
      state = -999;
      break;
    }; // end if


    // state machine
    if (state == 0) {
      // state 0: wait for command
      // P = PAGE
      // F = FREQUENCY

      if ((c == 'p') || (c == 'P')) {
        // ok, got our "p" -> go to state 1
        state = 1;

        // echo back char
        Serial.write(c);
      } else if ((c == 'f') || (c == 'F')) {
        // ok, got our "f" -> go to state 1
        state = 10;

        // echo back char
        Serial.write(c);
      } else {
        // error: echo "bell"
        Serial.write(bell);
      }; // end else - if

      // get next char
      continue;
    }; // end state 0

    // state 1: space (" ") or first digit of address ("0" to "9")
    if (state == 1) {
      if (c == ' ') {
        // space -> go to state 2 and get next char
        state = 2;

        // echo back char
        Serial.write(c);

        // get next char
        continue;
      } else if ((c >= '0') && (c <= '9')) {
        // digit -> first digit of address. Go to state 2 and process
        state = 2;

        // continue to state 2 without fetching next char
      } else {
        // error: echo "bell"
        Serial.write(bell);

        // get next char
        continue;
      }; // end else - if
    };  // end state 1

    // state 2: address ("0" to "9")
    if (state == 2) {
      if ((c >= '0') && (c <= '9')) {
        long int newaddress;

        newaddress = address * 10 + (c - '0');

        if (newaddress <= 0x1FFFFF) {
          // valid address
          address = newaddress;

          Serial.write(c);
        } else {
          // address to high. Send "beep"
          Serial.write(bell);
        }; // end else - if

      } else if (c == ' ') {
        // received space, go to next field (address source)
        Serial.write(c);
        state = 3;
      } else {
        // error: echo "bell"
        Serial.write(bell);
      }; // end else - elsif - if

      // get next char
      continue;
    }; // end state 2

    // state 3: address source: one single digit from 0 to 3
    if (state == 3) {
      if ((c >= '0') && (c <= '3')) {
        addresssource = c - '0';
        Serial.write(c);

        state = 4;
      } else {
        // invalid: sound bell
        Serial.write(bell);
      }; // end if

      // get next char
      continue;
    }; // end state 3


    // state 4: space between source and repeat
    if (state == 4) {
      if (c == ' ') {
        Serial.write(c);

        state = 6; // go from state 4 to state 6
        // (No state 5, callsign removed)
      } else {
        // invalid: sound bell
        Serial.write(bell);
      }; // end if

      // get next char
      continue;
    }; // end state 4


    // state 5: callsign: REMOVED in non-ham version

    // state 6: repeat: 1-digit value between 0 and 9
    if (state == 6) {
      if ((c >= '0') && (c <= '9')) {
        Serial.write(c);
        repeat = c - '0';

        // move to next state
        state = 7;
      } else {
        Serial.write(bell);
      }; // end if

      // get next char
      continue;
    }; // end state 6


    // state 7: space between repeat and message
    if (state == 7) {
      if (c == ' ') {
        Serial.write(c);

        // move to next state
        state = 8;
      } else {
        // invalid char
        Serial.write(bell);
      }; // end else - if

      // get next char
      continue;
    }; // end state 7


    // state 8: message, up to 40 chars, terminate with cr (0x0d) or lf (0x0a)
    if (state == 8) {
      // accepted is everything between space (ascii 0x20) and ~ (ascii 0x7e)
      if ((c >= 0x20) && (c <= 0x7e)) {
        // accept up to 40 chars
        if (msgsize < 40) {
          Serial.write(c);

          textmsg[msgsize] = c;
          msgsize++;
        } else {
          // to long
          Serial.write(bell);
        }; // end else - if

      } else if ((c == 0x0a) || (c == 0x0d)) {
        // done

        Serial.println("");

        // add terminating NULL
        textmsg[msgsize] = 0x00;

        // break out of loop
        state = -1;
        break;

      } else {
        // invalid char
        Serial.write(bell);
      }; // end else - elsif - if

      // get next char
      continue;
    }; // end state 8;


  }; // end while


  // Function "P": Send PAGE
  if (state == -1) {
    if ((pocsag_total_bits > pocsag_current_pos)) {
      if (pocsag_prev_pos < pocsag_current_pos) {
        if (bit_toggle)
        {
          //digitalWrite(13, HIGH);
          //si5351.set_freq(freq, SI5351_CLK0);
          Serial.print(1);
        } else {
          //digitalWrite(13, LOW);
          //si5351.set_freq(freq + freq_deviation, SI5351_CLK0);
          Serial.print(0);
        }
        pocsag_prev_pos = pocsag_current_pos;
      }
      //Serial.print(bit_toggle);

      if (pocsag_total_bits == pocsag_current_pos)
      {
        //TIMSK1 = 0;// stop timer interrupt 1 and TIMSK1 = (1<<OCIE1A)
        state = -999;
        si5351.output_enable(SI5351_CLK0, 0);
        Serial.println();
      }

    } else {

      Serial.print(F("address: "));
      Serial.println(address);

      Serial.print(F("addresssource: "));
      Serial.println(addresssource);

      Serial.print(F("repeat: "));
      Serial.println(repeat);

      Serial.print(F("message: "));
      Serial.println(textmsg);

      // create pocsag message
      // batch2 option = 0 (truncate message)
      // invert option1 (invert)
      int rc = pocsag.CreatePocsag(address, addresssource, textmsg, 0, 0);

      if (!rc) {
        Serial.print(F("Error in createpocsag! Error: "));
        Serial.println(pocsag.GetError());
        // sleep 10 seconds // now 3
        delay(100);
      } else {
        si5351.output_enable(SI5351_CLK0, 1);

        cli();//stop interrupts
        //set timer1 interrupt at 1Hz
        TCCR1A = 0;// set entire TCCR1A register to 0
        TCCR1B = 0;// same for TCCR1B
        TCNT1  = 0;//initialize counter value to 0

        //  OCR1A = 15624;// = (16*10^6) / (1*1024) - 1 (must be <65536) // set compare match register for 1hz increments
        //  OCR1A = 7811;// = (16*10^6) / (2*1024) - 1 (must be <65536)
        //  OCR1A = 1561;// = (16*10^6) / (10*1024) - 1 (must be <65536)
        //  OCR1A = 311;// = (16*10^6) / (50*1024) - 1 (must be <65536)
        //  OCR1A = 155;// = (16*10^6) / (100*1024) - 1 (must be <65536)
        OCR1A = 29;// = (16*10^6) / (512*1024) - 1 (must be <65536)
        //        OCR1A = 14;// = (16*10^6) / (1200*1024) - 1 (must be <65536)

        // turn on CTC mode
        TCCR1B |= (1 << WGM12);
        // Set CS12 and CS10 bits for 1024 prescaler
        TCCR1B |= (1 << CS12) | (1 << CS10);
        // enable timer compare interrupt
        TIMSK1 |= (1 << OCIE1A);

        // Message Length
        pocsag_total_bits = pocsag.GetSize() * 8;
        pocsag_current_pos = 0;

        sei();// allow interrupts
      }
    }
  } else {
    state = 0;
    pocsag_total_bits = pocsag_current_pos = 0;
    pocsag_prev_pos = -1;
    // reset var
    address = 0;
    addresssource = 0;
    msgsize = 0;

    Serial.println(F("Format:"));
    Serial.println(F("P <address> <source> <repeat> <message>"));
    Serial.println(F("F <freqmhz> <freq100Hz>"));

  }

}



