// Date and time functions using a DS1307 RTC connected via I2C and Wire lib
#include "Arduino.h"
#include "debugMonitor.h"
#include "RTClib.h"
#include "rtc_manager.h"
#include "TimeLib.h"
#include "TimeAlarms.h"
#include "Wire.h"

RTC_DS1307 rtc;

//char daysOfTheWeek[7][12] = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};

void init_rtc() {

  Wire.begin();
  
  if (! rtc.begin()) {
    dbgSerial.println("Couldn't find RTC");
    while (1);
  }
  
  if (!rtc.isrunning()) {
    dbgSerial.println("RTC is NOT running!");
    // following line sets the RTC to the date & time this sketch was compiled
    rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
    // This line sets the RTC with an explicit date & time, for example to set
    // January 21, 2014 at 3am you would call:
    // rtc.adjust(DateTime(2014, 1, 21, 3, 0, 0));
  }
  
}


void printCurrentDateTime()
{
  DateTime now = rtc.now();
  dbgSerial.print(now.year(), DEC); dbgSerial.print('/'); dbgSerial.print(now.month(), DEC); dbgSerial.print('/'); dbgSerial.print(now.day(), DEC);
  dbgSerial.print(' '); dbgSerial.print(now.hour(), DEC); dbgSerial.print(':'); dbgSerial.print(now.minute(), DEC); dbgSerial.print(':'); dbgSerial.println(now.second(), DEC);
}

String getCurrentDateTimeString()
{
  DateTime now = rtc.now();
  return String(now.year()) + "/" + String(now.month()) +  "/"  + String(now.day()) +  ">"  + String(now.hour()) +  ":"  + String(now.minute()) +  "/"  + String(now.second());
}


