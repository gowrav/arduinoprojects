#ifndef HEADER_GPRSCOMM
#define HEADER_GPRSCOMM
#include "TinyGPS++.h" // Include the TinyGPS++ library

#define _gpsFIX_DEFAULT F("DEFAULT")  // Manually Set
#define _gpsFIX_2D F("2D")  // Set by 2D
#define _gpsFIX_3D F("3D")  // Set by 3D

#define _gprsRXPIN 10  // Pin used as UARTRX  10, 11 for software serial
#define _gprsTXPIN 11  // Pin used as UARTTX
#define _gprsBAUD 9600  // Baud Rate

void initGPRS(void);
boolean postUpdate(String body);
void onGPS(void);
void offGPS(void);
TinyGPSPlus getGPSVal(void);
String getGPSSource(void);
void sendGPRSCommand(String cmdstr,long delaymillisec);
void sendGPRSCommandCont(String cmdstr,long delaymillisec);
void ShowGPRSSerialData();

#endif
