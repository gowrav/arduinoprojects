#include "Arduino.h"
#include "gas_monitor.h"


byte _gassensorPin;  // Pin used as trigPin
double _threshold;  // _threshold for Smoke Alarm
boolean _GasMonAnalog;
boolean _GasMoninitialized;

//<<constructor>> setup the GasMon
GasMon::GasMon() {
  // Construction Parameter here
  _GasMoninitialized = false;
  _GasMonAnalog = false;
}

//<<destructor>>
GasMon::~GasMon() {
  /*nothing to destruct*/
  _threshold = 512;// arbitrary median
}

//<<init>> setup the GasMon as Analog reader
boolean GasMon::initGasMonAnalog(int sensorPin) {
  // Construction Parameter here
  _gassensorPin = sensorPin;
  digitalWrite(_gassensorPin, INPUT_PULLUP);  // set pullup and _sensorPin as Analog Input
  _GasMoninitialized = true;
  _GasMonAnalog = true;
  return _GasMoninitialized;
}


//<<init>> setup the GasMon as Digital reader
boolean GasMon::initGasMonDigital(int sensorPin) {
  // Construction Parameter here
  _gassensorPin = sensorPin;
  pinMode(_gassensorPin, INPUT);  // set _sensorPin as Digital Input
  _GasMoninitialized = true;
  _GasMonAnalog = false;
  return _GasMoninitialized;
}

//<<set Threshold>> setup the BatMon Threshold
boolean GasMon::setThreshold(double threshold) {
  _threshold = threshold;
}

//<<read Value>> read the GasMon digital state
boolean GasMon::readState() {
  if (!_GasMonAnalog && _GasMoninitialized)
    return digitalRead(_gassensorPin);
  else if (_GasMonAnalog && _GasMoninitialized) {
    return (analogRead(_gassensorPin) > _threshold ? true : false);
  }
}

//<<read Value>> read the GasMon analog Value
double GasMon::readValue() {
  if (_GasMonAnalog && _GasMoninitialized)
    return analogRead(_gassensorPin);
}

