/*
  R2Promise IOT node compatible with following products.
    BinPactor - Smart Bin which can compact the waste at source.
    BinNect - Smart Bin Communication Device which makes any normal bin smart.

  This IOT end node contains hardware driven data acquisition modules to update
  procured data periodically to the preffered server

  The circuit:https://create.arduino.cc/
  BinNect:
    Core Processor : Arduino Nano / ESP12E
    Connectivity:
      GSM   : SIMCOM808 - Not Used
      GPRS  : SIMCOM808 GPRS class 12/10
      WIFI  : inbuilt(ESP12E)
    GeoLocation
      GPS   : SIMCOM808 GPS L1 C/A code
    Sensors:
      Dist  : Sonar US020 (Not rugged enough) / Vehicle Sonars / TOF VL53L0X (Expensive)
      Temp  : SIMCOM808 / inbuilt(ESP12E) - NotUsed
      Volt  : ADC with Resistance Devider
      GAS   : MQ2
      Smoke : MQ2
    Time:
      RTC   : DS1307
    Indicators:
      Power : RED (AlwaysOn)
      BAT   : RED (On Low-Battery)
      TX    : GREEN (On Transmission Active)
      FILL  : PWM GREEN (BIN Level)
      DISP  : LCD / OLED (Not Used)
    Controls:
      Power : push / toggle
      Reset : momentary
      GPS   : momentary (Update GPS)

  BinPactor:
    Core Processor : Arduino Mega / ESP32
    Connectivity:
      GSM   : SIMCOM808 - Not Used
      GPRS  : SIMCOM808 GPRS class 12/10
      WIFI  : inbuilt(ESP12E)
      BT    : SIMCOM808 / inbuilt(ESP12E) - NotUsed
      LORA  : -
      RF    : -
      IR    : -
    GeoLocation
      GPS   : SIMCOM808 GPS L1 C/A code
    Sensors:
      Dist  : Sonar US020 (Not rugged enough) / Vehicle Sonars / TOF VL53L0X (Expensive)
      Temp  : SIMCOM808 / inbuilt(ESP12E) - NotUsed
      Volt  : ADC with Resistance Devider
      Amp   : -
      GAS   : MQ2
      Smoke : MQ2
      Motion: SR501
      Weight: HX711
      RF    : REES52 / MFRC522
    Time:
      RTC   : DS1307
    Actuators:
      Relay : 2Ch Relay
      MOSFET: IRLZ44 / IRF540
      Motor : Hbridge / Pololulu / RelayDriven
    Indicators:
      Power : RED (AlwaysOn)
      BAT   : RED (On Low-Battery)
      TX    : GREEN (On Transmission Active)
      FILL  : PWM GREEN (BIN Level)
      DISP  : LCD / OLED (Not Used)
    Controls:
      Power : push / toggle
      Reset : momentary
      GPS   : momentary (Update GPS)

    list the components attached to each input
    list the components attached to each output

  Typical pin layout used for RFID:
  -----------------------------------------------------------------------------------------
               MFRC522      Arduino       Arduino   Arduino    Arduino          Arduino
               Reader/PCD   Uno           Mega      Nano v3    Leonardo/Micro   Pro Micro
   Signal      Pin          Pin           Pin       Pin        Pin              Pin
   -----------------------------------------------------------------------------------------
   RST/Reset   RST          9             5         D9         RESET/ICSP-5     RST
   SPI SS      SDA(SS)      10            53        D10        10               10
   SPI MOSI    MOSI         11 / ICSP-4   51        D11        ICSP-4           16
   SPI MISO    MISO         12 / ICSP-1   50        D12        ICSP-1           14
   SPI SCK     SCK          13 / ICSP-3   52        D13        ICSP-3           15
   IRQ pin is interrupt

  Created 26 July 2017
  By GeekSynergy (http://geeksynergy.com)
  Modified 26 July 2017
  By GeekSynergy (http://geeksynergy.com)

  http://r2promise.com

*/

#include "debugMonitor.h"
#include "rtc_manager.h"
#include "sonar_scan.h"
#include "bat_monitor.h"
#include "gas_monitor.h"
#include "gprs_comm.h"

#include "TimeLib.h"
#include "TimeAlarms.h"
#include "HX711.h"
#include "SPI.h"
#include "MFRC522.h"

#define SS_PIN 53    //10 // Configurable, see typical pin layout above for RFID
#define RST_PIN 5    //9  // Configurable, see typical pin layout above for RFID
int IRQ_PIN = 3;    //  Interrupt pin for the RFID
MFRC522 mfrc522(SS_PIN, RST_PIN);   // Create MFRC522 instance.
byte regVal = 0xA0; // 0x7F; rx irq, interrupt pin enable value for Rc522

AlarmId interval_alarm_id;
AlarmId sonar_update_id;
AlarmId remote_update_id;

Sonar leftSonar;
Sonar rightSonar;

BatMon batA;
GasMon gasA;
int motionSen = 2;  // Motion sensor
HX711 scale(14, 15);  //HX711 scale(DOUT, CLK); 23,22 was used, 14,15 in Mega
int frwMotorCtrl = 22; //Motor forward ditection control
int revMotorCtrl = 23; //motor reverse ditection control
boolean updating;
boolean compressing;
boolean binFull;
boolean motorOn;  // this flag is used to test motor condition either On or Off, true=On
int max_updateCount;    // this variable is used to define number times update method trigger, if any error occurs while doing
int updateCount;
// default value of variables, which will control action of each dust bin
int total_binDepth = 50; // this is total depth of the bin, value should be in centi-meter
int comp_trigger = 40; // this is triggering point, The compaction starts when the waste reaches this value
int comp_counter = 3;  // this will define the number of cycle for compaction, once counter reach to this value then it show bin fill
int temp_connter = 0;
int comp_percent = 20; // this will define the compaction in percentage, how much has to compact every time
int comp_depth = 0;  // this will be used for storing compaction depth each time, once compaction complete and help to define next compaction depth
int max_comp_weight = 20; // this will define the max compaction weight, once reach this value compaction will stop
boolean agentLogin;
int bin_Dmotor = 24; // bin door control relay
int bin_Dlock = 16; // bin door lock status read pin
int bin_align = 17; // this align pin is used to get status is bin, is it placed in proper manager or not
int motionDelay = 10000; // this variable is used to define delay after motion detection while doing compaction
float battery_min = 11; // this is used to notify user, once battery level drops below this value, ic low battery
int bin_wt = 10;      // default bin weight
int bin_rst_wt = 10;   // this +- value will used to compare weight and rest value, this will add to value,which was taken last before open bin door
int comp_stop_delay = 32000; // this variable is used to run moter in reverse order for defined duration


void setup()
{
  init_debug();//hardware serial
  init_rtc();// I2C based
  SPI.begin();			// Init SPI bus  for RFID RC522
  mfrc522.PCD_Init();		// Init MFRC522
  mfrc522.PCD_DumpVersionToSerial();	// Show details of PCD - MFRC522 Card Reader details

  initGPRS();
  onGPS(); // Turn ON GPS

  leftSonar.initSonar(10, 11);  // init pin numbers for nano 4,5
  //rightSonar.initSonar(6, 7); // init pin numbers

  batA.initBatMon(A3, 1026000L, 4657000L);
  gasA.initGasMonAnalog(A4); // set Analog Pin to sense voltage, smallResistor to SenseVoltage Across, LoadBearing Resistor
  pinMode(motionSen, INPUT);
  pinMode(IRQ_PIN, INPUT_PULLUP);
  pinMode(frwMotorCtrl, OUTPUT);
  pinMode(revMotorCtrl, OUTPUT);
  pinMode(bin_Dmotor, OUTPUT);
  pinMode(bin_Dlock, INPUT_PULLUP);
  pinMode(bin_align, INPUT_PULLUP);
  scale.set_scale(7050); //This value is obtained by using the SparkFun_HX711_Calibration sketch
  scale.tare(); //Assuming there is no weight on the scale at start up, reset the scale to 0
  digitalWrite(frwMotorCtrl, LOW);
  digitalWrite(revMotorCtrl, LOW);
  updating = false;
  compressing = false;
  binFull = false;
  motorOn = false;
  updateCount = 0;
  max_updateCount = 1; // later make it 3
  agentLogin = false;
  
  remote_update_id = Alarm.timerRepeat(00, 02, 00, RemoteUpdate); // timer for every 05 minutes
  
  //attachInterrupt(digitalPinToInterrupt(motionSen), controlMotor, RISING);

  // mfrc522.PCD_WriteRegister(mfrc522.ComIEnReg, regVal);
  //attachInterrupt(digitalPinToInterrupt(IRQ_PIN), agentInterruptbyRFID, FALLING);
  RemoteUpdate(); // First Time trigger on begin
}


void loop()
{
  //while(true)leftSonar.printSonarDistance();

//  if (binClosed()) {
    Serial.println(F("Bin is closed"));
    binMonitor(); // compress components inside dust bin
    monitorBattery();
    // RFID reader functionality
    if (!compressing && mfrc522.PICC_IsNewCardPresent())
    {
      if (!agentLogin && mfrc522.PICC_ReadCardSerial()) {
        agentInterruptbyRFID();
      }

    }
//  }
//  else {
//    // bin not closed and waiting to close
//    while (!digitalRead(bin_Dlock)) {
//      Serial.println(F("waiting to close bin door"));
//    }
//    digitalWrite(bin_Dmotor, LOW);
//    agentLogin = false;
//    if (!(bin_wt - bin_rst_wt <= scale.get_units() <= bin_wt + bin_rst_wt)) {
//      Serial.println(F("reset all value"));
//      restartBin();
//    }
//
//  }
}

//boolean binClosed() {
//  // based on bin align and door sensor will tell the status
//  boolean status = false;
//  if (digitalRead(bin_Dlock) && digitalRead(bin_align)) {
//    status = true;
//  }
//  return status;
//}

void monitorBattery() {
  if (batA.getVolt() < battery_min) {
    // send notification to agent about low battery status
    Serial.print(F("The Battery is low : "));
    Serial.print(batA.getVoltDirect());
    Serial.print(F(","));
    Serial.println(batA.getVolt());
  }
}


void binMonitor() {
  //float wt= scale.get_units();
  Serial.println(F("sonar value"));
  Serial.println(leftSonar.getSonarDistance());
  Serial.println(updating);
  Serial.println(binFull);
  Serial.println(comp_depth);
  if (leftSonar.getSonarDistance() < (total_binDepth - comp_trigger) ) {
    if (!updating && !binFull) {
      compressing = true;       // swithch on motor to start compression
      temp_connter++;
      comp_depth += (comp_percent * total_binDepth) / 100;
//      compactionStart();
//      while ((scale.get_units() < max_comp_weight) && (leftSonar.getSonarDistance() < total_binDepth - comp_depth) ) {
//        Serial.print(F("inside weight value "));
//        Serial.println(scale.get_units());
//        Serial.print(F("inside Sonal value "));
//        Serial.println(leftSonar.getSonarDistance());
//        if (!motorOn) {
//          compactionStart();
//        }
//        if (digitalRead(motionSen) == 1) {
//          controlMotor();
//        }

//      }

      if (temp_connter >= comp_counter) {
        binFull = true; // this condition will tell us the Max capacity of dust bin compression
      }
      compressing = false;
//      compactionStop();
      RemoteUpdate();

    }
  }
  if (updating) {
    RemoteUpdate();
  }
}

//void forceCompaction() {
//  if (!updating) {
//    compressing = true;       // swithch on motor to start compression
//    compactionStart();
//    if (comp_depth < (comp_percent * total_binDepth) / 100) {
//      comp_depth = (comp_percent * total_binDepth) / 100 - comp_depth;
//    } else {
//      comp_depth = comp_depth - (comp_percent * total_binDepth) / 100;
//    }
//
//    while ((scale.get_units() < max_comp_weight) && (leftSonar.getSonarDistance() < total_binDepth - comp_depth)) {
//      Serial.print(F("inside weight value "));
//      Serial.println(scale.get_units());
//      Serial.print(F("inside Sonal value "));
//      Serial.println(leftSonar.getSonarDistance());
//      if (!motorOn) {
//        compactionStart();
//      }
//    }
//    compressing = false;
//    compactionStop();
//
//  }
//
//}

//
//void controlMotor() {
//  Serial.println(F("Motion is detected"));
//  if (motorOn) {
//    //compactionStop();
//    digitalWrite(frwMotorCtrl, LOW);
//    // detachInterrupt(digitalPinToInterrupt(motionSen));
//    Alarm.delay(motionDelay);
//    // attachInterrupt(digitalPinToInterrupt(motionSen), controlMotor, RISING);
//  }
//
//}

//
//void compactionStart() {
//  digitalWrite(frwMotorCtrl, HIGH);
//  digitalWrite(revMotorCtrl, LOW);
//  motorOn = true;
//}
//
//void compactionStop() {
//  motorOn = false;
//  digitalWrite(frwMotorCtrl, LOW);
//  digitalWrite(revMotorCtrl, HIGH);
//  Alarm.delay(comp_stop_delay);
//  digitalWrite(revMotorCtrl, HIGH);
//
//}


void RemoteUpdate()
{
  updating = true;
  if (!compressing) {
    dbgSerial.print(F("Update: "));

    String body = F("{\"thing\": \"gsm_geeksynergy\"");
    // body.concat(",\"key\": ");
    // body.concat(dKey);
    body.concat(F(",\"key\": \"vJsj-FOCxI-3VKN9-"));
    body.concat(F("AOb@U-CfpcR-7OS@$-Fo73K-5KnoQ-51gxF-1-10\""));
    body.concat(F(",\"content\":{"));

    body.concat(F("\"weight\":{"));
    body.concat(F("\"kg\":"));
    body.concat(scale.get_units());
    body.concat(F("},"));
    
  body.concat(F("\"sonar\":{"));
    body.concat(F("\"A\":"));
    body.concat(leftSonar.getSonarDistance());
    body.concat(F(",\"B\":"));
    body.concat(rightSonar.getSonarDistance());
    body.concat(F("},"));

    body.concat(F("\"gps\":{"));
    body.concat(F("\"lat\":"));
    body.concat(getGPSVal().location.lat());
    body.concat(F(",\"lng\":"));
    body.concat(getGPSVal().location.lng());
    body.concat(F(",\"src\":"));
    body.concat("\"");
    body.concat(getGPSSource());
    body.concat("\"");
    body.concat(F("},"));

    body.concat(F("\"gas\":{"));
    body.concat(F("\"A\":{"));
    body.concat(F("\"val\":"));
    body.concat(gasA.readValue());
    body.concat(F(",\"trig\":"));
    body.concat(gasA.readState());
    body.concat(F("}},"));

    //   body.concat(F(",\"motion\":"));
    // body.concat(motion);

    body.concat(F("\"batt\":"));
    body.concat(F("{\"src\":"));
    body.concat(batA.getVolt());
    body.concat(F(",\"vref\":"));
    body.concat(batA.readVcc());
    body.concat(F("}"));
    body.concat(F("}}"));

    dbgSerial.println(body);
    boolean result = postUpdate(body);
    if (!result && updateCount < max_updateCount) {
      updateCount++;
      dbgSerial.println(F("Loop Update"));
      dbgSerial.println(updateCount);
      RemoteUpdate();

    }
    updateCount = 0;
    dbgSerial.println(F("Response return"));
    dbgSerial.println(result);
    updating = false;
  }
}


void restartBin() {
  temp_connter = 0;
  comp_depth = 0;
  binFull = false;
  //RemoteUpdate();
}

//void openDlock() {
//  digitalWrite(bin_Dmotor, HIGH);
//}

void agentInterruptbyRFID() {
  Serial.println(F("Agent is reached bin and punched in RFID"));
  String content = "";
  for (byte i = 0; i < mfrc522.uid.size; i++)
  {
    Serial.print(mfrc522.uid.uidByte[i] < 0x10 ? " 0" : " ");
    Serial.print(mfrc522.uid.uidByte[i], HEX);
    content.concat(String(mfrc522.uid.uidByte[i] < 0x10 ? " 0" : " "));
    content.concat(String(mfrc522.uid.uidByte[i], HEX));
  }
  content.toUpperCase();
  if (content.substring(1) == "E5 8F 0B 85") //change here the UID of the card/cards that you want to give access
  {
    Serial.println(F(" Authorized access"));
    Serial.println();
    agentLogin = true;
//    forceCompaction();
    bin_wt = scale.get_units();
    //openDlock();
    RemoteUpdate();
  }

  else {
    Serial.println(F(" Access denied"));
  }


}
