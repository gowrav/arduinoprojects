
#ifndef HEADER_RTCMANAGER
#define HEADER_RTCMANAGER

void init_rtc( void );
void printCurrentDateTime( void );
String getCurrentDateTimeString( void );

#endif
