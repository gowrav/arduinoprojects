#ifndef HEADER_BATMONITOR
#define HEADER_BATMONITOR

#define BAT_NUM_SAMPLES 10
#define CALIBRATE_VOLTAGE 6.456
#define BAT_SAMPLINGDELAY 10

class BatMon {
  public:
    BatMon();
    ~BatMon();
    boolean initBatMon(int sensorPin, double smallR , double bigR);
    void setVrefScaleConstant(long vref_scale_constant);
    void setSampleCount(byte count);
    void setCalibrateVoltage(float ref);
    double readVcc();
    double getVolt();
    double getVoltDirect();
};

#endif

