/*
  This entire program is taken from Jason Mildrum, NT7S and Przemek Sadowski, SQ9NJE.
  There is not enough original code written by me to make it worth mentioning.
  http://nt7s.com/
  http://sq9nje.pl/
  http://ak2b.blogspot.com/
  This sketch includes modifications to use the etherkit/Si5351Arduino library
  found here: https://github.com/etherkit/Si5351Arduino which was updated as of 4/17
  Read the 'Readme.md for details'
*/

#include "si5351_markold.h"
#include <Wire.h>


//#define F_MIN        50000000ULL               // Lower frequency limit
//#define F_MAX        9000000000UL

Si5351 si5351;

//volatile uint32_t LSB = 4498200000ULL;//899950000ULL;
//volatile uint32_t USB = 4499200000ULL;//900150000ULL;
 unsigned long bfo = 4500000000ULL;//900150000ULL; //start in usb

//These USB/LSB frequencies are added to or subtracted from the vfo frequency in the "Loop()"
//In this example my start frequency will be 14.20000 plus 9.001500 or clk0 = 23.2015Mhz
  unsigned long vfo = 62600000ULL;//1420000000ULL; / SI5351_FREQ_MULT; //start freq - change to suit
//volatile uint32_t radix = 100;  //start step size - change to suit
boolean changed_f = 1; // Print Once
//String tbfo = "";

//------------------------------- Set Optional Features here --------------------------------------
//Remove comment (//) from the option you want to use. Pick only one
#define IF_Offset //Output is the display plus or minus the bfo frequency
//#define Direct_conversion //What you see on display is what you get
//#define FreqX4  //output is four times the display frequency
//--------------------------------------------------------------------------------------------------




void setup()
{
  Serial.begin(115200);
  Wire.begin();

//  si5351.set_correction(231000, SI5351_PLL_INPUT_XO); //**mine. There is a calibration sketch in File/Examples/si5351Arduino-Jason
  //where you can determine the correction by using the serial monitor.

  //initialize the Si5351
  si5351.init(SI5351_CRYSTAL_LOAD_8PF, 0, 0); // 0 is the default crystal frequency of 25Mhz.
  //If you're using a 27Mhz crystal, put in 27000000 instead of 0

  si5351.set_pll(SI5351_PLL_FIXED, SI5351_PLLA);
  // Set CLK0 to output the starting "vfo" frequency as set above by vfo = ?

#ifdef IF_Offset
  volatile uint32_t vfoT = (vfo * SI5351_FREQ_MULT) + bfo;
  // Set CLK2 to output bfo frequency
  si5351.set_freq( bfo, SI5351_CLK2);
#endif

  //  si5351.drive_strength(SI5351_CLK0, SI5351_DRIVE_2MA); //you can set this to 2MA, 4MA, 6MA or 8MA
  //  si5351.drive_strength(SI5351_CLK1,SI5351_DRIVE_2MA); //be careful though - measure into 50ohms
  //  si5351.drive_strength(SI5351_CLK2, SI5351_DRIVE_2MA); //

}


void loop()
{
  // Update the display if the frequency has been changed
  if (changed_f)
  {
    //    display_frequency();

    Serial.print("BFO Frequency : ");
    Serial.println(bfo);

#ifdef IF_Offset
    Serial.print("LO Frequency : ");
    Serial.println(vfo + bfo);

    si5351.set_freq((vfo * SI5351_FREQ_MULT) + bfo, SI5351_CLK0);
    //you can also subtract the bfo to suit your needs
    //si5351.set_freq((vfo * SI5351_FREQ_MULT) - bfo  , SI5351_CLK0);

    //    if (vfo >= 10000000ULL & tbfo != "USB")
    //    {
    //      bfo = USB;
    //      tbfo = "USB";
    //      si5351.set_freq( bfo, SI5351_CLK2);
    //      Serial.println("We've switched from LSB to USB");
    //    }
    //    else if (vfo < 10000000ULL & tbfo != "LSB")
    //    {
    //      bfo = LSB;
    //      tbfo = "LSB";
    //      si5351.set_freq( bfo, SI5351_CLK2);
    //      Serial.println("We've switched from USB to LSB");
    //    }
#endif
    changed_f = 0;
  }
}
