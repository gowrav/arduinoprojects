#include <SoftwareSerial.h>
#include "Wire.h"// Arduino Wire library is required if I2Cdev I2CDEV_ARDUINO_WIRE implementation is used in I2Cdev.h, I2Cdev and MPU6050 must be installed as libraries, or else the .cpp/.h files for both classes must be in the include path of your project
#include "I2Cdev.h"
#include "MPU6050_6Axis_MotionApps20.h"
//#include "MPU6050.h" // not necessary if using MotionApps include file class default I2C address is 0x68 specific I2C addresses may be passed as a parameter here
// AD0 low = 0x68 (default for SparkFun breakout and InvenSense evaluation board)
// AD0 high = 0x69
//#include <Filters.h> // Refine the Reading with LowPass Filter

MPU6050 mpu;
#define LED_PIN 13 // (Arduino is 13, Teensy is 11, Teensy++ is 6)
bool blinkState = false;
SoftwareSerial btSerial(10, 11); // RX, TX


// MPU control/status vars
bool dmpReady = false;  // set true if DMP init was successful
uint8_t mpuIntStatus;   // holds actual interrupt status byte from MPU
uint8_t devStatus;      // return status after each device operation (0 = success, !0 = error)
uint16_t packetSize;    // expected DMP packet size (default is 42 bytes)
uint16_t fifoCount;     // count of all bytes currently in FIFO
uint8_t fifoBuffer[64]; // FIFO storage buffer

// orientation/motion vars
Quaternion q;           // [w, x, y, z]         quaternion container
VectorInt16 aa;         // [x, y, z]            accel sensor measurements
VectorInt16 aaReal;     // [x, y, z]            gravity-free accel sensor measurements
VectorInt16 aaWorld;    // [x, y, z]            world-frame accel sensor measurements
VectorFloat gravity;    // [x, y, z]            gravity vector
float euler[3];         // [psi, theta, phi]    Euler angle container
float ypr[3];           // [yaw, pitch, roll]   yaw/pitch/roll container and gravity vector


volatile bool mpuInterrupt = false;     // indicates whether MPU interrupt pin has gone high
void dmpDataReady() {
  mpuInterrupt = true;
}


//// filters out changes faster that 5 Hz.
//float filterFrequency = 10.0;
//
//// create a one pole (RC) lowpass filter
//FilterOnePole lowpassFilterx( LOWPASS, filterFrequency );
//FilterOnePole lowpassFiltery( LOWPASS, filterFrequency );
//FilterOnePole lowpassFilterz( LOWPASS, filterFrequency );

void initIMU_DMP_All()
{
  // Initialize all systems but the activity counts remains undisturbed
  // initialize device
  //Serial.println(F("Initializing I2C devices..."));
  mpu.initialize();

  // verify connection
  //Serial.println(F("Testing device connections..."));
  //Serial.println(mpu.testConnection() ? F("MPU6050 connection successful") : F("MPU6050 connection failed"));

  // wait for ready
  //  Serial.println(F("\nSend any character to begin DMP programming and demo: "));
  //  while (Serial.available() && Serial.read()); // empty buffer
  //  while (!Serial.available());                 // wait for data
  //  while (Serial.available() && Serial.read()); // empty buffer again

  // load and configure the DMP
  //Serial.println(F("Initializing DMP..."));
  devStatus = mpu.dmpInitialize();

  // make sure it worked (returns 0 if so)
  if (devStatus == 0) {
    // turn on the DMP, now that it's ready
    //Serial.println(F("Enabling DMP..."));
    mpu.setDMPEnabled(true);

    // enable Arduino interrupt detection
    //Serial.println(F("Enabling interrupt detection (Arduino external interrupt 0)..."));
    attachInterrupt(0, dmpDataReady, RISING);
    mpuIntStatus = mpu.getIntStatus();

    // set our DMP Ready flag so the main loop() function knows it's okay to use it
    //Serial.println(F("DMP ready! Waiting for first interrupt..."));
    dmpReady = true;

    // get expected DMP packet size for later comparison
    packetSize = mpu.dmpGetFIFOPacketSize();
  } else {
    // ERROR!
    // 1 = initial memory load failed
    // 2 = DMP configuration updates failed
    // (if it's going to break, usually the code will be 1)
    //Serial.print(F("DMP Initialization failed (code "));
    //Serial.print(devStatus);
    //Serial.println(F(")"));
  }
}


short failDMPdelay = 200;
short failCount = 0;
short failCountMax = 10;
short norm_factor = 64;
void acquireMPUdata()
{
  // Ensure the DMP is working
  if (!dmpReady)
  {
    if (failCount++ > failCountMax)
      return; // Encountered Critical logging required
    delay(failDMPdelay);
    initIMU_DMP_All();
  }
  else
    failCount = 0;
  // DMP monitor ends here

  while (!mpuInterrupt && fifoCount < packetSize)
  {
    // Put the system into Other tasks untill the fifo is filled and mpuInterrupt kicks In
  }
  // Once triggered with an Interrupt Bring the flag down. and read Int_Status
  mpuInterrupt = false;
  mpuIntStatus = mpu.getIntStatus();
  // get current FIFO count and ensure that there is no overflow in the system, if not the host has not responded in time to the IMU's interrupt
  fifoCount = mpu.getFIFOCount();

  // check for overflow (this should never happen unless our code is too inefficient)
  if ((mpuIntStatus & 0x10) || fifoCount == 1024) {
    // reset so we can continue cleanly
    mpu.resetFIFO();
    Serial.println(F("FIFO overflow!")); // Error also occurs if Wire is loose for I2C
  }
  // otherwise, check for DMP data ready interrupt (this should happen frequently)
  else if (mpuIntStatus & 0x01)
  {
    // wait for correct available data length, should be a VERY short wait
    while (fifoCount < packetSize) fifoCount = mpu.getFIFOCount();

    // read a packet from FIFO
    mpu.getFIFOBytes(fifoBuffer, packetSize);

    // track FIFO count here in case there is > 1 packet available
    // (this lets us immediately read more without waiting for an interrupt)
    fifoCount -= packetSize;
    // Now we have pockets of Data that can be used for Pedometry

    // display initial world-frame acceleration, adjusted to remove gravity
    // and rotated based on known orientation from quaternion
    mpu.dmpGetQuaternion(&q, fifoBuffer);
    mpu.dmpGetAccel(&aa, fifoBuffer);
    mpu.dmpGetGravity(&gravity, &q);
    mpu.dmpGetLinearAccelInWorld(&aaWorld, &aa, &q);

    //    lowpassFilterx.input(aaWorld.x );
    //    lowpassFiltery.input(aaWorld.y);
    //    lowpassFilterz.input(aaWorld.z);

    //
    //    Serial.print("aworld\t");
    //    Serial.print(lowpassFilterx.output()); Serial.print("\t");
    //    Serial.print(lowpassFiltery.output()); Serial.print("\t");
    //    Serial.println(lowpassFilterz.output());


    mpu.dmpGetEuler(euler, &q);
    // Serial.print("euler\t"); Serial.print(euler[0] * 180 / M_PI); Serial.print("\t"); Serial.print(euler[1] * 180 / M_PI); Serial.print("\t"); Serial.println(euler[2] * 180 / M_PI);

    mpu.dmpGetYawPitchRoll(ypr, &q, &gravity);
    //Serial.print("ypr\t"); Serial.print(ypr[0] * 180 / M_PI); Serial.print("\t"); Serial.print(ypr[1] * 180 / M_PI); Serial.print("\t"); Serial.println(ypr[2] * 180 / M_PI);

    //    // display real acceleration, adjusted to remove gravity
    //    mpu.dmpGetQuaternion(&q, fifoBuffer);
    //    mpu.dmpGetAccel(&aa, fifoBuffer);
    //    mpu.dmpGetGravity(&gravity, &q);
    //    mpu.dmpGetLinearAccel(&aaReal, &aa, &gravity);
    //    Serial.print("areal\t"); Serial.print(aaReal.x); Serial.print("\t"); Serial.print(aaReal.y); Serial.print("\t"); Serial.println(aaReal.z);


    //    normalize the values of Accel by dividing with 16;
    aaWorld.x = aaWorld.x / norm_factor;
    aaWorld.y = aaWorld.y / norm_factor;
    aaWorld.z = aaWorld.z / norm_factor;


    //Serial.print("aworld\t"); Serial.print(aaWorld.x); Serial.print("\t"); Serial.print(aaWorld.y); Serial.print("\t"); Serial.println(aaWorld.z);

    //    // blink LED to indicate activity
    //    blinkState = !blinkState;
    //    digitalWrite(LED_PIN, blinkState);

//    Serial.print(q.w); Serial.print(","); Serial.print(q.x); Serial.print(","); Serial.print(q.y); Serial.print(","); Serial.print(q.z); Serial.print(",,");
//    Serial.print(euler[0] * 180 / M_PI); Serial.print(","); Serial.print(euler[1] * 180 / M_PI); Serial.print(","); Serial.print(euler[2] * 180 / M_PI); Serial.print(",,");
//    Serial.print(ypr[0] * 180 / M_PI); Serial.print(","); Serial.print(ypr[1] * 180 / M_PI); Serial.print(","); Serial.print(ypr[2] * 180 / M_PI); Serial.print(",,");
//    Serial.print(aaReal.x); Serial.print(","); Serial.print(aaReal.y); Serial.print(","); Serial.print(aaReal.z); Serial.print(",,");
//    Serial.print(aaWorld.x); Serial.print(","); Serial.print(aaWorld.y); Serial.print(","); Serial.print(aaWorld.z); Serial.println("");
//
//    btSerial.print(q.w); btSerial.print(","); btSerial.print(q.x); btSerial.print(","); btSerial.print(q.y); btSerial.print(","); btSerial.print(q.z); btSerial.print(",,");
//    btSerial.print(euler[0] * 180 / M_PI); btSerial.print(","); btSerial.print(euler[1] * 180 / M_PI); btSerial.print(","); btSerial.print(euler[2] * 180 / M_PI); btSerial.print(",,");
//    btSerial.print(ypr[0] * 180 / M_PI); btSerial.print(","); btSerial.print(ypr[1] * 180 / M_PI); btSerial.print(","); btSerial.print(ypr[2] * 180 / M_PI); btSerial.print(",,");
//    btSerial.print(aaReal.x); btSerial.print(","); btSerial.print(aaReal.y); btSerial.print(","); btSerial.print(aaReal.z); btSerial.print(",,");
//    btSerial.print(aaWorld.x); btSerial.print(","); btSerial.print(aaWorld.y); btSerial.print(","); btSerial.print(aaWorld.z); btSerial.println("");

  }
}

float Xavg = 0;
float Zavg = 0;
float Yavg = 0;
short max_LoopUnit = 50;

float upperthreshhold = 135.0;
float lowerthreshhold = 115.0;
void calibrate()
{
  Serial.println("Callibrating - Don't Move");
  btSerial.println("Callibrating - Don't Move");
  digitalWrite(LED_PIN, HIGH); // Calibrating Data
  float xsum = 0;
  float ysum = 0;
  float zsum = 0;
  for (int i = 0; i < max_LoopUnit; i++)
  {
    acquireMPUdata();
    xsum += aaWorld.x;
    ysum += aaWorld.y;
    zsum += aaWorld.z;
  }
  //  Serial.print(xsum); Serial.print("\t"); Serial.print(ysum); Serial.print("\t"); Serial.println(zsum);
  Xavg = xsum / max_LoopUnit;
  Yavg = ysum / max_LoopUnit;
  Zavg = zsum / max_LoopUnit;
  digitalWrite(LED_PIN, LOW); // Calibrating Done
  //Serial.print("Avg: \t"); Serial.print(Xavg); Serial.print("\t"); Serial.print(Yavg); Serial.print("\t"); Serial.println(Zavg);
  delayMicroseconds(500);
  int thresavg = sqrt((Xavg * Xavg) + (Yavg * Yavg) + (Zavg * Zavg));
  lowerthreshhold = thresavg + 3;
  upperthreshhold = thresavg + 8;

  //Serial.print("Lth: "); Serial.print(lowerthreshhold); Serial.print("/t Uth: "); Serial.println(upperthreshhold);
  Serial.println("Done - Start Walking");
  btSerial.println("Done - Start Walking");
  //
  //    Serial.print("q.w,q.x,q.y,q.z,,");
  //    Serial.print("euler[0],euler[1],euler[2],,");
  //    Serial.print("YAW,PITCH,ROLL,,");
  //    Serial.print("aaReal.x,aaReal.y,aaReal.z,,");
  //    Serial.println("aaWorld.x,aaWorld.y,aaWorld.z");
  //
  //    btSerial.print("q.w,q.x,q.y,q.z,,");
  //    btSerial.print("euler[0],euler[1],euler[2],,");
  //    btSerial.print("YAW,PITCH,ROLL,,");
  //    btSerial.print("aaReal.x,aaReal.y,aaReal.z,,");
  //    btSerial.println("aaWorld.x,aaWorld.y,aaWorld.z");

  //  Serial.println("\t Calibrated \n");
  //  btSerial.println("\t Calibrated \n");
}

void setup() {
  Wire.begin();
  // initialize serial communication
  Serial.begin(115200);

  btSerial.begin(115200);
  //  btSerial.println("ArduinoPedo");

  // Lets initialize all the systems and keep the count at zero for all activities
  initIMU_DMP_All();
  // configure LED for output
  pinMode(LED_PIN, OUTPUT);

  // Onetime thing to calibrate
  calibrate();
}


float xval[50] = {0};
float yval[50] = {0};
float zval[50] = {0};

int  prevStep, steps, flag = 0;
unsigned long prev_stepped_time = -2000;
unsigned long current_stepped_time = 0;
boolean actionIdentifier = true;
void stepIN()
{
  int acc = 0;
  float totvect[50] = {0};
  float totave[50] = {0};
  float xaccl[50] = {0};
  float yaccl[50] = {0};
  float zaccl[50] = {0};


  for (int i = 0; i < max_LoopUnit; i++)
  {
    acquireMPUdata();
    xaccl[i] = aaWorld.x;
    yaccl[i] = aaWorld.y;
    zaccl[i] = aaWorld.z;
    totvect[i] = sqrt(((xaccl[i] - Xavg) * (xaccl[i] - Xavg)) + ((yaccl[i] - Yavg) * (yaccl[i] - Yavg)) + ((zval[i] - Zavg) * (zval[i] - Zavg)));
    //Serial.println(totvect[i]);
    delayMicroseconds(1);

    prevStep = steps;
    if (i > 1)
    {
      totave[i] = (totvect[i] + totvect[i - 1]) / 2 ;
      //Serial.println(totave[i]);
      //cal steps
      if (totave[i] > upperthreshhold && flag == 0)
      {
        flag = 1;
      }
      else if (totave[i] > upperthreshhold && flag == 1)
      {
        //do nothing
      }
      if (totave[i] < lowerthreshhold  && flag == 1)
      {
        current_stepped_time = millis();
        steps = steps + 1;
        flag = 0;
        break;
      }
    }

    // Serial.println('\n'); Serial.print("steps="); Serial.println(steps);
  }

  if (actionIdentifier)
  {
    if (prevStep != steps | ( millis() - prev_stepped_time < 3000))
    {
      //    for (int i = 0; i < 500; i++) // lets not count step for another 5 miliseconds
      //    {
      //      delayMicroseconds(10);
      //      acquireMPUdata();
      //    }
      // Serial.println('\n'); Serial.print("steps="); Serial.println(steps);
      if (prevStep != steps & ((current_stepped_time - prev_stepped_time) < 500) )
      {
        Serial.print("\n\nRunning : "); Serial.print('\t'); Serial.print("steps="); Serial.println(steps);
        btSerial.print("\n\nRunning : "); btSerial.print('\t'); btSerial.print("steps="); btSerial.println(steps);
      }
      else if (prevStep != steps & ((current_stepped_time - prev_stepped_time) < 700) )
      {
        Serial.print("\n\nBrisk Walking : "); Serial.print('\t'); Serial.print("steps="); Serial.println(steps);
        btSerial.print("\n\nBrisk Walking : "); btSerial.print('\t'); btSerial.print("steps="); btSerial.println(steps);
      }
      else if (prevStep != steps)
      {
        Serial.print("\n\nWalking : "); Serial.print('\t'); Serial.print("steps="); Serial.println(steps);
        btSerial.print("\n\nWalking : "); btSerial.print('\t'); btSerial.print("steps="); btSerial.println(steps);
      }
    }
    else
    {
      Serial.print("\n\nStationary : ");; Serial.print('\t'); Serial.print("Duration =  "); Serial.print((millis() - prev_stepped_time) / 1000); Serial.print(" Seconds");
      btSerial.print("\n\nStationary : ");; btSerial.print('\t'); btSerial.print("Duration =  "); btSerial.print((millis() - prev_stepped_time) / 1000); btSerial.print(" Seconds");
    }
    prev_stepped_time = current_stepped_time;
    //  delay(1000);
    // stepcal(totave);
  }

}


void loop() {
  stepIN();
  btSerialEvent();
}

String inputString = "";         // a string to hold incoming data
boolean stringComplete = false;  // whether the string is complete

void btSerialEvent() {
  inputString = "";
  stringComplete = false;
  while (btSerial.available()) {
    delayMicroseconds(500);
    // get the new byte:
    char inChar = (char)btSerial.read();
    // add it to the inputString:
    inputString += inChar;
    // if the incoming character is a newline, set a flag
    // so the main loop can do something about it:
    if (inChar == '\n') {
      stringComplete = true;
    }
  }

  if (inputString.startsWith("reset"))
  {
    calibrate();
  }

}


