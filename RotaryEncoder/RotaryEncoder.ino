/*     Arduino Rotary Encoder Tutorial

    by Dejan Nedelkovski, www.HowToMechatronics.com

*/

// One can also refer this https://perhof.wordpress.com/2012/11/01/using-rotary-encoders-with-arduino/


#define outputA 4
#define outputB 2

#define outputS A4

int counter = 0;
int aState;
int aLastState;

void setup() {
  pinMode (outputA, INPUT);
  pinMode (outputB, INPUT);
  pinMode (outputS, INPUT);
  // Enable internal pull-up resistors
  digitalWrite(outputS, HIGH);

  Serial.begin (9600);
  // Reads the initial state of the outputA
  aLastState = digitalRead(outputA);
}

void loop() {
  aState = digitalRead(outputA); // Reads the "current" state of the outputA

  // If the previous and the current state of the outputA are different, that means a Pulse has occured
  if (aState != aLastState) {
    // If the outputB state is different to the outputA state, that means the encoder is rotating clockwise
    if (digitalRead(outputB) != aState) {
      counter ++;
    } else {
      counter --;
    }
    Serial.print("Position: ");
    Serial.println(counter);
  }
  aLastState = aState; // Updates the previous state of the outputA with the current state

//  Serial.println("Switch State: " + digitalRead(outputS)?"HIGH":"LOW");

  if (!digitalRead(outputS)) {
    while (!digitalRead(outputS));
    Serial.println("Switch Triggerred.!");
  }

}
