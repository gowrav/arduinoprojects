//
//                                  ATtiny85
//                                  _______
//          (Chip Pin 1) PB5       |*      | VCC        (Chip Pin 8)
//          (Chip Pin 2) PB3 D3/A3 |       | D2/A1  PB2 (Chip Pin 7)
//          (Chip Pin 3) PB4 D4/A2 |       | D1     PB1 (Chip Pin 6)
//          (Chip Pin 4)       GND |       | D0     PB0 (Chip Pin 5)
//                                  -------
//

int pin5 = 0;
int pin6 = 1;
int pin7 = 2;
int pin2 = 3;
int pin3 = 4;
// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(pin2, OUTPUT);
  pinMode(pin3, OUTPUT);
  pinMode(pin5, OUTPUT);
  pinMode(pin6, OUTPUT);
  pinMode(pin7, OUTPUT);
}

// the loop function runs over and over again forever
void loop() {
  digitalWrite(pin2, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(100);                       // wait for a second
  digitalWrite(pin3, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(100);                       // wait for a second
  digitalWrite(pin5, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(100);                       // wait for a second
  digitalWrite(pin6, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(100);                       // wait for a second
  digitalWrite(pin7, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(100);                       // wait for a second

  digitalWrite(pin2, LOW);   // turn the LED on (HIGH is the voltage level)
  delay(100);                       // wait for a second
  digitalWrite(pin3, LOW);   // turn the LED on (HIGH is the voltage level)
  delay(100);                       // wait for a second
  digitalWrite(pin5, LOW);   // turn the LED on (HIGH is the voltage level)
  delay(100);                       // wait for a second
  digitalWrite(pin6, LOW);   // turn the LED on (HIGH is the voltage level)
  delay(100);                       // wait for a second
  digitalWrite(pin7, LOW);   // turn the LED on (HIGH is the voltage level)
  delay(100);                       // wait for a second

}
