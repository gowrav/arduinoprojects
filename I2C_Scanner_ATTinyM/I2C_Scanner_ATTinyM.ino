#include <TinyWireM.h> // I2C Master lib for ATTinys which use USI
#include <SoftwareSerial.h>

SoftwareSerial tinySerial(3, 4);
int pulse = 1;

void setup()
{
  pinMode(pulse, OUTPUT);
  tinySerial.begin(9600);
  tinySerial.println("\nI2C Scanner");
  TinyWireM.begin(); // initialize I2C lib
}


void loop()
{

  byte error, address;
  int nDevices;

  tinySerial.println("Scanning...");

  nDevices = 0;
  for (address = 1; address < 127; address++ )
  {
    // The i2c_scanner uses the return value of
    // the Write.endTransmisstion to see if
    // a device did acknowledge to the address.
    TinyWireM.beginTransmission(address);
    error =  TinyWireM.endTransmission(); // Send to the slave

    if (error == 0)
    {
      tinySerial.print("I2C device found at address 0x");
      if (address < 16)
        tinySerial.print("0");
      tinySerial.print(address, HEX);
      tinySerial.println("  !");

      nDevices++;
    }
    else if (error == 4)
    {
      tinySerial.print("Unknown error at address 0x");
      if (address < 16)
        tinySerial.print("0");
      tinySerial.println(address, HEX);
    }
  }
  if (nDevices == 0)
    tinySerial.println("No I2C devices found\n");
  else
    tinySerial.println("done\n");



  int k = 10;
  while (k--) {
    digitalWrite(pulse, HIGH);   // turn the LED on (HIGH is the voltage level)
    delay(100);                       // wait for a second
    digitalWrite(pulse, LOW);    // turn the LED off by making the voltage LOW
    delay(100);                       // wait for a second
  }

}
