// LinkIt One sketch for MQTT Demo

#include <LWiFi.h>
#include <LWiFiClient.h>

#include <PubSubClient.h>

/* 
  Modify to your WIFI Access Credentials. 
*/
#define WIFI_AP "GeekSynergy_HQ"
#define WIFI_PASSWORD "p@ssw0rd"
#define WIFI_AUTH LWIFI_WPA  // choose from LWIFI_OPEN, LWIFI_WPA, or LWIFI_WEP.

/*
  Modify to your MQTT broker - Select only one
*/
char mqttBroker[] = "iot.eclipse.org";
// byte mqttBroker[] = {192,168,1,220}; // modify to your local broker

LWiFiClient wifiClient;

PubSubClient client( wifiClient );

unsigned long lastSend;

void InitLWiFi()
{
  LWiFi.begin();
  // Keep retrying until connected to AP
  Serial.println("Connecting to AP");
  while (0 == LWiFi.connect(WIFI_AP, LWiFiLoginInfo(WIFI_AUTH, WIFI_PASSWORD))) {
    delay(1000);
  }
  Serial.println("Connected to AP");
}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Connecting to MQTT broker ...");
    // Attempt to connect
    if ( client.connect("LinkIt One Client") ) {  // Better use some random name
      Serial.println( "[DONE]" );
      // Publish a message on topic "outTopic"
      client.publish( "outTopic","Hello, This is LinkIt One" );
    // Subscribe to topic "inTopic"
      client.subscribe( "inTopic" );
    } else {
      Serial.print( "[FAILED] [ rc = " );
      Serial.print( client.state() );
      Serial.println( " : retrying in 5 seconds]" );
      // Wait 5 seconds before retrying
      delay( 5000 );
    }
  }
}


void setup()
{
   delay( 10000 );
   Serial.begin( 115200 );
   InitLWiFi();

   client.setServer( mqttBroker, 1883 );
   client.setCallback( callback );

   lastSend = 0;
}

void loop()
{
  
  if( !client.connected() ) {
    reconnect();
  }

  if( millis()-lastSend > 5000 ) {  // Send an update only after 5 seconds
    sendAnalogData();
    lastSend = millis();
  }
  
  client.loop();
}

void callback( char* topic, byte* payload, unsigned int length ) {
  Serial.print( "Recived message on Topic:" );
  Serial.print( topic );
  Serial.print( "    Message:");
  for (int i=0;i<length;i++) {
    Serial.print( (char)payload[i] );
  }
  Serial.println();
}

void sendAnalogData() {
  // Read data to send
  int data_A0 = analogRead( A0 );
  int data_A1 = analogRead( A1 );
  int data_A2 = analogRead( A2 );

  // Just debug messages
  Serial.print( "Sending analog data : [" );
  Serial.print( data_A0 ); Serial.print( data_A1 ); Serial.print( data_A2 );
  Serial.print( "]   -> " );

  // Prepare a JSON payload string
  String payload = "{";
  payload += "\"A0\":\""; payload += int(data_A0); payload += "\", ";
  payload += "\"A1\":\""; payload += int(data_A1); payload += "\", ";
  payload += "\"A2\":\""; payload += int(data_A2); payload += "\"";
  payload += "}";
  
  // Send payload
  char analogData[100];
  payload.toCharArray( analogData, 100 );
  client.publish( "analogData", analogData );
  Serial.println( analogData );
}

