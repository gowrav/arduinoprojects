
// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(13, OUTPUT);
  pinMode(9, OUTPUT);

  pinMode(2, OUTPUT);
  pinMode(3, OUTPUT);

  pinMode(8, OUTPUT);
  pinMode(12, OUTPUT);


//  pinMode(A6, OUTPUT);
//  digitalWrite(A6, HIGH);   // turn the LED on (HIGH is the voltage level)

}

// the loop function runs over and over again forever
void loop() {
  digitalWrite(9, HIGH);  
  digitalWrite(13, HIGH); 

  digitalWrite(2, LOW); 
  digitalWrite(3, LOW); 
  
  digitalWrite(8, LOW); 
  digitalWrite(12, LOW); 

  delay(1000);

  digitalWrite(9, HIGH);  
  digitalWrite(13, HIGH); 

  digitalWrite(2, LOW); 
  digitalWrite(3, LOW); 
  
  digitalWrite(8, HIGH); 
  digitalWrite(12, HIGH); 

 delay(1000);

 digitalWrite(9, LOW);  
  digitalWrite(13, LOW); 

  digitalWrite(2, LOW); 
  digitalWrite(3, LOW); 
  
  digitalWrite(8, HIGH); 
  digitalWrite(12, HIGH); 

  delay(1000);

  digitalWrite(9, LOW);  
  digitalWrite(13, LOW); 

  digitalWrite(2, HIGH); 
  digitalWrite(3, HIGH); 
  
  digitalWrite(8, LOW); 
  digitalWrite(12, LOW); 

  delay(1000);

  

  
}
