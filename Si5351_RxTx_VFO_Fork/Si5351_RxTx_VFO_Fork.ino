/*
  A part of this program is taken from Jason Mildrum, NT7S.
  All extra functions are written by me, Rob Engberts PA0RWE

  References:
  http://nt7s.com/
  http://sq9nje.pl/
  http://ak2b.blogspot.com/
  http://pa0rwe.nl/?page_id=804

    SI5351_VFO control program for Arduino NANO
    Copyright PA0RWE Rob Engberts

    Using the Si5351 library by Jason Mildrun nt7s

    Functions:
    - CLK0 - Tx frequency = Display frequency
    - CLK1 - Rx / RIT frequency = Tx +/- BFO (upper- or lower mixing)
             When RIT active, RIT frequency is displayed and is tunable.
             When RIT is inactive Rx = Tx +/- BFO
    - CLK2 - BFO frequency, tunable

    - Stepsize:  select (pushbutton)
    - Calibrate: (pushbutton) calculates difference between X-tal and measured
                 x-tal frequency, to correct x-tal frequency.
    - Selection: (pushbutton) Switch between TRx and BFO mode
    - RIT switch: tunable Rx frequency, while Tx frequency not changed
    - Programming PIC by ICSP

    Si5351 settings: I2C address is in the .h file
                     X-tal freq is in the .h file but set in line 354

***************************************************************************
    02-04-2015   0.1    Start building program based on the PIC version

***************************************************************************
   Includes
**************************************************************************/

#include <VFO_si5351.h>
#include <Wire.h>

/**************************************************************************
   (Pin) Definitions
**************************************************************************/
//      I2C-SDA       A4      // I2C-SDA
//      I2C-SCL       A5      // I2C-SCL

//#define F_MIN        1000000UL        // Lower frequency limit
//#define F_MAX        300000000UL      // Upper frequency limit


Si5351 si5351;

/**************************************************************************
   Declarations
**************************************************************************/
volatile uint32_t bfo_f = 4499200000ULL / SI5351_FREQ_MULT;    // CLK0 start IF
volatile uint32_t rfVFO_f = 62600000ULL / SI5351_FREQ_MULT;   // RF Tuning freq
volatile uint32_t lo_f = rfVFO_f + bfo_f;                      // CLK2 start Rx freq
uint32_t vco_c = 0;                                           // X-tal correction factor
uint32_t xt_freq;// Default 0
//volatile uint32_t calibrate_init = 232000ULL;//0;
volatile uint32_t calibrate_init = 189000ULL;//0;


/**************************************/
/*            S E T U P               */
/**************************************/
void setup()
{
  Serial.begin(115200);
  Wire.begin();

  //initialize the Si5351
  si5351.set_correction(calibrate_init); // Set to zero because I'm using an other calibration method
//  si5351.init(SI5351_CRYSTAL_LOAD_8PF, xt_freq); //If you're using a 27Mhz crystal, put in 27000000 instead of 0
  si5351.init(SI5351_CRYSTAL_LOAD_8PF, xt_freq); //If you're using a 27Mhz crystal, put in 27000000 instead of 0
  
  // 0 is the default crystal frequency of 25Mhz.
  si5351.set_pll(SI5351_PLL_FIXED, SI5351_PLLA);

  // Set CLK0 to output the Rx frequncy lo_f = vfo +/- bfo frequency
  si5351.set_freq((lo_f * SI5351_FREQ_MULT), SI5351_PLL_FIXED, SI5351_CLK0);
  //  si5351.drive_strength(SI5351_CLK0, SI5351_DRIVE_4MA);
  si5351.drive_strength(SI5351_CLK0, SI5351_CLK_DRIVE_STRENGTH_8MA);


  // Set CLK2 to output the starting "bfo" frequency as set above by bfo_f = ?
  si5351.set_freq((bfo_f * SI5351_FREQ_MULT), SI5351_PLL_FIXED, SI5351_CLK2);
  //  si5351.drive_strength(SI5351_CLK2, SI5351_DRIVE_4MA);
  si5351.drive_strength(SI5351_CLK2, SI5351_CLK_DRIVE_STRENGTH_8MA);

  //// Set CLK1 to output bfo frequency
  //  si5351.set_freq((bfo_f * SI5351_FREQ_MULT), SI5351_PLL_FIXED, SI5351_CLK1);
  //  si5351.drive_strength(SI5351_CLK1,SI5351_DRIVE_2MA);
  //  si5351.set_clock_disable(SI5351_CLK1, 1);

  Serial.println("SI5351 Tuned..!");

}

void loop() {
}





