#include "si5351.h"
#include "Wire.h"
#include <EEPROM.h>
#include "EEPROMAnything.h"
#include <RotaryEncoder.h>;

#include <LiquidCrystal.h>
const int rs = 5, en = 6, d4 = 7, d5 = 8, d6 = 9, d7 = 10;
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);

const int buttonPin1 = 11;
const int buttonPin2 = 12;

int buttonState1 = 0;
int buttonState2 = 0;

//#define DEBUG_MODE // Uncomment to Debug

// Frequnecy Limits for tuning
uint64_t tuningfreq_minval = 50000000ULL;
uint64_t tuningfreq_maxval = 2000000000ULL;

int encoderpinA = 3;//Pin for the Current Board
int encoderpinB = 2;//Pin for the Current Board
int encoderSWpin = 4;//Pin for the Current Board
int encoderMultiplier = 5;//SET as Default
int encoderStepSize = 6;//SET as Default
int encoderInterval = 2000;//SET as Default
long changeMultiplier = 50000;// Custom Configuration set to 10000 for fine tuning

struct config_t
{
  uint64_t xtal_f;
  uint64_t ppm_corr;
  uint64_t default_vfo_f;
  uint64_t default_bfo_f;
  uint64_t lo_f = 0;//default_vfo_f + default_bfo_f;
} configuration;



Si5351 si5351;

bool valueChanged = true;//setup to default freq
bool runOnce = true;//initalize the xtal freq, correction, power

int pref_xtal_f_eepromadr = 1;
int pref_ppm_corr_eepromadr = 2;
int pref_bfo_eepromadr = 3;
int pref_vfo_eepromadr = 4;

RotaryEncoder encoder(encoderpinA, encoderpinB, encoderMultiplier, encoderStepSize, encoderInterval);

String cmdStr;
int opListSize = 6;
char* opList[] = {"SXF", "SCF",  "SVF", "SBF", "RLO", "ZZZ"};

void initDefaultParams() {
  configuration.xtal_f = 0ULL;
  configuration.ppm_corr = 184000ULL;//238000ULL;//232000ULL;
  configuration.default_vfo_f =   62650000ULL;
  configuration.default_bfo_f = 4499200000ULL;
  configuration.lo_f = configuration.default_vfo_f + configuration.default_bfo_f;
  savePrefValues();
}



long rotaryDelay = 5;
long printDelay = 100000;
long counterTime = 0;

void rotaryReadOut() {
  int enc = encoder.readEncoder();
  if (enc != 0) {
    configuration.default_vfo_f = configuration.default_vfo_f + (enc) * changeMultiplier;
    configuration.default_vfo_f = ((enc > 0) && (configuration.default_vfo_f > tuningfreq_maxval))  ? tuningfreq_minval : min(configuration.default_vfo_f, tuningfreq_maxval);
    configuration.default_vfo_f = ((enc < 0) && (configuration.default_vfo_f < tuningfreq_minval))  ? tuningfreq_maxval : max(configuration.default_vfo_f, tuningfreq_minval);
    configuration.lo_f = configuration.default_vfo_f + configuration.default_bfo_f;
    counterTime = 0;
    valueChanged = true;
  }
  delayMicroseconds(5);
}


void regularPrint() {
  // Print once every 50ms approx
  if (counterTime <= 0 )
  {
    Serial.println(uint64ToString(configuration.default_vfo_f));
    lcd.setCursor(5,0);
    lcd.print("TUNING");
    lcd.setCursor(0, 1);
    if(configuration.default_vfo_f<100000000){
      lcd.print("Freq: "+String(configuration.default_vfo_f/100000.00));
      lcd.setCursor(13,1);
      lcd.print("kHz");
      lcd.setCursor(0, 1);
    }
    else{
      lcd.print("Freq: "+String((configuration.default_vfo_f/100000000.0000),4));
      lcd.setCursor(13,1);
      lcd.print("MHz");
      lcd.setCursor(0, 1);
    }
    counterTime = printDelay / rotaryDelay;
  }
  counterTime--;
}


void setup()
{
  // Start serial and initialize the Si5351
  Serial.begin(115200);

lcd.begin(16, 2);
lcd.setCursor(2, 0);
lcd.print("GEEKSYNERGY");
delay(2000); 
lcd.setCursor(0,0);
lcd.print("                ");
pinMode(buttonPin1, INPUT_PULLUP);
pinMode(buttonPin2, INPUT_PULLUP);
pinMode(encoderSWpin, INPUT_PULLUP);

  loadPrefValues();
  if (!si5351.init(SI5351_CRYSTAL_LOAD_8PF, configuration.xtal_f, configuration.ppm_corr))
  {
#ifdef DEBUG_MODE
    Serial.println(F("Device not found on I2C bus!"));
#endif
  }
#ifdef DEBUG_MODE
  Serial.println(F("Commands Help : SXF, SCF, SVF, SBF, RLO, ZZZ"));
#endif
}

void loop()
{
  buttonState1 = digitalRead(buttonPin1);
  buttonState2 = digitalRead(buttonPin2);

  if (buttonState1 == 0) {
    lcd.setCursor(0,0);
    lcd.print("Button 1 pressed");
    lcd.setCursor(0,0);
    lcd.print("                ");
  }
  if (buttonState2 == 0) {
    lcd.setCursor(0,0);
    lcd.print("Button 2 pressed");
    lcd.setCursor(0,0);
    lcd.print("                ");
  }
  
  
  if (valueChanged) {
    valueChanged = false;
    counterTime = 0;
    printFreqValues();
    if (runOnce) {
      runOnce = false;
      si5351.init(SI5351_CRYSTAL_LOAD_8PF, configuration.xtal_f, configuration.ppm_corr);
      si5351.drive_strength(SI5351_CLK2, SI5351_DRIVE_2MA);
      si5351.drive_strength(SI5351_CLK0, SI5351_DRIVE_2MA);
    }
    si5351.set_freq(configuration.default_bfo_f, SI5351_CLK2);
    si5351.set_freq(configuration.lo_f, SI5351_CLK0);
  }
  fetchSerialCommand();
  rotaryReadOut();
  regularPrint();
  checkandSaveBtn();
}

void checkandSaveBtn() {
  if (!digitalRead(encoderSWpin)) {
    while (!digitalRead(encoderSWpin))delay(10);
    EEPROM_writeAnything(0, configuration);
  }
}

void fetchSerialCommand() {
  if (Serial.available() > 0) cmdStr.remove(0); // Remove previous commands
  while (Serial.available() > 0)
  {
    char carbyte = Serial.read();
    if (carbyte == ',')
      break;
    else
      cmdStr += carbyte;
    delayMicroseconds(80);//best delay b/w bytes
  }

  if (cmdStr.length() >= 3)
  {
    char tempBuf [64];

    switch (IsArrayContains(cmdStr.substring(0, 3), opList))
    {
      case 0  :
        cmdStr.substring(4).toCharArray(tempBuf, cmdStr.substring(4).length());
        configuration.xtal_f = strtoul(tempBuf, NULL, 0) ;
        savePrefValues();
        break;

      case 1  :
        cmdStr.substring(4).toCharArray(tempBuf, cmdStr.substring(4).length());
        configuration.ppm_corr = strtoul(tempBuf, NULL, 0);
        savePrefValues();
        break;

      case 2  :
        cmdStr.substring(4).toCharArray(tempBuf, cmdStr.substring(4).length());
        configuration.default_vfo_f = strtoul(tempBuf, NULL, 0) ;
        configuration.lo_f = configuration.default_vfo_f + configuration.default_bfo_f;
        savePrefValues();
        break;

      case 3  :
        cmdStr.substring(4).toCharArray(tempBuf, cmdStr.substring(4).length());
        configuration.default_bfo_f = strtoul(tempBuf, NULL, 0) ;
        configuration.lo_f = configuration.default_vfo_f + configuration.default_bfo_f;
        savePrefValues();
        break;

      case 4  :
        printFreqValues();
        break;

      case 5  :
        initDefaultParams();
        break;

      default :
        cmdStr.remove(0);
        return;
    }

    cmdStr.remove(0);
    valueChanged = true;
  }
}


void savePrefValues() {
  EEPROM_writeAnything(0, configuration);
}

void loadPrefValues() {
  EEPROM_readAnything(0, configuration);
}

void printFreqValues() {
#ifdef DEBUG_MODE
  Serial.print("Xtal Frequency : ");
  Serial.println(uint64ToString(configuration.xtal_f));
  Serial.print("PPM Correction : ");
  Serial.println(uint64ToString(configuration.ppm_corr));
  Serial.print("VFO Frequency : ");
  Serial.println(uint64ToString(configuration.default_vfo_f));
  Serial.print("BFO Frequency : ");
  Serial.println(uint64ToString(configuration.default_bfo_f));
  Serial.print("LO Frequency : ");
  Serial.println(uint64ToString(configuration.lo_f));
#endif

}


int IsArrayContains(String searchValue, char* searchList[])
{
  searchValue.toUpperCase();
  for (int i = 0; i < opListSize ; i++)
    if (searchValue == searchList[i])
      return i;
  return -1;
}


String uint64ToString(uint64_t input) {
  String result = "";
  uint8_t base = 10;

  do {
    char c = input % base;
    input /= base;

    if (c < 10)
      c += '0';
    else
      c += 'A' - 10;
    result = c + result;
  } while (input);
  return result;
}
