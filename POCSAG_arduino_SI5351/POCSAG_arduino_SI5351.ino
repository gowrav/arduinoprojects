// sendpocsag

// receive POCSAG-message from serial port
// transmit message on 433.900 Mhz using si4432-based "RF22" ISM modules

// This is the "non-ham" version of the application. All references
// to ham call-signs and the CW beacon has been removed

// this program uses the arduino RadioHead library
// http://www.airspayce.com/mikem/arduino/RadioHead/


// Version 0.1.0 (20140330) Initial release
// Version 0.1.1 (20140418) pocsag message creation now done on arduino
// Version 0.1.2 (20140516) using "Pocsag" class, moved to ReadHead library, removed callsign and CW beacon


#include <SPI.h>
// pocsag message generation library
#include <Pocsag.h>

/*
   si5351_example.ino - Simple example of using Si5351Arduino library
*/

#include "si5351.h"
#include "Wire.h"

Si5351 si5351;
//Si5351 si5351(0x61);


volatile int index = 0;
volatile uint16_t symbol;
volatile int psk_bit = 0;
volatile int zero_count = 0;


Pocsag pocsag;

// frequency
//unsigned long freq = 1500000000ULL;
unsigned long freq = 6960000000ULL;
unsigned long freq_deviation = 4000ULL;
//unsigned long freq_deviation = 6000ULL;

void setup()
{

  // Start serial and initialize the Si5351
  Wire.begin();
  Wire.setClock(400000L);
  Serial.begin(115200);

  Serial.println("Init SI5351");
  si5351.init(SI5351_CRYSTAL_LOAD_6PF, 0, 0);

  // Set CLK0 to output freq MHz
  si5351.set_freq(freq, SI5351_CLK0);
  si5351.output_enable(SI5351_CLK0, 0);

  // Query a status update and wait a bit to let the Si5351 populate the
  // status flags correctly.
  si5351.update_status();
  delay(500);

  // Read the Status Register and print it every 10 seconds
  si5351.update_status();
  Serial.print("SYS_INIT: ");
  Serial.print(si5351.dev_status.SYS_INIT);
  Serial.print("  LOL_A: ");
  Serial.print(si5351.dev_status.LOL_A);
  Serial.print("  LOL_B: ");
  Serial.print(si5351.dev_status.LOL_B);
  Serial.print("  LOS: ");
  Serial.print(si5351.dev_status.LOS);
  Serial.print("  REVID: ");
  Serial.println(si5351.dev_status.REVID);



}


void loop() {
  //vars
  int rc;
  int state; // state machine for CLI input

  // bell = ascii 0x07
  char bell = 0x07;

  // data
  long int address;
  int addresssource;
  int repeat;
  char textmsg[42]; // to store text message;
  int msgsize;

  int freqsize;
  int freq1; // freq MHZ part (3 digits)
  int freq2; // freq. 100 Hz part (4 digits)

  // read input:
  // format: "P <address> <source> <repeat> <message>"
  // format: "F <freqmhz> <freq100Hz>"

  Serial.println("Format:");
  Serial.println("P <address> <source> <repeat> <message>");
  Serial.println("F <freqmhz> <freq100Hz>");

  // init var
  state = 0;
  address = 0;
  addresssource = 0;
  msgsize = 0;

  freqsize = 0;
  freq1 = 0; freq2 = 0;

  while (state >= 0) {
    char c;
    // loop until we get a character from serial input
    while (!Serial.available()) {
    }; // end while

    c = Serial.read();


    // break out on ESC
    if (c == 0x1b) {
      state = -999;
      break;
    }; // end if


    // state machine
    if (state == 0) {
      // state 0: wait for command
      // P = PAGE
      // F = FREQUENCY

      if ((c == 'p') || (c == 'P')) {
        // ok, got our "p" -> go to state 1
        state = 1;

        // echo back char
        Serial.write(c);
      } else if ((c == 'f') || (c == 'F')) {
        // ok, got our "f" -> go to state 1
        state = 10;

        // echo back char
        Serial.write(c);
      } else {
        // error: echo "bell"
        Serial.write(bell);
      }; // end else - if

      // get next char
      continue;
    }; // end state 0

    // state 1: space (" ") or first digit of address ("0" to "9")
    if (state == 1) {
      if (c == ' ') {
        // space -> go to state 2 and get next char
        state = 2;

        // echo back char
        Serial.write(c);

        // get next char
        continue;
      } else if ((c >= '0') && (c <= '9')) {
        // digit -> first digit of address. Go to state 2 and process
        state = 2;

        // continue to state 2 without fetching next char
      } else {
        // error: echo "bell"
        Serial.write(bell);

        // get next char
        continue;
      }; // end else - if
    };  // end state 1

    // state 2: address ("0" to "9")
    if (state == 2) {
      if ((c >= '0') && (c <= '9')) {
        long int newaddress;

        newaddress = address * 10 + (c - '0');

        if (newaddress <= 0x1FFFFF) {
          // valid address
          address = newaddress;

          Serial.write(c);
        } else {
          // address to high. Send "beep"
          Serial.write(bell);
        }; // end else - if

      } else if (c == ' ') {
        // received space, go to next field (address source)
        Serial.write(c);
        state = 3;
      } else {
        // error: echo "bell"
        Serial.write(bell);
      }; // end else - elsif - if

      // get next char
      continue;
    }; // end state 2

    // state 3: address source: one single digit from 0 to 3
    if (state == 3) {
      if ((c >= '0') && (c <= '3')) {
        addresssource = c - '0';
        Serial.write(c);

        state = 4;
      } else {
        // invalid: sound bell
        Serial.write(bell);
      }; // end if

      // get next char
      continue;
    }; // end state 3


    // state 4: space between source and repeat
    if (state == 4) {
      if (c == ' ') {
        Serial.write(c);

        state = 6; // go from state 4 to state 6
        // (No state 5, callsign removed)
      } else {
        // invalid: sound bell
        Serial.write(bell);
      }; // end if

      // get next char
      continue;
    }; // end state 4


    // state 5: callsign: REMOVED in non-ham version

    // state 6: repeat: 1-digit value between 0 and 9
    if (state == 6) {
      if ((c >= '0') && (c <= '9')) {
        Serial.write(c);
        repeat = c - '0';

        // move to next state
        state = 7;
      } else {
        Serial.write(bell);
      }; // end if

      // get next char
      continue;
    }; // end state 6


    // state 7: space between repeat and message
    if (state == 7) {
      if (c == ' ') {
        Serial.write(c);

        // move to next state
        state = 8;
      } else {
        // invalid char
        Serial.write(bell);
      }; // end else - if

      // get next char
      continue;
    }; // end state 7


    // state 8: message, up to 40 chars, terminate with cr (0x0d) or lf (0x0a)
    if (state == 8) {
      // accepted is everything between space (ascii 0x20) and ~ (ascii 0x7e)
      if ((c >= 0x20) && (c <= 0x7e)) {
        // accept up to 40 chars
        if (msgsize < 40) {
          Serial.write(c);

          textmsg[msgsize] = c;
          msgsize++;
        } else {
          // to long
          Serial.write(bell);
        }; // end else - if

      } else if ((c == 0x0a) || (c == 0x0d)) {
        // done

        Serial.println("");

        // add terminating NULL
        textmsg[msgsize] = 0x00;

        // break out of loop
        state = -1;
        break;

      } else {
        // invalid char
        Serial.write(bell);
      }; // end else - elsif - if

      // get next char
      continue;
    }; // end state 8;


    // PART 2: frequency

    // state 10: space (" ") or first digit of address ("0" to "9")
    if (state == 10) {
      if (c == ' ') {
        // space -> go to state 11 and get next char
        state = 11;

        // echo back char
        Serial.write(c);

        // get next char
        continue;
      } else if ((c >= '0') && (c <= '9')) {
        // digit -> first digit of address. Go to state 2 and process
        state = 11;

        // init freqsize;
        freqsize = 0;

        // continue to state 2 without fetching next char
      } else {
        // error: echo "bell"
        Serial.write(bell);

        // get next char
        continue;
      }; // end else - if
    };  // end state 10


    // state 11: freq. Mhz part: 3 digits needed
    if (state == 11) {
      if ((c >= '0') && (c <= '9')) {
        if (freqsize < 3) {
          freq1 *= 10;
          freq1 += (c - '0');

          freqsize++;
          Serial.write(c);

          // go to state 12 (wait for space) if 3 digits received
          if (freqsize == 3) {
            state = 12;
          }; // end if
        } else {
          // too large: error
          Serial.write(bell);
        }; // end else - if
      } else {
        // unknown char: error
        Serial.write(bell);
      }; // end else - if

      // get next char
      continue;

    }; // end state 11


    // state 12: space between freq part 1 and freq part 2
    if (state == 12) {
      if (c == ' ') {
        // space received, go to state 13
        state = 13;
        Serial.write(c);

        // reinit freqsize;
        freqsize = 0;
      } else {
        // unknown char
        Serial.write(bell);
      }; // end else - if

      // get next char
      continue;

    }; // end state 12;

    // state 13: part 2 of freq. (100 Hz part). 4 digits needed
    if (state == 13) {
      if ((c >= '0') && (c <= '9')) {
        if (freqsize < 4) {
          freq2 *= 10;
          freq2 += (c - '0');

          freqsize++;
          Serial.write(c);
        } else {
          // too large: error
          Serial.write(bell);
        }; // end else - if

        // get next char
        continue;

      } else if ((c == 0x0a) || (c == 0x0d)) {
        if (freqsize == 4) {
          // 4 digits received, done
          state = -2;
          Serial.println("");

          // break out
          break;
        } else {
          // not yet 3 digits
          Serial.write(bell);

          // get next char;
          continue;
        }; // end else - if

      } else {
        // unknown char
        Serial.write(bell);

        // get next char
        continue;
      }; // end else - elsif - if

    }; // end state 12;

  }; // end while


  // Function "P": Send PAGE
  if (state == -1) {

    //    address = 12345;
    //    addresssource = 123;
    //    repeat = 4;
    //    String textmsg_string = "This is a Test Message";
    //    textmsg_string.toCharArray(textmsg, 22);//


    Serial.print("address: ");
    Serial.println(address);

    Serial.print("addresssource: ");
    Serial.println(addresssource);

    Serial.print("repeat: ");
    Serial.println(repeat);

    Serial.print("message: ");
    Serial.println(textmsg);

    // create pocsag message
    // batch2 option = 0 (truncate message)
    // invert option1 (invert)

    //    rc = pocsag.CreatePocsag(address, addresssource, textmsg, 0, 1);
    rc = pocsag.CreatePocsag(address, addresssource, textmsg, 0, 0);

    if (!rc) {
      Serial.print("Error in createpocsag! Error: ");
      Serial.println(pocsag.GetError());
      // sleep 10 seconds // now 3
      delay(1000);
    } else {



      //      // Set up Timer1 for interrupts at 31.25 Hz
      //      cli(); //stop interrupts
      //      TCCR1A = 0;// set entire TCCR1A register to 0
      //      TCNT1  = 0;//initialize counter value to 0
      //      // set compare match register
      //      if (baud_rate == 3125)
      //      {
      //        OCR1A = 7971;// = (16*10^6) / (1*1024) - 1 (must be <65536)
      //        //OCR1A = 499;// = (16*10^6) / (1*1024) - 1 (must be <65536)
      //      }
      //      else if (baud_rate == 6250)
      //      {
      //        OCR1A = 250;
      //      }
      //      // turn on CTC mode, set CS12 and CS10 bits for 1024 prescaler
      //      //TCCR1B = (1 << WGM12) | (1 << CS12) | (1 << CS10);
      //
      //      // 64
      //      TCCR1B = (1 << WGM12) | (1 << CS11) | (1 << CS10);
      //      // enable timer compare interrupt
      //      TIMSK1 = (1 << OCIE1A);
      //      sei(); //allow interrupts


      // send at least once + repeat
      for (int l = -1; l < repeat; l++) {
        Serial.println("POCSAG SEND");

        //rf22.send((uint8_t *)pocsag.GetMsgPointer(), pocsag.GetSize());
        //rf22.waitPacketSent();

        si5351.output_enable(SI5351_CLK0, 1);
        Serial.println("Output TurnedOn");

        for (int z = 0; z < pocsag.GetSize(); z++) // for (int z = 0; z < pocsag.GetSize(); z++)
        {
          //          Serial.print(((uint8_t *)pocsag.GetMsgPointer())[z], BIN);
          for (int y = 7; y >= 0; y--) {
            //Serial.print(bitRead(((uint8_t *)pocsag.GetMsgPointer())[z], y));
            if (bitRead(((uint8_t *)pocsag.GetMsgPointer())[z], y))
              si5351.set_freq(freq, SI5351_CLK0);
            //              si5351.set_clock_source(SI5351_CLK1, SI5351_CLK_SRC_MS);
            else
              si5351.set_freq(freq + freq_deviation, SI5351_CLK0);
            //              si5351.set_clock_source(SI5351_CLK1, SI5351_CLK_SRC_MS0);
            // Pre set Delay
//                        delayMicroseconds(500);
//            delay(2);
          }
        }

        Serial.println("Output TurnedOff");
        si5351.output_enable(SI5351_CLK0, 0);

        Serial.print("\nTotalBytes Sent : ");
        Serial.println(pocsag.GetSize() * 8);

        delay(500);

      }; // end for

    }; // end else - if;
  }; // end function P (send PAGE)

  // function "F": change frequency

  //  if (state == -2) {
  //    float newfreq;
  //
  //    newfreq = ((float)freq1) + ((float)freq2) / 10000.0F; // f1 = MHz, F2 = 100 Hz
  //
  //    // ISM band: 434.050 to 434.800 and 863 to 870
  //    if ( ((newfreq >= 433.050F) && (newfreq <= 434.8F)) ||
  //         ((newfreq >= 863.0) && (newfreq < 870.0F)) ) {
  //      Serial.print("switching to new frequency: ");
  //      Serial.println(newfreq);
  //
  //      freq = newfreq;
  //      //rf22.setFrequency(freq, 0.05); // set frequency, AfcPullinRange not used (receive-only)
  //    } else {
  //      Serial.print("Error: invalid frequency (should be 433.050-434.800 or 853-8670 Mhz) ");
  //      Serial.println(newfreq);
  //    }; // end if
  //  }; // end function F (frequency)

}; // end main application



//ISR(TIMER1_COMPA_vect)
//{
//  if (symbol == 0)
//  {
//    if (zero_count)
//    {
//      psk_bit = 0;
//      zero_count--;
//    }
//    else
//    { ;
//      index++;
//
//      // Reset message pointer to beginning of string if at end
//      if (index >= mystring.length())
//      {
//        index = 0;
//      }
//
//      symbol = varicode[mystring.charAt(index)];
//    }
//  }
//  else
//  {
//    if (symbol & 0x8000) // 1 in the MSB position
//    {
//      psk_bit = 1;
//    }
//    else // 0 in the MSB position
//    {
//      psk_bit = 0;
//    }
//
//    symbol <<= 1;
//
//    if (symbol == 0)
//    {
//      zero_count = 2;
//    }
//    else
//    {
//      zero_count = 0;
//    }
//  }
//
//  set_vfo = 1;
//
//}





