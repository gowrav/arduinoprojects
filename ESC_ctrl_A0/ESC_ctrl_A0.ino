
//    Control Brushless Motor using Arduino
//
//    Ever thought to control Brushless Motor with ESC  without a Transmitter and Receiver or have a Project
//    in which you want to control Brushless Motor speed using a simple Circuit or Arduino,
//    then there is a way we can do it with Arduino Micro-controller.This involves the use of PWM signal
//    from Arduino to control brushless motor speed with an ESC.This will save you the money to buy a servo tester
//    or an RC Transmitter and receiver.So Let’s Get started!!
//
//    Things you need:
//    Arduino(I will be using a UNO)
//    Potentiometer
//    Lipo Battery(Which is able to supply Sufficient Current)
//    ESC(Make sure that the motor draws less current than mentioned on the ESC)
//    Brushless Motor(In my case an A2212)
//
//    Arduino Brushless Motor Code
//    In this code, we are simply mapping or referring the maximum(1023) and minimum(0)
//    Analog values at pin A0 to the required maximum(2000) and minimum(1000) values to
//    operate and control the speed of the ESC.You may require changing the max and min values
//    of the ESC i.e 1000 and 2000 to different values, in other words, you may need to calibrate it,
//    Because Different ESC’s may have a diffrent start point and end point.
//
//    Wiring up the Circuit
//
//    First, connect the three terminals of Brushless motor to the three terminals of the ESC.
//    Screw the Motor to a heavy wooden plank anything similar so that it remains stable at high RPM.
//    Download and Flash the code available at the bottom of the page to the Arduino using a USB cable(Code is explained in the further part of this page).
//    Connect the signal wire of ESC mostly white or yellow color to any PWM pin, Arduino,
//    I connected it to the D8 pin and specified it a Pin 8 in the Arduino Sketch.
//    You can use more than one pins for controlling many motors.
//
//    Connect the Potentiometer to the VCC or 5v pin of the Arduino and the Ground.
//    Connect the third terminal that is the variable pin to the Analog pin A0
//    You can power the Arduino using the BEC(Battery Eliminator Circuit)Present in your ESC.
//    To use the BEC just connect the red thick wire to the Vin Pin of Arduino.It can provide 5V.
//    Not all ESC’s have a BEC, in this case, you can use an external 5v power supply.
//    After Powering the Arduino now connect the Lipo battery to your ESC.
//    You are Done!!Now slowly turn the Potentiometer Knob to start and increase the speed of the Motor.


#include <Servo.h>//Using servo library to control ESC

Servo esc; //Creating a servo class with name as esc
int val = 0; //Creating a variable val

void setup()

{
  esc.attach(8); //Specify the esc signal pin,Here as D8
  esc.writeMicroseconds(1000); //initialize the signal to 1000
  Serial.begin(115200);
}

void loop()
{

  // val = analogRead(A0); //Read input from analog pin a0 and store in val
  while (Serial.available() > 0) {
    // look for the next valid integer in the incoming serial stream:
    int serailval = Serial.parseInt();
    // look for the newline. That's the end of your sentence:
    if (Serial.read() == '\n') {
      val = serailval;
    }
    delay(5);
    Serial.println(val);
  }

  int escval = map(val, 0, 1023, 1000, 2000); //mapping val to minimum and maximum(Change if needed)
  esc.writeMicroseconds(escval); //using val as the signal to esc
  Serial.println(escval);
}
