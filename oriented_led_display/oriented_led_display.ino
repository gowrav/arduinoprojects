#include <TimerOne.h>
#include <SoftwareSerial.h>
#include <EEPROM.h>
#include "default_font.h"

SoftwareSerial btSerial(10, 9); // RX, TX
//#define NO_UNITS 1  // number of display units used 
#define NO_SLAB  1
#define NO_SLICE  1
//#define NO_CHAR  4*NO_UNITS//4
#define REP_COUNT   2//4
#define CHAR_LINES   16//4
#define BS_LIMIT   8//4

int NO_UNITS=4;
int NO_CHAR = 2*NO_UNITS; // this is character per brick then multiplied with the total no of bricks

int LED_BRIGHTNESS =  400; // 0 to 1000 // 400 is optimum

bool DARKMODE_ENABLED = false;//set dark mode to true to invert light
bool BITSCROLL_ENABLED = true;//set dark mode to true to invert light

bool cp = true;// Check Password.... If the module is turned on, for the first time always asks for password and then proceeds



//String dispString = "PEEKSHOP";
//String content = "";
//
//float f = 123.456f;
//int cPath = 12;  // content storage address
//int bPath = 2;  // 112  brightness controll address
//int sPath = 4;  // 115 scrolling direction controll address , ic left or right, 0 means left to right and 1 means right to left
//int spPath = 6;  //  117 speed of the scrolling address
//int stylepath = 8;  //  display style address

String dispString = "www.geeksynergy.com";
String dispString1 = "FAILED";
String dispString2 = "Connected";
String dispString3 = "Reset Done";
String dispString4 = "Pas Req!";
String content = "";
String defaultPassword = "geek@123"; // can change the default pasword to your own
String currentPassword = "";
String qwerty = "";
String Direction = "Left";
int resetPin = A0; // digital pin to reset the password to default password


float f = 123.456f;
int cPath = 100;  // content storage address
int bPath = 2;  // 112  brightness controll address
int sPath = 4;  // 115 scrolling direction controll address , ic left or right, 0 means left to right and 1 means right to left
int spPath = 6;  //  117 speed of the scrolling address
int stylepath = 8;  //  display style address
int pwdpath = 12; // default password path
int currpath = 45; // current password path
int passlength = 90; // password length path
int unitpath = 97;
int passwordLength; // pasword length integer


int clock_sec = 00;
int clock_min = 00;
int clock_hrs = 00;

String hours = "";
String mints = "";
String secon = "";

String kk = "0";

int timerclock_sec = 00;
int timerclock_min = 00;
int timerclock_hrs = 00;

String timerhours = "";
String timermints = "";
String timersecon = "";

int cclock_hrs = 0, cclock_min = 0, cclock_sec = 0;

//int bVal = 72;  //  brightness value
//int sVal = 82;  //  scrolling direction value , ic left or right
//int spVal = 92;  //  speed of the scrolling value, it is integer value
int contLen = 0;
boolean getCont = true;
int sSpeed = 9;
int style = 0;  // 0 means static and 1 means scrolling


//int latchPin = A3;//8; //LT
//int clockPin = 13;//12; //SK
//int dataPin = 7;//11; //R1
////int dataPin2 = 11; //G1
//
//int dispAll_IO = 45;
//
//int en_74138 = A2;//2;
//
//int la_74138 = 5;//3;
//int lb_74138 = 6;//4;


//int lc_74138 = 5;
//int ld_74138 = 6;

//int k = 0;
//int lTimes = 2;

int latchPin = 8; //LT
int clockPin = 12; //SK
int dataPin = 11; //R1
//int dataPin2 = 11; //G1

int dispAll_IO = 45;

int en_74138 = 2;

int la_74138 = 3;
int lb_74138 = 4;


int lc_74138 = 5;
int ld_74138 = 6;

int k = 0;
int lTimes = 2;




void shiftOut(unsigned char dataOut)
{

  if (DARKMODE_ENABLED)
  {
    dataOut = ~dataOut;
  }

  for (int i = 7; i >= 0  ; i--)
  {
#if defined(__AVR_ATmega1280__) || defined(__AVR_ATmega2560__)
    // Fast Data Toggler
    PORTB &= ~(1 << (6));
    if (dataOut & (0x01 << i)) PORTB &= ~(1 << (5));
    else
      PORTB |= 1 << (5);
    PORTB |= 1 << (6);
#endif
    //#if defined(__AVR_ATmega328P__) || defined(__AVR_ATmega168__)
    //    //     SLOW Toggler
    //    digitalWrite(clockPin, LOW);
    //    if (dataOut & (0x01 << i))
    //      digitalWrite(dataPin, LOW);
    //    else
    //      digitalWrite(dataPin, HIGH);
    //    digitalWrite(clockPin, HIGH);
    //#endif
    //#if defined(__AVR_ATmega328P__) || defined(__AVR_ATmega168__)
    //    // Fast Data Toggler
    //    PORTB &= ~(1 << (4));
    //    if (dataOut & (0x01 << i)) PORTB &= ~(1 << (3));
    //    else
    //      PORTB |= 1 << (3);
    //    PORTB |= 1 << (4);
    //#endif
#if defined(__AVR_ATmega328P__) || defined(__AVR_ATmega168__)
    // Super Fast Data Toggler
    PORTB &= 0xEF;
    if (dataOut & (0x01 << i)) PORTB &= 0xF7;
    else
      PORTB |= 0x08;
    PORTB |= 0x10;
#endif
  }
}


void cyclePower(int l)
{

  //#if defined(__AVR_ATmega328P__) || defined(__AVR_ATmega168__)
  //    //     SLOW Toggler
  //
  //  digitalWrite(en_74138, LOW);
  //  digitalWrite(la_74138, l % 2 > 0);
  //  digitalWrite(lb_74138, l % 4 > 1);
  //  digitalWrite(en_74138, HIGH);
  //  delayMicroseconds(LED_BRIGHTNESS);
  //  digitalWrite(en_74138, LOW);
  //#endif
  //#if defined(__AVR_ATmega328P__) || defined(__AVR_ATmega168__)
  //    // Fast Data Toggler
  //    PORTB &= ~(1 << (4));
  //    if (dataOut & (0x01 << i)) PORTB &= ~(1 << (3));
  //    else
  //      PORTB |= 1 << (3);
  //    PORTB |= 1 << (4);
  //#endif

#if defined(__AVR_ATmega328P__) || defined(__AVR_ATmega168__)
  // Super Fast Data Toggler
  PORTD &= 0xFB;
  if (l % 2 > 0 )
    PORTD |= 0x08;
  else
    PORTD &= 0xF7;

  if (l % 4 > 1)
    PORTD |= 0x10;
  else
    PORTD &= 0xEF;

  PORTD |= 0x04;
  delayMicroseconds(LED_BRIGHTNESS);
  PORTD &= 0xFB;

#endif

}




uint8_t msg_board[] =
{
  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,
  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,
  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,
  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,
  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,
  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,
  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,
  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,
  
  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,
  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,
  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,
  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,
  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,
  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,
  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,
  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,
  
  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,
  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,
  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,
  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,
  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,
  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,
  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,
  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00,  0x00, 0x00
};


void display_msg(String eightchars, int delaySec)
{
  while (delaySec--) {
    display_msg(eightchars);

  }
}


void display_msg(String eightchars)
{  
   NO_CHAR = 2*NO_UNITS;
  // Reduce string length to 8
  if (eightchars.length() > NO_CHAR)
    eightchars = eightchars.substring(0, NO_CHAR);

  // Optimize string with spaces to 8
  if (eightchars.length() < NO_CHAR)
  {
    String temp = "";
    for (int i = 0; i < (NO_CHAR - eightchars.length()) / 2; i++)
      temp += " ";
    temp += eightchars;
    for (int i = 0; i < (((NO_CHAR - eightchars.length()) / 2) + eightchars.length() % 2); i++)
      temp += " ";
    eightchars = temp;
  }


  // Set msgBoardData here
  for (int y = 0; y < NO_CHAR; y++)
  
    for (int x = 0; x < CHAR_LINES; x++)
    {
      //msg_board[(CHAR_LINES * y) + x] = pgm_read_word_near(font8x16_basic + (eightchars.charAt(y) * CHAR_LINES) + x);
      for(int j=0; j < 16 ; j++)
      {
       msg_board[(CHAR_LINES * y) + x] = pgm_read_word_near(font8x16_basic + (eightchars.charAt(y) * CHAR_LINES) + x);        
      }
    }

//msg_board[CHAR_LINES*NO_CHAR] = msg_board[CHAR_LINES*NO_CHAR];
//Matrix.Transpose( msg_board,CHAR_LINES,NO_CHAR, msg_board);
Serial.print(msg_board[CHAR_LINES*NO_CHAR]);

  digitalWrite(en_74138, LOW);//Turn off display
  for (int i = 0; i < NO_SLAB * NO_SLICE; i++)
  {
    for (int l = 3; l >= 0 ; l--)
    {
      for (int k = 0; k < REP_COUNT; k++)
      {
        for (int m = 0; m < NO_CHAR ; m++)
        {
          shiftOut(msg_board[m * CHAR_LINES + ((4 * 3) + l)]);
          shiftOut(msg_board[m * CHAR_LINES + ((4 * 2) + l)]);
          shiftOut(msg_board[m * CHAR_LINES + ((4 * 1) + l)]);
          shiftOut(msg_board[m * CHAR_LINES + (l)]);
          PORTB &= 0xFE;
          PORTB |= 0x01;
          PORTB &= 0xFE;
        }
      }

      cyclePower(l);
    }
  }
}


void display_msgfilled()
{

  digitalWrite(en_74138, LOW);//Turn off display
  for (int i = 0; i < NO_SLAB * NO_SLICE; i++)
  {
    for (int l = 3; l >= 0 ; l--)
    {
      for (int k = 0; k < REP_COUNT; k++)
      {
        for (int m = 0; m < NO_CHAR ; m++)
        {
          int repeatfour = 4;

          while (repeatfour--)
            shiftOut(0xFF);

          PORTB &= 0xFE;
          PORTB |= 0x01;
          PORTB &= 0xFE;
          //delay(10);
        }
      }

      cyclePower(l);
    }
  }
}



void displayScroll_msg_right(String longString)
{
  NO_CHAR = 4*NO_UNITS;

    String spaces = "";
  for( int i =0; i<NO_CHAR;i++)
   spaces +=" ";

  longString = spaces+ longString;//+spaces; // Needs to be looked into regarding the jump of last part

  for (int m = longString.length(); m >0; m --)//for (int m = 0; m < longString.length(); m ++)
  {
    String shortString = "";
    if (m + NO_CHAR < longString.length())
      shortString = longString.substring(m, m + NO_CHAR+1);
    else
      shortString = longString.substring(m);

    int TEMP_BS_LIMIT = BS_LIMIT;
    if (BITSCROLL_ENABLED) {
      if (m + NO_CHAR < longString.length())
        shortString = longString.substring(m, m + NO_CHAR+1);
      else
        shortString = longString.substring(m);// Substituted m+1 with m
    }
    else
    {
      TEMP_BS_LIMIT = 1;
    }

    for (int bsc = TEMP_BS_LIMIT; bsc > 0 ; bsc--)//for (int bsc = 0; bsc < TEMP_BS_LIMIT ; bsc++)
    {
      // Set msgBoardData here
      for (int y = 0; y < NO_CHAR; y++)
        for (int x = 0; x < CHAR_LINES; x++)
        {
          //msg_board[(CHAR_LINES * y) + x] = pgm_read_word_near(font8x16_basic + (shortString.charAt(y) * CHAR_LINES) + x);
          msg_board[(CHAR_LINES * y) + x] = (unsigned char)pgm_read_word_near(font8x16_basic + (shortString.charAt(y) * CHAR_LINES) + x) << bsc;//disappearing
          msg_board[(CHAR_LINES * y) + x] |= (unsigned char)pgm_read_word_near(font8x16_basic + (shortString.charAt(y + 1) * CHAR_LINES) + x) >>(BS_LIMIT - bsc) ; //appearing
        }
      int k = sSpeed;
      if (BITSCROLL_ENABLED) {
        k = k / 3;
      }
      while (k--)
      {
        if (btSerial.available())
        {
          getBluetoothdata();
        }
        digitalWrite(en_74138, LOW);//Turn off display
        for (int i = 0; i < NO_SLAB * NO_SLICE; i++)
        {
          for (int l = 3; l >= 0 ; l--)
          {
            for (int m = 0; m < NO_CHAR; m++)
            {
              shiftOut(msg_board[m * CHAR_LINES + ((4 * 3) + l)]);
              shiftOut(msg_board[m * CHAR_LINES + ((4 * 2) + l)]);
              shiftOut(msg_board[m * CHAR_LINES + ((4 * 1) + l)]);
              shiftOut(msg_board[m * CHAR_LINES + (l)]);
              PORTB &= 0xFE;
              PORTB |= 0x01;
              PORTB &= 0xFE;
            }
            cyclePower(l);
          }
        }
      }
    }
  }
}

void displayScroll_msg_left(String longString)
{
  NO_CHAR = 4*NO_UNITS;
  String spaces = "";
  for( int i =0; i<NO_CHAR;i++)
   spaces +=" ";

  longString = spaces+ longString;//+spaces; // Needs to be looked into regarding the jump of last part

  for (int m = 0; m < longString.length(); m ++)
  {
    String shortString = "";
    if (m + NO_CHAR < longString.length())
      shortString = longString.substring(m, m + NO_CHAR+1);
    else
      shortString = longString.substring(m);

    int TEMP_BS_LIMIT = BS_LIMIT;
    if (BITSCROLL_ENABLED) {
      if (m + NO_CHAR < longString.length())
        shortString = longString.substring(m, m + NO_CHAR+1);
      else
        shortString = longString.substring(m);// Substituted m+1 with m
    }
    else
    {
      TEMP_BS_LIMIT = 1;
    }

    for (int bsc = 0; bsc < TEMP_BS_LIMIT ; bsc++)
    {
      // Set msgBoardData here
      for (int y = 0; y < NO_CHAR; y++)
        for (int x = 0; x < CHAR_LINES; x++)
        {
          //msg_board[(CHAR_LINES * y) + x] = pgm_read_word_near(font8x16_basic + (shortString.charAt(y) * CHAR_LINES) + x);
          msg_board[(CHAR_LINES * y) + x] = (unsigned char)pgm_read_word_near(font8x16_basic + (shortString.charAt(y) * CHAR_LINES) + x) << bsc;//disappearing
          msg_board[(CHAR_LINES * y) + x] |= (unsigned char)pgm_read_word_near(font8x16_basic + (shortString.charAt(y + 1) * CHAR_LINES) + x) >> (BS_LIMIT - bsc) ; //appearing
        }
      int k = sSpeed;
      if (BITSCROLL_ENABLED) {
        k = k/3;// / 3;
      }
      while (k--)
      {
        if (btSerial.available())
        {
          getBluetoothdata();
        }
        digitalWrite(en_74138, LOW);//Turn off display
        for (int i = NO_SLAB * NO_SLICE; i >0 ; i--)//for (int i = 0; i < NO_SLAB * NO_SLICE; i++)
        {
          for (int l = 3; l >= 0 ; l--)
          {
            for (int m = 0; m < NO_CHAR; m++)
            {
              shiftOut(msg_board[m * CHAR_LINES + ((4 * 3) + l)]);
              shiftOut(msg_board[m * CHAR_LINES + ((4 * 2) + l)]);
              shiftOut(msg_board[m * CHAR_LINES + ((4 * 1) + l)]);
              shiftOut(msg_board[m * CHAR_LINES + (l)]);
              PORTB &= 0xFE;
              PORTB |= 0x01;
              PORTB &= 0xFE;
            }
            cyclePower(l);
          }
        }
      }
    }
  }
}

void display_flush()
{
  PORTD &= 0xFB;
  for (int l = 0; l < NO_SLAB * NO_SLICE ; l++)
  {
    shiftOut(0);
    PORTB &= 0xFE;
    PORTB |= 0x01;
    PORTB &= 0xFE;
  }
  PORTD |= 0x04;
}


void display_flood()
{
  int delaySec1 = 300;
  while (delaySec1--) {
    display_msgfilled();
  }
}

void init_matrixDisplay()
{
  pinMode(latchPin, OUTPUT);
  pinMode(clockPin, OUTPUT);
  pinMode(dataPin, OUTPUT);
  pinMode(en_74138, OUTPUT);
  pinMode(la_74138, OUTPUT);
  pinMode(lb_74138, OUTPUT);
//  pinMode(lc_74138, OUTPUT);
//  pinMode(ld_74138, OUTPUT);

  pinMode(dispAll_IO, INPUT);

//  digitalWrite(lc_74138, LOW);
//  digitalWrite(ld_74138, LOW);
  digitalWrite(en_74138, LOW);
  display_flush();
}

boolean testLED = false;


 
void setup()
{
  Serial.begin(9600); Serial.setTimeout(50);
  btSerial.begin(9600); btSerial.setTimeout(50);

  NO_UNITS = EEPROM.read(unitpath);
  if(NO_UNITS == 0)
    NO_UNITS = 6;
   
  init_matrixDisplay();

  pinMode(resetPin, INPUT_PULLUP);

  Timer1.initialize(1000000);         // initialize timer1, and set a 1/2 second period
  Timer1.attachInterrupt(callback);

  display_flush();
//  displayScroll_msg_left(dispString);
//  for (int r = 0; r < 300; r++)
//  {
//    display_msg(dispString);
//  }
  contLen = dispString.length();
  if (EEPROM.read(cPath) < 3)
  {
    EEPROM.write(cPath, contLen);
    for (int i = 0; i < dispString.length(); i++)
    {
      int count = cPath + 1 + i;
      EEPROM.write(count, dispString.charAt(i));
    }
    EEPROM.write(spPath, 25);
    EEPROM.write(bPath, 80);
    EEPROM.write(sPath, 1);
    EEPROM.write(stylepath, 0);
  }

  for (int i = 0; i < defaultPassword.length(); i++)
  {
    int count = pwdpath + 1 + i;
    EEPROM.write(count, defaultPassword.charAt(i));
  }

  int checkLength = EEPROM.read(passlength);

  Serial.println("Password length after eeprom read is: " + checkLength);

  for (int i = 0; i < checkLength; i++)
  {
    int count = currpath + 1 + i;
    qwerty += char(EEPROM.read(count));
  }
  Serial.println(" Current password after eeprom read in setup option is: " + qwerty);
  currentPassword = qwerty;

  if (currentPassword == "")
  {
    currentPassword = defaultPassword ;
    passwordLength = currentPassword.length();

    Serial.println("Password length is :" + currentPassword.length());

    EEPROM.write(passlength, passwordLength);

    int checkLength = EEPROM.read(passlength);

    for (int i = 0; i < checkLength; i++)
    {
      int count = currpath + 1 + i;
      EEPROM.write(count, currentPassword.charAt(i));
    }
    Serial.println("default data written");
  }

}

void callback() {

  if (clock_hrs < 10)
    hours = kk + String(clock_hrs);
  else
    hours = String(clock_hrs);

  if (clock_min < 10)
    mints = kk + String(clock_min);
  else
    mints = String(clock_min);

  if (clock_sec < 10)
    secon = kk + String(clock_sec);
  else
    secon = String(clock_sec);

  clock_sec = clock_sec + 1;
  if (clock_sec == 60)
  {
    clock_sec = 00;
    clock_min = clock_min + 1;
  }
  if (clock_min == 60)
  {
    clock_min = 00;
    if (clock_hrs == 12)
      clock_hrs = 01;
    else clock_hrs = clock_hrs + 1;
  }


  if (timerclock_hrs < 10)
    timerhours = kk + String(timerclock_hrs);
  else
    timerhours = String(timerclock_hrs);

  if (timerclock_min < 10)
    timermints = kk + String(timerclock_min);
  else
    timermints = String(timerclock_min);

  if (timerclock_sec < 10)
    timersecon = kk + String(timerclock_sec);
  else
    timersecon = String(timerclock_sec);
  //    if((timerclock_sec == 00)&&(timerclock_min == 0)&&(timerclock_hrs == 00))
  //    {
  //      while(!btSerial.available())
  //    display_msg("Time up");
  //    }
  if (timerclock_sec > 0)
    timerclock_sec = timerclock_sec - 1;
  if (timerclock_sec == 0)
  {
    if (timerclock_min > 0)
    {
      timerclock_min = timerclock_min - 1;
      timerclock_sec = 59;
    }

    else if (timerclock_hrs > 0)
    {
      timerclock_hrs = timerclock_hrs - 1;
      timerclock_min = 59;
      timerclock_sec = 59;
    }

    else if (timerclock_min == 0)
    {
      if (timerclock_hrs > 0)
      {
        timerclock_min = 59;
        timerclock_hrs = timerclock_hrs - 1;
        timerclock_sec = 59;
      }
    }


  }

}

void simply()
{
  while (!btSerial.available())
    display_msg(hours + ":" + mints + ":" + secon);
}

void simplytimer()
{
  while (!btSerial.available())
    display_msg(timerhours + ":" + timermints + ":" + timersecon);
}


void loop() {
  int sensorVal = digitalRead(resetPin);
//    Serial.println(sSpeed);
  if (sensorVal == 0)
  {
    clearEEpromData();
    for (int i = 100 ; i < EEPROM.length() ; i++)
    {
      EEPROM.write(i, 0);
    }
    //     Serial.println("all data cleared");
  }
  // initialize all the value from eeprom before start.
  if (getCont) {
    getCont = false;
    contLen = EEPROM.read(cPath);
    content = "";
    for (int j = 0; j < contLen; j++)
    {
      int coV = cPath + 1 + j;
      content += char(EEPROM.read(coV));
    }
    dispString = content;
    sSpeed = EEPROM.read(spPath); // adding new value to scrolling speed from bluetooth.
    sSpeed = 200 - sSpeed * 2;
    LED_BRIGHTNESS = EEPROM.read(bPath); // adding new value to scrolling speed from bluetooth.
    LED_BRIGHTNESS = LED_BRIGHTNESS * 10;
    style = EEPROM.read(stylepath);
  }

  // read the data from bluetooth
  if (btSerial.available())
  {
    getBluetoothdata();
  }
  if (testLED) {
    display_flood();
  }
  else {
    if (style == 0)
    {
      display_msg(dispString, 100);
    }
    else
    {
      if(Direction == "Left")
      displayScroll_msg_left(dispString);
      if(Direction == "Right")
      displayScroll_msg_right(dispString);
    }
  }
}

void getBluetoothdata() {
  Serial.println(F("got some bluetooth data"));
  String dataRec = btSerial.readString();
  Serial.println(dataRec);
  String contCheck = dataRec.substring(0, dataRec.indexOf('~'));
  Serial.println(contCheck);
  Serial.print(F("cp: "));
  Serial.println(cp);

  if ((cp == true) && (contCheck != "password"))
  {
    while (!btSerial.available())
      display_msg(dispString4);
  }

  if (contCheck.equals("resetWatch"))
  {
    while (!btSerial.available())
      display_msg("00:00:00");
    Timer1.stop();
  }
  if (contCheck.equals("pauseWatch"))
  {
    Timer1.stop();
    String watch_hours, watch_mints, watch_secon;


    cclock_hrs = clock_hrs;
    cclock_min = clock_min;
    cclock_sec = clock_sec;

    if (clock_hrs < 10)
      watch_hours = kk + String(cclock_hrs);
    else
      watch_hours = String(cclock_hrs);

    if (clock_min < 10)
      watch_mints = kk + String(cclock_min);
    else
      watch_mints = String(cclock_min);

    if (clock_sec < 10)
      watch_secon = kk + String(cclock_sec);
    else
      watch_secon = String(cclock_sec);

    while (!btSerial.available())
      display_msg(watch_hours + ":" + watch_mints + ":" + watch_secon);
  }

  if (contCheck.equals("startWatch"))
  {
    Timer1.start();
    String hourss = (dataRec.substring((dataRec.indexOf('~') + 1), dataRec.indexOf(':')));
    String mintss = (dataRec.substring((dataRec.indexOf(':') + 1), dataRec.indexOf(':') + 3));
    String secoss = (dataRec.substring((dataRec.indexOf(':') + 4), dataRec.indexOf(':') + 6));
    clock_hrs = hourss.toInt();
    clock_min = mintss.toInt();
    clock_sec = secoss.toInt();
    simply();
  }

  if (contCheck.equals("time"))
  {
    Timer1.start();
    String hourss = (dataRec.substring((dataRec.indexOf('~') + 1), dataRec.indexOf(':')));
    String mintss = (dataRec.substring((dataRec.indexOf(':') + 1), dataRec.indexOf(':') + 3));
    String secoss = (dataRec.substring((dataRec.indexOf(':') + 4), dataRec.indexOf(':') + 6));
    Serial.print(F("hours: "));
    Serial.println( hourss);
    Serial.print(F("min: "));
    Serial.println(mintss);
    Serial.print(F("secon: "));
    Serial.println(secoss);
    //    int addition = hourss.toInt() + mintss.toInt() + secoss.toInt();
    //    Serial.println("adition: " + addition);
    clock_hrs = hourss.toInt();
    if (clock_hrs > 12)
      clock_hrs = clock_hrs - 12;
    clock_min = mintss.toInt();
    clock_sec = secoss.toInt() + 1;
    simply();

  }

  if (contCheck.equals("timer"))
  {
    Timer1.start();
    String hourss = (dataRec.substring((dataRec.indexOf('~') + 1), dataRec.indexOf(':')));
    String mintss = (dataRec.substring((dataRec.indexOf(':') + 1), dataRec.indexOf(':') + 3));
    String secoss = (dataRec.substring((dataRec.indexOf(':') + 4), dataRec.indexOf(':') + 6));
    Serial.print(F("hours: "));
    Serial.println(hourss);
    Serial.print(F("min: "));
    Serial.println( mintss);
    Serial.print(F("secon: "));
    Serial.println(secoss);
    //    int addition = hourss.toInt() + mintss.toInt() + secoss.toInt();
    //    Serial.println("adition: " + addition);
    timerclock_hrs = hourss.toInt();
    //    if(clock_hrs>12)
    //    clock_hrs = clock_hrs-12;
    timerclock_min = mintss.toInt();
    timerclock_sec = secoss.toInt();
    simplytimer();

  }
  if (contCheck.equals("Style"))
  {
    style = dataRec.substring(dataRec.indexOf('~') + 1).toInt();
    setDataAttribute(style, stylepath);   // here we have decide display style, ic static or scrolling
  }

  if (contCheck.equals("old"))      // Condition becomes true whenever reset option is given
  {


    String oldPassword = (dataRec.substring((dataRec.indexOf('~') + 1), dataRec.indexOf(',')));
    Serial.print(F("old pass: "));
    Serial.println(oldPassword);

    Serial.println(dataRec);
    
    String newPassword = (dataRec.substring((dataRec.indexOf(',') + 5), dataRec.length()));
   
    Serial.print(F("new pass: "));
    Serial.println(newPassword);

    if (oldPassword == currentPassword)
    {
      btSerial.print("true");

      currentPassword = newPassword;

      passwordLength = currentPassword.length();

      Serial.print(F("Password length is :"));
      Serial.println("Password length is :" + currentPassword.length());

      EEPROM.write(passlength, passwordLength);

      int checkLength = EEPROM.read(passlength);

      Serial.print(F("Password length after eeprom read is: "));
      Serial.println(checkLength);

      for (int i = 0; i < checkLength; i++)
      {
        int count = currpath + 1 + i;
        EEPROM.write(count, currentPassword.charAt(i));
      }
      qwerty = "";
      for (int i = 0; i < checkLength; i++)
      {
        int count = currpath + 1 + i;
        qwerty += char(EEPROM.read(count));
      }
      Serial.print(F(" Current password after eeprom read in reset password option is: "));
      Serial.println(qwerty);

      for (int r = 0; r < 300; r++)
      {
        display_msg(dispString3);
      }


    }
    else
    {
      btSerial.print("false");
    }
  }




  if (contCheck.equals("password"))  // Condition becomes true whenever user opens the app
  {
    String currentPasswordd;
    String key = (dataRec.substring((dataRec.indexOf('~') + 1), dataRec.length()));
    Serial.println(key);
    Serial.print(F("current password is: " ));
    Serial.println(currentPassword);

    int checkLength = EEPROM.read(passlength);
    Serial.print(F("Password length after eeprom read in password check is: " ));
    Serial.println(checkLength);
    checkLength = EEPROM.read(passlength);
    qwerty = "";

    for (int i = 0; i < checkLength; i++)
    {
      int count = currpath + 1 + i;
      qwerty += char(EEPROM.read(count));
    }
    Serial.print(F("current password after eeprom read in first time app open is: " ));
    Serial.println(qwerty);
    if (key != qwerty)
    {
      btSerial.print("false");
      for (int r = 0; r < 300; r++)
      {
        display_msg(dispString1);
      }
    }
    else
    {
      btSerial.print("true");
      for (int r = 0; r < 300; r++)
      {
        display_msg(dispString2);
      }
      cp = false;
      Serial.print(F("cp: "));
      Serial.println(cp);

    }


  }


  if (contCheck.equals("Content"))
  {
    changeContent(dataRec.substring(dataRec.indexOf('~') + 1), cPath); // here cPath for content address, it will change as per the bluetooth data
  }



  if (contCheck.equals("Brightness"))
  {
    LED_BRIGHTNESS = dataRec.substring(dataRec.lastIndexOf('~') + 1).toInt() * 15;
    setDataAttribute(dataRec.substring(dataRec.lastIndexOf('~') + 1).toInt(), bPath);
  }
  if (contCheck.equals("ON"))
  {
    testLEDdisplay();
  }
  if (contCheck.equals("OFF"))
  {
    testLED = false;
  }
  if (contCheck.equals("SSpeed"))
  {
    sSpeed = 200 - dataRec.substring(dataRec.lastIndexOf('~') + 1).toInt() * 2;
    setDataAttribute(dataRec.substring(dataRec.lastIndexOf('~') + 1).toInt(), spPath);
  }
  if (contCheck.equals("Panel"))
  {
    NO_UNITS = dataRec.substring(dataRec.lastIndexOf('~') + 1).toInt();
    EEPROM.write(unitpath, NO_UNITS);
    
//    Serial.println(no_units);
   
  }
  if (contCheck.equals("Mood"))
  {
    String Mood = (dataRec.substring((dataRec.indexOf('~') + 1), dataRec.length()));
    Serial.println(Mood);
    if(Mood == "on")
    DARKMODE_ENABLED = true;
    if(Mood == "off")
    DARKMODE_ENABLED = false;
  }
  if (contCheck.equals("Direction"))
  {
    Direction = (dataRec.substring((dataRec.indexOf('~') + 1), dataRec.length()));
  }

}

void testLEDdisplay() {
  testLED = true;
  display_msg("TEST LED", 100);
  display_flood();
}
void changeContent(String data, int pathLen)
{
  getCont = true;
  dispString = data;
  int cLen = data.length();
  EEPROM.write(pathLen, cLen);
  for (int i = 0; i < data.length(); i++)
  {
    int count = pathLen + 1 + i;
    EEPROM.write(count, data.charAt(i));
  }
}


void setDataAttribute(int data, int pathLen)
{
  EEPROM.write(pathLen, data);
}

void clearEEpromData()
{
  for (int i = 0 ; i < EEPROM.length() ; i++)
  {
    EEPROM.write(i, 0);
  }
}
