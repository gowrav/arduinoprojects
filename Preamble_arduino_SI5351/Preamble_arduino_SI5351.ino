//timer interrupts
//by Amanda Ghassaei
//June 2012
//https://www.instructables.com/id/Arduino-Timer-Interrupts/

/*
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.

*/



#include <SPI.h>
#include "si5351.h"
#include "Wire.h"

Si5351 si5351;

// frequency
unsigned long freq = 6960000000ULL;
unsigned long freq_deviation = 20000ULL;

//timer setup for timer0, timer1, and timer2.
//For arduino uno or any board with ATMEL 328/168.. diecimila, duemilanove, lilypad, nano, mini...

//this code will enable all three arduino timer interrupts.
//timer0 will interrupt at 2kHz
//timer1 will interrupt at 1Hz
//timer2 will interrupt at 8kHz

//storage variables
boolean toggle0 = 0;
boolean prevValue = 0;

void setup() {

  // Start serial and initialize the Si5351
  Wire.begin();
  Wire.setClock(400000L);
  Serial.begin(115200);

  Serial.println("Init SI5351");
  si5351.init(SI5351_CRYSTAL_LOAD_6PF, 0, 0);

  // Set CLK0 to output freq MHz
  si5351.set_freq(freq, SI5351_CLK0);
  si5351.output_enable(SI5351_CLK0, 1);

  si5351.update_status();
  delay(500);

  // Read the Status Register and print it every 10 seconds
  Serial.print("SYS_INIT: ");
  Serial.print(si5351.dev_status.SYS_INIT);
  Serial.print("  LOL_A: ");
  Serial.print(si5351.dev_status.LOL_A);
  Serial.print("  LOL_B: ");
  Serial.print(si5351.dev_status.LOL_B);
  Serial.print("  LOS: ");
  Serial.print(si5351.dev_status.LOS);
  Serial.print("  REVID: ");
  Serial.println(si5351.dev_status.REVID);


  pinMode(13, OUTPUT);

  cli();//stop interrupts

  //  ////set timer0 interrupt at 2kHz
  //  TCCR0A = 0;// set entire TCCR2A register to 0
  //  TCCR0B = 0;// same for TCCR2B
  //  TCNT0  = 0;//initialize counter value to 0
  //  // set compare match register for 2khz increments
  //  //OCR0A = 124;// = (16*10^6) / (2000*64) - 1 (must be <256)
  //  OCR0A = 207;// = (16*10^6) / (1200*64) - 1 (must be <256)
  //  // turn on CTC mode
  //  TCCR0A |= (1 << WGM01);
  //  // Set CS01 and CS00 bits for 64 prescaler
  //  TCCR0B |= (1 << CS01) | (1 << CS00);
  //  // enable timer compare interrupt
  //  TIMSK0 |= (1 << OCIE0A);
  //  //


  //set timer1 interrupt at 1Hz
  TCCR1A = 0;// set entire TCCR1A register to 0
  TCCR1B = 0;// same for TCCR1B
  TCNT1  = 0;//initialize counter value to 0
  // set compare match register for 1hz increments
  //  OCR1A = 15624;// = (16*10^6) / (1*1024) - 1 (must be <65536)
  //  OCR1A = 7811;// = (16*10^6) / (2*1024) - 1 (must be <65536)
  //  OCR1A = 1561;// = (16*10^6) / (10*1024) - 1 (must be <65536)
  //  OCR1A = 155;// = (16*10^6) / (100*1024) - 1 (must be <65536)
  //  OCR1A = 29;// = (16*10^6) / (512*1024) - 1 (must be <65536)
  OCR1A = 14;// = (16*10^6) / (512*1024) - 1 (must be <65536)
  // turn on CTC mode
  TCCR1B |= (1 << WGM12);
  // Set CS12 and CS10 bits for 1024 prescaler
  TCCR1B |= (1 << CS12) | (1 << CS10);
  // enable timer compare interrupt
  TIMSK1 |= (1 << OCIE1A);

  //  //set timer2 interrupt at 8kHz
  //  TCCR2A = 0;// set entire TCCR2A register to 0
  //  TCCR2B = 0;// same for TCCR2B
  //  TCNT2  = 0;//initialize counter value to 0
  //  // set compare match register for 8khz increments
  //  OCR2A = 249;// = (16*10^6) / (8000*8) - 1 (must be <256)
  //  // turn on CTC mode
  //  TCCR2A |= (1 << WGM21);
  //  // Set CS21 bit for 8 prescaler
  //  TCCR2B |= (1 << CS21);
  //  // enable timer compare interrupt
  //  TIMSK2 |= (1 << OCIE2A);

  sei();//allow interrupts

}//end setup


//
//ISR(TIMER0_COMPA_vect) { //timer0 interrupt 2kHz toggles pin 8
//  //generates pulse wave of frequency 2kHz/2 = 1kHz (takes two cycles for full wave- toggle high then toggle low)
//  if (toggle0) {
//    digitalWrite(13, HIGH);
//    si5351.set_freq(freq, SI5351_CLK0);
//    toggle0 = 0;
//  }
//  else {
//    digitalWrite(13, LOW);
//    si5351.set_freq(freq + freq_deviation, SI5351_CLK0);
//    toggle0 = 1;
//  }
//}


ISR(TIMER1_COMPA_vect) { //timer1 interrupt 1Hz toggles pin 13 (LED)
  //generates pulse wave of frequency 1Hz/2 = 0.5kHz (takes two cycles for full wave- toggle high then toggle low)
  if (toggle0) {
    digitalWrite(13, HIGH);
    toggle0 = 0;
  }
  else {
    digitalWrite(13, LOW);
    toggle0 = 1;
  }
}

//ISR(TIMER2_COMPA_vect) { //timer1 interrupt 8kHz toggles pin 9
//  //generates pulse wave of frequency 8kHz/2 = 4kHz (takes two cycles for full wave- toggle high then toggle low)
//  if (toggle0) {
//    digitalWrite(13, HIGH);
//    //    si5351.set_freq(freq, SI5351_CLK0);
//    toggle0 = 0;
//  }
//  else {
//    digitalWrite(13, LOW);
//    //    si5351.set_freq(freq + freq_deviation, SI5351_CLK0);
//    toggle0 = 1;
//  }
//}


void loop() {
  //do other things here

  if (toggle0) {
    if (prevValue != toggle0)
      si5351.set_freq(freq, SI5351_CLK0);
  }
  else {
    if (prevValue != toggle0)
      si5351.set_freq(freq + freq_deviation, SI5351_CLK0);
  }
  prevValue = toggle0;

}
