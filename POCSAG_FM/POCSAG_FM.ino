/**
   This example demonstrates abilities of the XantoKT0803 library.

*/
#include <XantoI2C.h>
#include <Pocsag.h>
#include <LiquidCrystal.h>

#include "XantoKT0803L.h"

const uint8_t PIN_SCL = A5;//8;
const uint8_t PIN_SDA = A4;//9;

XantoKT0803L fm(PIN_SCL, PIN_SDA);

char snprintf_buffer[19] = "0x00 76543210 0x00";

Pocsag pocsag;


// initialize the library by associating any needed LCD interface pin
// with the arduino pin number it is connected to
const int rs = 7, en = 6, d4 = 5, d5 = 4, d6 = 3, d7 = 2;
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);

void setup() {
  delay(100);
  Serial.begin(115200);
  printUsage();
  
  
  // set up the LCD's number of columns and rows:
  lcd.begin(16, 2);
  // Print a message to the LCD.
  lcd.print("AiRpaper");
  //setFrequency(997);
  readAndPrintAllRegisters();

}

void printUsage() {
  Serial.println("Usage:");
  Serial.println(" -Write register: W register_address_hex value_hex");
  Serial.println(" -Read register: R register_address_hex");
  Serial.println(" -Set Frequency: F frequency*10");
  Serial.println(" -Print all registers: E");
  Serial.println(" -Mute: M");
  Serial.println(" -Unmute: U");
  Serial.println("POCSAG Format:");
  Serial.println("P <address> <source> <repeat> <message>");

  Serial.println("E.g.: \"W 0x02 0x40\" - write value=0x40 to the register with address=0x02");
  Serial.println("E.g.: \"R 0x02\" - read the register with address=0x02");
  Serial.println("E.g.: \"F 997\" - set radio frequency=99.7MHz");
  Serial.println("E.g.: \"P 123456 1234 1 This is a Test Message.\" - set radio frequency=99.7MHz");

}

void printErrorIfExists() {
  if (fm.error > 0) {
    //Serial.print("Error: ");
    //Serial.println(fm.error);
    fm.error = 0;
  }
}

void printRegister(uint8_t register_address, uint8_t value) {
  snprintf(snprintf_buffer, 19, "0x%02X %d%d%d%d%d%d%d%d 0x%02X",
           register_address,
           bitRead(value, 7),
           bitRead(value, 6),
           bitRead(value, 5),
           bitRead(value, 4),
           bitRead(value, 3),
           bitRead(value, 2),
           bitRead(value, 1),
           bitRead(value, 0),
           value
          );
  //Serial.println(snprintf_buffer);
}

void readAndPrintRegister(uint8_t register_address) {
  Serial.print("Read: ");
  Serial.println(register_address, HEX);

  uint8_t value = fm.read(register_address);
  printErrorIfExists();

  Serial.println("Register address, BIN value, HEX value:");
  printRegister(register_address, value);
}

void readAndPrintAllRegisters() {
  Serial.println("Read all: ");

  Serial.println("Register address, BIN value, HEX value:");
  for (uint8_t i = 0; i < fm.getRegistersCount(); i++) {
    uint8_t value = fm.read(fm.getRegisters()[i]);
    printErrorIfExists();
    printRegister(fm.getRegisters()[i], value);
  }
  Serial.println("Done");
}

void writeAndPrintRegister(uint8_t register_address, uint8_t value) {
  Serial.print("Write: ");
  Serial.print(register_address, HEX);
  Serial.print(" ");
  Serial.println(value, HEX);

  value = fm.write(register_address, value);
  printErrorIfExists();

  Serial.println("Register address, BIN value, HEX value:");
  printRegister(register_address, value);

  value = fm.read(register_address);
  printErrorIfExists();

  Serial.println("Register address, BIN value, HEX value:");
  printRegister(register_address, value);
}

void setFrequency(uint16_t frequency) {
  Serial.print("Set frequency: ");
  Serial.print(frequency / 10.0);
  Serial.println("MHz");

  fm.setFrequency(frequency);
  // set the cursor to column 0, line 1
  // (note: line 1 is the second row, since counting begins with 0):
  lcd.setCursor(0, 1);
  // print the number of seconds since reset:
  lcd.print("Freq: ");
  lcd.print(frequency / 10.0);
  lcd.print("MHz");
}

void loop() {

  //vars
  int rc;
  int state; // state machine for CLI input

  //char bell = 0x07;// bell = ascii 0x07

  // data
  long int address;
  int addresssource;
  int repeat;
  char textmsg[42]; // to store text message;
  int msgsize;



  // init var
  state = 0;
  address = 0;
  addresssource = 0;
  msgsize = 0;


  while (state >= 0) {

    while (!Serial.available()) {
    }; // end while // loop until we get a character from serial input

    char c = Serial.read();

    // break out on ESC
    if (c == 0x1b) {
      state = -999;
      break;
    }; // end if

    // state machine
    if (state == 0) {
      // state 0: wait for command
      // P = PAGE
      // F = FREQUENCY
      if ((c == 'p') || (c == 'P')) {
        // ok, got our "p" -> go to state 1
        state = 1;
        // echo back char
        //Serial.write(c);
      } else {

        // ok, got our "f" -> go to state 1
        state = 10;
        // echo back char
        //Serial.write(c);

        if (c == 'W') {
          String str = Serial.readString();

          int register_address, value;
          sscanf(str.c_str(), "%i %i", &register_address, &value);
          writeAndPrintRegister(register_address, value);

        } else if (c == 'R') {
          String str = Serial.readString();

          int register_address;
          sscanf(str.c_str(), "%i %i", &register_address);

          readAndPrintRegister(register_address);

        } else if (c == 'F') {

          setFrequency(Serial.parseInt());

        } else if (c == 'E') {

          readAndPrintAllRegisters();

        } else if (c == 'M') {

          Serial.println("Mute");
          fm.mute(1);
          printErrorIfExists();

        } else if (c == 'U') {

          Serial.println("Unmute");
          fm.mute(0);
          printErrorIfExists();

        } else {
          //          Serial.write(bell);
          //Serial.println("Unknown command");
          // printUsage();
        }
        break;
      }
      // get next char
      continue;
    }; // end state 0

    // state 1: space (" ") or first digit of address ("0" to "9")
    if (state == 1) {
      if (c == ' ') {
        // space -> go to state 2 and get next char
        state = 2;

        // echo back char
        //Serial.write(c);

        // get next char
        continue;
      } else if ((c >= '0') && (c <= '9')) {
        // digit -> first digit of address. Go to state 2 and process
        state = 2;

        // continue to state 2 without fetching next char
      } else {
        // error: echo "bell"
        //        Serial.write(bell);

        // get next char
        continue;
      }; // end else - if
    };  // end state 1

    // state 2: address ("0" to "9")
    if (state == 2) {
      if ((c >= '0') && (c <= '9')) {
        long int newaddress;

        newaddress = address * 10 + (c - '0');

        if (newaddress <= 0x1FFFFF) {
          // valid address
          address = newaddress;

          //Serial.write(c);
        } else {
          // address to high. Send "beep"
          //          Serial.write(bell);
        }; // end else - if

      } else if (c == ' ') {
        // received space, go to next field (address source)
        //Serial.write(c);
        state = 3;
      } else {
        // error: echo "bell"
        //        Serial.write(bell);
      }; // end else - elsif - if

      // get next char
      continue;
    }; // end state 2

    // state 3: address source: one single digit from 0 to 3
    if (state == 3) {
      if ((c >= '0') && (c <= '3')) {
        addresssource = c - '0';
        //Serial.write(c);

        state = 4;
      } else {
        // invalid: sound bell
        //        Serial.write(bell);
      }; // end if

      // get next char
      continue;
    }; // end state 3


    // state 4: space between source and repeat
    if (state == 4) {
      if (c == ' ') {
        //Serial.write(c);

        state = 6; // go from state 4 to state 6
        // (No state 5, callsign removed)
      } else {
        // invalid: sound bell
        //        Serial.write(bell);
      }; // end if

      // get next char
      continue;
    }; // end state 4


    // state 5: callsign: REMOVED in non-ham version
    // state 6: repeat: 1-digit value between 0 and 9
    if (state == 6) {
      if ((c >= '0') && (c <= '9')) {
        //Serial.write(c);
        repeat = c - '0';

        // move to next state
        state = 7;
      } else {
        //        Serial.write(bell);
      }; // end if

      // get next char
      continue;
    }; // end state 6

    // state 7: space between repeat and message
    if (state == 7) {
      if (c == ' ') {
        //Serial.write(c);

        // move to next state
        state = 8;
      } else {
        // invalid char
        //        Serial.write(bell);
      }; // end else - if

      // get next char
      continue;
    }; // end state 7


    // state 8: message, up to 40 chars, terminate with cr (0x0d) or lf (0x0a)
    if (state == 8) {
      // accepted is everything between space (ascii 0x20) and ~ (ascii 0x7e)
      if ((c >= 0x20) && (c <= 0x7e)) {
        // accept up to 40 chars
        if (msgsize < 40) {
          //Serial.write(c);

          textmsg[msgsize] = c;
          msgsize++;
        } else {
          // to long
          //          Serial.write(bell);
        }; // end else - if

      } else if ((c == 0x0a) || (c == 0x0d)) {
        // done
        //Serial.println("");
        // add terminating NULL
        textmsg[msgsize] = 0x00;
        // break out of loop
        state = -1;
        break;

      } else {
        // invalid char
        //        Serial.write(bell);
      }; // end else - elsif - if

      // get next char
      continue;
    }; // end state 8;

  }

  // Function "P": Send PAGE
  if (state == -1) {

    //    Serial.print("address: ");
    //    Serial.println(address);
    //
    //    Serial.print("addresssource: ");
    //    Serial.println(addresssource);
    //
    //    Serial.print("repeat: ");
    //    Serial.println(repeat);
    //
    //    Serial.print("message: ");
    //    Serial.println(textmsg);

    rc = pocsag.CreatePocsag(address, addresssource, textmsg, 0, 0);

    if (!rc) {
      Serial.print("Error in createpocsag! Error: ");
      Serial.println(pocsag.GetError());
      // sleep 10 seconds // now 3
    } else {
      //Serial.print('#');
      // send at least once + repeat
      //for (int l = -1; l < repeat; l++) {
      for (int z = 0; z < pocsag.GetSize(); z++) // for (int z = 0; z < pocsag.GetSize(); z++)
      {
        //          Serial.print((char)((uint8_t *)pocsag.GetMsgPointer())[z]);
        for (int y = 7; y >= 0; y--) {          //Serial.print(((uint8_t *)pocsag.GetMsgPointer())[z], BIN);
          Serial.print(bitRead(((uint8_t *)pocsag.GetMsgPointer())[z], y));
        }
      }
      //}; // end for
      Serial.print('#');
    }; // end else - if;
  }; // end function P (send PAGE)


}


