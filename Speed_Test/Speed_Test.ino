

int motor_speed_pin = 8;
int motor_direction_pin = 9;
int motor_speed = 255;
// the setup routine runs once when you press reset:
void setup() {
  pinMode(motor_speed_pin, OUTPUT);
  pinMode(motor_direction_pin, OUTPUT);
}

void loop() {
  digitalWrite(motor_direction_pin, HIGH);
  analogWrite(motor_speed_pin, motor_speed);
  delay(1000);
  analogWrite(motor_speed_pin, 0);
  delay(500);
  digitalWrite(motor_direction_pin, LOW);
  analogWrite(motor_speed_pin, motor_speed);
  delay(1000);
  analogWrite(motor_speed_pin, 0);
  delay(500);
}
