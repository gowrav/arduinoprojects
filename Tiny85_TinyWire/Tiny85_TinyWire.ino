/* ATtiny85 as an I2C Master  Ex1          BroHogan                      1/21/11
   I2C master reading DS1621 temperature sensor. (display with leds)
   SETUP:
   ATtiny Pin 1 = (RESET) N/U                      ATtiny Pin 2 = (D3) LED3
   ATtiny Pin 3 = (D4) to LED1                     ATtiny Pin 4 = GND
   ATtiny Pin 5 = SDA on DS1621                    ATtiny Pin 6 = (D1) to LED2
   ATtiny Pin 7 = SCK on DS1621                    ATtiny Pin 8 = VCC (2.7-5.5V)
   NOTE! - It's very important to use pullups on the SDA & SCL lines!
   DS1621 wired per data sheet. This ex assumes A0-A2 are set LOW for an addeess of 0x48
   TinyWireM USAGE & CREDITS: - see TinyWireM.h
   NOTES:
   The ATtiny85 + DS1621 draws 1.7mA @5V when leds are not on and not reading temp.
   Using sleep mode, they draw .2 @5V @ idle - see http://brownsofa.org/blog/archives/261
*/

#include <TinyWireM.h>   // I2C Master lib for ATTinys which use USI
#include <SoftwareSerial.h>

SoftwareSerial tinySerial(3, 4);
int pulse = 1;


void setup()
{
  pinMode(pulse, OUTPUT);
  tinySerial.begin(115200);
  tinySerial.println("\nI2C Scanner");
  TinyWireM.begin();                    // initialize I2C lib
}


void loop()
{

  byte error, address;
  int nDevices;

  tinySerial.println("Scanning...");

  nDevices = 0;
  for (address = 1; address < 127; address++ )
  {
    // The i2c_scanner uses the return value of
    // the Write.endTransmisstion to see if
    // a device did acknowledge to the address.
    TinyWireM.beginTransmission(address);
    error = TinyWireM.endTransmission();          // Send to the slave

    if (error == 0)
    {
      tinySerial.print("I2C device found at address 0x");
      if (address < 16)
        tinySerial.print("0");
      tinySerial.print(address, HEX);
      tinySerial.println("  !");
      nDevices++;
    }
    else if (error == 4)
    {
      tinySerial.print("Unknown error at address 0x");
      if (address < 16)
        tinySerial.print("0");
      tinySerial.println(address, HEX);
    }
//    else
//    {
//      tinySerial.print("Error ");
//      tinySerial.print(error);
//      tinySerial.print(" at address 0x");
//      tinySerial.println(address, HEX);
//    }
  }
  if (nDevices == 0)
    tinySerial.println("No I2C devices found\n");
  else
    tinySerial.println("done\n");

  Blink(pulse, 5);

}


void Blink(byte led, byte times) { // poor man's GUI
  for (byte i = 0; i < times; i++) {
    digitalWrite(led, HIGH);
    delay (200);
    digitalWrite(led, LOW);
    delay (100);
  }
}
