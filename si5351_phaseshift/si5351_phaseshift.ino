// put your setup code here, to run once:

// We will output 14.1 MHz on CLK0 and CLK1.
// A PLLA frequency of 705 MHz was chosen to give an even
// divisor by 14.1 MHz.
#include "si5351.h"
#include "Wire.h"

Si5351 si5351;

unsigned long long freq = 1410000000ULL;
unsigned long long pll_freq = 70500000000ULL;

void setup() {

  // Start serial and initialize the Si5351
  Wire.begin();
  Wire.setClock(400000L);

  // Set CLK0 and CLK1 to output 14.1 MHz with a fixed PLL frequency
  si5351.set_freq_manual(freq, pll_freq, SI5351_CLK0);
  si5351.set_freq_manual(freq, pll_freq, SI5351_CLK1);

  // Now we can set CLK1 to have a 90 deg phase shift by entering
  // 50 in the CLK1 phase register, since the ratio of the PLL to
  // the clock frequency is 50.
  si5351.set_phase(SI5351_CLK0, 0);
  si5351.set_phase(SI5351_CLK1, 50);

  // We need to reset the PLL before they will be in phase alignment
  si5351.pll_reset(SI5351_PLLA);

}

void loop() {
  // put your main code here, to run repeatedly:

}

