#include <USIWire.h>
#include <SoftwareSerial.h>
#include "si5351-avr-tiny-minimal.h"

SoftwareSerial tinySerial(3, 4);

void setup()
{
  tinySerial.begin(9600);
  tinySerial.println("\nI2C Scanner");
  Wire.begin();


  si5351_init(SI5351_CRYSTAL_LOAD_8PF, 0);
  si5351_set_freq(45000000UL, SI5351_CLK0);
  //  si5351_drive_strength(SI5351_CLK0, SI5351_DRIVE_4MA);
  //  si5351_set_correction(-900);
}


void loop()
{

  delay(500);
  si5351_set_clock_invert(SI5351_CLK0, 0);
  delay(500);
  si5351_set_clock_invert(SI5351_CLK0, 1);
  //  byte error, address;
  //  int nDevices;
  //
  //  tinySerial.println("Scanning...");
  //
  //  nDevices = 0;
  //  for (address = 1; address < 127; address++ )
  //  {
  //    // The i2c_scanner uses the return value of
  //    // the Write.endTransmisstion to see if
  //    // a device did acknowledge to the address.
  //    Wire.beginTransmission(address);
  //    error = Wire.endTransmission();
  //
  //    if (error == 0)
  //    {
  //      tinySerial.print("I2C device found at address 0x");
  //      if (address < 16)
  //        tinySerial.print("0");
  //      tinySerial.print(address, HEX);
  //      tinySerial.println("  !");
  //
  //      nDevices++;
  //    }
  //    else if (error == 4)
  //    {
  //      tinySerial.print("Unknown error at address 0x");
  //      if (address < 16)
  //        tinySerial.print("0");
  //      tinySerial.println(address, HEX);
  //    }
  //  }
  //  if (nDevices == 0)
  //    tinySerial.println("No I2C devices found\n");
  //  else
  //    tinySerial.println("done\n");
  //


}
