#include "si5351-avr-tiny-minimal.h"
#include <SoftwareSerial.h>

SoftwareSerial tinySerial(3, 4);

int loopcount = 0;

void setup()
{
  // Start serial and initialize the Si5351
  tinySerial.begin(9600);
  tinySerial.println("\nSerial Begin.VFO!");

  si5351_init(SI5351_CRYSTAL_LOAD_8PF, 0);
  si5351_set_freq(14000000UL, SI5351_CLK0);
  si5351_drive_strength(SI5351_CLK0, SI5351_DRIVE_4MA);
  si5351_set_correction(-900);
}

void loop()
{
  loopcount++;
  tinySerial.println("Loop : " + loopcount);
  delay(500);
}
