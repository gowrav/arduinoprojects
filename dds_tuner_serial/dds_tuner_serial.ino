#include "si5351.h"
#include "Wire.h"
#include <EEPROM.h>
#include "EEPROMAnything.h"

struct config_t
{
  uint64_t xtal_f;
  uint64_t ppm_corr;
  uint64_t default_vfo_f;
  uint64_t default_bfo_f;
  uint64_t lo_f = 0;//default_vfo_f + default_bfo_f;
} configuration;



Si5351 si5351;

bool valueChanged = false;
bool runOnce = false;

int pref_xtal_f_eepromadr = 1;
int pref_ppm_corr_eepromadr = 2;
int pref_bfo_eepromadr = 3;
int pref_vfo_eepromadr = 4;

//uint64_t xtal_f = 0ULL;
//uint64_t ppm_corr = 232000ULL;
//uint64_t default_vfo_f =   62650000ULL;
//uint64_t default_bfo_f = 4499200000ULL;
//uint64_t lo_f = 0;//default_vfo_f + default_bfo_f;

String cmdStr;
int opListSize = 6;
char* opList[] = {"SXF", "SCF",  "SVF", "SBF", "RLO", "ZZZ"};

void initDefaultParams() {
  configuration.xtal_f = 0ULL;
  configuration.ppm_corr = 232000ULL;
  configuration.default_vfo_f =   62650000ULL;
  configuration.default_bfo_f = 4499200000ULL;
  configuration.lo_f = configuration.default_vfo_f + configuration.default_bfo_f;
  savePrefValues();
}


void setup()
{
  // Start serial and initialize the Si5351
  Serial.begin(115200);
  loadPrefValues();
  if (!si5351.init(SI5351_CRYSTAL_LOAD_8PF, configuration.xtal_f, configuration.ppm_corr))
  {
    Serial.println(F("Device not found on I2C bus!"));
  }
  Serial.println(F("Commands Help : SXF, SCF, SVF, SBF, RLO, ZZZ"));

}

void loop()
{
  if (valueChanged) {
    valueChanged = false;
    loadPrefValues();
    printFreqValues();
    if (runOnce) {
      runOnce = false;
      si5351.init(SI5351_CRYSTAL_LOAD_8PF, configuration.xtal_f, configuration.ppm_corr);
      si5351.drive_strength(SI5351_CLK2, SI5351_DRIVE_2MA);
      si5351.drive_strength(SI5351_CLK0, SI5351_DRIVE_2MA);
    }
    si5351.set_freq(configuration.default_bfo_f, SI5351_CLK2);
    si5351.set_freq(configuration.lo_f, SI5351_CLK0);
  }

  fetchSerialCommand();
}

void fetchSerialCommand() {
  if (Serial.available() > 0) cmdStr.remove(0); // Remove previous commands
  while (Serial.available() > 0)
  {
    char carbyte = Serial.read();
    if (carbyte == ',')
      break;
    else
      cmdStr += carbyte;
    delayMicroseconds(80);//best delay b/w bytes
  }

  if (cmdStr.length() >= 3)
  {
    //    Serial.print(F("Command Recieved : "));
    //    Serial.println(cmdStr);
    //    Serial.print(F("Command Index : "));
    //    Serial.println(IsArrayContains(cmdStr.substring(0, 3), opList));
    char tempBuf [64];

    switch (IsArrayContains(cmdStr.substring(0, 3), opList))
    {
      case 0  :
        cmdStr.substring(4).toCharArray(tempBuf, cmdStr.substring(4).length());
        configuration.xtal_f = strtoul(tempBuf, NULL, 0) ;
        savePrefValues();
        break;

      case 1  :
        cmdStr.substring(4).toCharArray(tempBuf, cmdStr.substring(4).length());
        configuration.ppm_corr = strtoul(tempBuf, NULL, 0);
        savePrefValues();
        break;

      case 2  :
        cmdStr.substring(4).toCharArray(tempBuf, cmdStr.substring(4).length());
        configuration.default_vfo_f = strtoul(tempBuf, NULL, 0) ;
        configuration.lo_f = configuration.default_vfo_f + configuration.default_bfo_f;
        savePrefValues();
        break;

      case 3  :
        cmdStr.substring(4).toCharArray(tempBuf, cmdStr.substring(4).length());
        configuration.default_bfo_f = strtoul(tempBuf, NULL, 0) ;
        configuration.lo_f = configuration.default_vfo_f + configuration.default_bfo_f;
        savePrefValues();
        break;

      case 4  :
        //printFreqValues();
        break;

      case 5  :
        initDefaultParams();
        break;

      default :
        cmdStr.remove(0);
        return;
    }

    cmdStr.remove(0);
    valueChanged = true;

  }
}


void savePrefValues() {
  //  EEPROM.write(pref_xtal_f_eepromadr, xtal_f);
  //  EEPROM.write(pref_ppm_corr_eepromadr, ppm_corr);
  //  EEPROM.write(pref_bfo_eepromadr, default_bfo_f);
  //  EEPROM.write(pref_vfo_eepromadr, default_vfo_f);
  EEPROM_writeAnything(0, configuration);
}

void loadPrefValues() {
  //  xtal_f = EEPROM.read(pref_xtal_f_eepromadr);
  //  ppm_corr = EEPROM.read(pref_ppm_corr_eepromadr);
  //  default_bfo_f = EEPROM.read(pref_bfo_eepromadr);
  //  default_vfo_f = EEPROM.read(pref_vfo_eepromadr);
  EEPROM_readAnything(0, configuration);

}

void printFreqValues() {
  Serial.print("Xtal Frequency : ");
  Serial.println(uint64ToString(configuration.xtal_f));
  Serial.print("PPM Correction : ");
  Serial.println(uint64ToString(configuration.ppm_corr));
  Serial.print("VFO Frequency : ");
  Serial.println(uint64ToString(configuration.default_vfo_f));
  Serial.print("BFO Frequency : ");
  Serial.println(uint64ToString(configuration.default_bfo_f));
  Serial.print("LO Frequency : ");
  Serial.println(uint64ToString(configuration.lo_f));
}


int IsArrayContains(String searchValue, char* searchList[])
{
  searchValue.toUpperCase();
  for (int i = 0; i < opListSize ; i++)
    if (searchValue == searchList[i])
      return i;
  return -1;
}


String uint64ToString(uint64_t input) {
  String result = "";
  uint8_t base = 10;

  do {
    char c = input % base;
    input /= base;

    if (c < 10)
      c += '0';
    else
      c += 'A' - 10;
    result = c + result;
  } while (input);
  return result;
}



