/*
 https://us-central1-trackmylock.cloudfunctions.net/createlock?uid=1234
 */

#include <LGPS.h>
#include <LGPRS.h>
#include <LGPRSClient.h>
#include <LGPRSServer.h>

unsigned long previous = 0;
const long interval = 20000;

int key1 = 8;
int key2 = 6;
int key3 = 5;
int key4 = 4;

char server[] = "us-central1-trackmylock.cloudfunctions.net";
char path[] = "/updategps?uid=355675061266722";
int port = 80; // HTTPS

LGPRSClient client;

gpsSentenceInfoStruct info;
char buff[256];
String a1;
String a2;

void chkloc()
{
  Serial.println("inside chkloc");
  LGPS.getData(&info);
  parseGPGGA((const char*)info.GPGGA);
  while(client.available())
  {
    char  c = client.read();
    Serial.print(c);
  }
  
}

static unsigned char getComma(unsigned char num,const char *str)
{
  unsigned char i,j = 0;
  int len=strlen(str);
  for(i = 0;i < len;i ++)
  {
     if(str[i] == ',')
      j++;
     if(j == num)
      return i + 1; 
  }
  return 0; 
}

static double getDoubleNumber(const char *s)
{
  char buf[10];
  unsigned char i;
  double rev;
  
  i=getComma(1, s);
  i = i - 1;
  strncpy(buf, s, i);
  buf[i] = 0;
  rev=atof(buf);
  return rev; 
}

void parseGPGGA(const char* GPGGAstr)
{
  Serial.println(" inside PARSE GPGGA");
  double latitude;
  double longitude;
  int tmp, hour, minute, second, num ;
  Serial.println(GPGGAstr);
  if(GPGGAstr[0] == '$')
  {
    tmp = getComma(1, GPGGAstr);
    tmp = getComma(2, GPGGAstr);
    latitude = getDoubleNumber(&GPGGAstr[tmp]);
    tmp = getComma(4, GPGGAstr);
    longitude = getDoubleNumber(&GPGGAstr[tmp]);
//    sprintf(a1,"%f",latitude/100);
Serial.println("PARSE GPGGA");


Serial.println(latitude);
Serial.println(longitude);

   double da1 = int(latitude/100) ;
   double da2 = int(longitude/100) ;
Serial.println(da1);
Serial.println(da2);

Serial.println(((latitude/100.00f-da1))/60.00f);
Serial.println(((longitude/100.00f-da2))/60.00f);


    a1 = String((da1 + 100*((latitude/100.00f-da1))/60.00f),6) ;
    a2 = String((da2 + 100*((longitude/100.00f-da2))/60.00f),6) ;


 Serial.println(a1);
Serial.println(a2);
   
//    sprintf(a2,"%f",longitude/100);    
 
  }
  else
  {
    Serial.println("Not get data"); 
  }
}

void runn()
{
  // setup Serial po
  Serial.begin(115200);


  Serial.println("Attach to GPRS network by auto-detect APN setting");
  while (!LGPRS.attachGPRS())
  {
    delay(500);
  }
 
  // if you get a connection, report back via serial:
  Serial.print("Connect to ");
  Serial.println(server);
  
  if (client.connect(server, port))
  {
    Serial.println("connected"); 
  
    // Make a HTTP request:
    client.print("GET /updategps?uid=355675061266722&lat=");
    client.print(a1);
    client.print("&lng=");
    client.print(a2);
//    client.print(path);
    client.println(" HTTP/1.1");
    client.print("Host: ");
    client.println(server);
     
    client.println("Connection: close");
    client.println();
    
  }
  else
  {
    // if you didn't get a connection to the server:
    Serial.println("connection failed");
  }
  LGPS.powerOn();
  delay(3000);
}


void setup()
{
  runn();

  pinMode(key1, INPUT);
  pinMode(key2, INPUT);
  pinMode(key3, INPUT);
  pinMode(key4, INPUT);

  previous = millis() - interval;
}

void test()
{
  Serial.println("Attach to GPRS network by auto-detect APN setting");
  while (!LGPRS.attachGPRS())
  {
    delay(500);
  }
 
  // if you get a connection, report back via serial:
  Serial.print("Connect to ");
  Serial.println(server);
  
  if (client.connect(server, port))
  {
    Serial.println("connected"); 
  
    // Make a HTTP request:
    client.print("GET /updategps?uid=355675061266722&lat=");
    client.print(a1);
    client.print("&lng=");
    client.print(a2);
//    client.print(path);
    client.println(" HTTP/1.1");
    client.print("Host: ");
    client.println(server);
     
    client.println("Connection: close");
    client.println();
    
  }
  else
  {
    // if you didn't get a connection to the server:
    Serial.println("connection failed");
  }
  LGPS.powerOn();
  delay(3000);
}

void tmprd()
{
  Serial.println("Attach to GPRS network by auto-detect APN setting");
  while (!LGPRS.attachGPRS())
  {
    delay(500);
  }
 
  // if you get a connection, report back via serial:
  Serial.print("Connect to ");
  Serial.println(server);
  
  if (client.connect(server, port))
  {
    Serial.println("connected"); 
  
    // Make a HTTP request:
    client.print("GET /updategps?uid=355675061266722&lat=");
    client.print(a1);
    client.print("&lng=");
    client.print(a2);
    client.print("&tamper=");
    client.print("true");
//    client.print(path);
    client.println(" HTTP/1.1");
    client.print("Host: ");
    client.println(server);
     
    client.println("Connection: close");
    client.println();
    
  }
  else
  {
    // if you didn't get a connection to the server:
    Serial.println("connection failed");
  }
  LGPS.powerOn();
  delay(3000);
}

void loop()
{ 

 int keyState1 = digitalRead(key1);
 int keyState2 = digitalRead(key2);
 int keyState3 = digitalRead(key3);
 int keyState4 = digitalRead(key4);


 if((keyState1==0)||(keyState2==0)||(keyState3==0)||(keyState4==0))
         {  
//            Serial.println("");
            Serial.println("Tampered");
            tmprd();
             while(client.available())
            {
               char  c = client.read();
               Serial.print(c);
            }
         }
 else
 {
  unsigned long current = millis();
/*  Serial.print("current ");
  Serial.println(current);
  Serial.print("previous ");
  Serial.println(previous);*/
   if(current - previous >= interval)   
   {
//        Serial.println("inside condition");
     
        previous = current;
        test();
        chkloc();
        
         Serial.println("");
//         Serial.println("not tampered");
              
    }
 }
   

 // delay(1000);
}

