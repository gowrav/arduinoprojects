/*
   si5351_example.ino - Simple example of using Si5351Arduino library

   Copyright (C) 2015 - 2016 Jason Milldrum <milldrum@gmail.com>

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "si5351.h"
#include "Wire.h"

Si5351 si5351;

uint64_t xtal_f = 0ULL;
uint64_t ppm_corr = 232000ULL;

uint64_t default_vfo_f =   62650000ULL;
uint64_t default_bfo_f = 4499200000ULL;

uint64_t lo_f = default_vfo_f + default_bfo_f;

void setup()
{
  bool i2c_found;

  // Start serial and initialize the Si5351
  Serial.begin(115200);
  i2c_found = si5351.init(SI5351_CRYSTAL_LOAD_8PF, xtal_f, ppm_corr);
  if (!i2c_found)
  {
    Serial.println("Device not found on I2C bus!");
  }

  si5351.set_freq(default_bfo_f, SI5351_CLK2);
  si5351.drive_strength(SI5351_CLK2, SI5351_DRIVE_2MA);

  si5351.set_freq(lo_f, SI5351_CLK0);
  si5351.drive_strength(SI5351_CLK0, SI5351_DRIVE_2MA);

  Serial.println("Device found..! and Started");

  delay(500);
}

void loop()
{

}
