// P10 Pixel Driver


// In this panel, first, A , B , LAT , CLK , DAT , and OE pass through the 74HC245 (8-channel transceiver) of the general-purpose logic IC.
// The A and B lines of the connector (after passing 74HC245) are connected to the input of 74HC138 (3 to 8 decoder ) of the general purpose logic IC
// and are used to drive the source driver FET at the output side.
// In addition, the DAT line, CLK and LAT are connected to the 74HC595 (shift register ) and used as a sink driver.
// The input data is in negative logic with DAT ( written as /DAT specification ).
// When the 74HC595 terminal goes to 0 V, suction occurs due to the potential difference with the source driver side, and the LED lights up.

// Total pins A B CLK STORE DATA Enable.
// 8bits * 4 Combinations = 32LEDS * 16 IC

//Pin Setup
int OE = 8;
int FET_A = 7;
int FET_B = 6;
int CLK = 5;
int STORE = 4;
int DATA = 3;

int shiftregister_count_per_panel = 16 // they are in series.. and 4 rows of outputs need to be shuffled using fet
                                    int fet_row_driver_count_perpanel = 4 // A B tells which row


void setup() {

  pinMode(OE, OUTPUT);
  pinMode(FET_A, OUTPUT);
  pinMode(FET_B, OUTPUT);
  pinMode(CLK, OUTPUT);
  pinMode(STORE, OUTPUT);
  pinMode(DATA, OUTPUT);
  
  // put your setup code here, to run once:
  // Empty all registers

  for (int i = 0; i < shiftregister_count_per_panel * fet_row_driver_count_perpanel; i++)
  {

  }


}

void loop() {
  // put your main code here, to run repeatedly:

}
