#include <TimerOne.h>
#include <SoftwareSerial.h>
#include <EEPROM.h>
#include "default_font.h"


SoftwareSerial wifiSerial(10, 9); // RX, TX
#define NO_SLAB  1
#define NO_SLICE  1
#define REP_COUNT   2//4
#define CHAR_LINES   16//4
#define BS_LIMIT   8//4

int NO_UNITS = 1;
int NO_CHAR = 4 * NO_UNITS;

int LED_BRIGHTNESS =  400; // 0 to 1000 // 400 is optimum

bool DARKMODE_ENABLED = false;//set dark mode to true to invert light
bool BITSCROLL_ENABLED = true;//set dark mode to true to invert light

String dispString = "AiRpaper";//"AiRpaper - Information to Everyone";
String content = "";
String qwerty = "";
String Direction = "Left";
int resetPin = A0; // digital pin to reset the password to default password


float f = 123.456f;
int cPath = 100;  // content storage address
int bPath = 2;  // 112  brightness controll address
int sPath = 4;  // 115 scrolling direction controll address , ic left or right, 0 means left to right and 1 means right to left
int spPath = 6;  //  117 speed of the scrolling address
int stylepath = 8;  //  display style address

int contLen = 0;
boolean getCont = true;
int sSpeed = 25;
int style = 0;  // 0 means static and 1 means scrolling

int latchPin = 8; //LT
int clockPin = 12; //SK
int dataPin = 11; //R1

int dispAll_IO = 45;

int en_74138 = 2;
int la_74138 = 3;
int lb_74138 = 4;
int lc_74138 = 5;
int ld_74138 = 6;
int k = 0;
int lTimes = 2;

void shiftOut(unsigned char dataOut)
{

  if (DARKMODE_ENABLED)
  {
    dataOut = ~dataOut;
  }

  for (int i = 7; i >= 0  ; i--)
  {
#if defined(__AVR_ATmega1280__) || defined(__AVR_ATmega2560__)
    // Fast Data Toggler
    PORTB &= ~(1 << (6));
    if (dataOut & (0x01 << i)) PORTB &= ~(1 << (5));
    else
      PORTB |= 1 << (5);
    PORTB |= 1 << (6);
#endif

#if defined(__AVR_ATmega328P__) || defined(__AVR_ATmega168__)
    // Super Fast Data Toggler
    PORTB &= 0xEF;
    if (dataOut & (0x01 << i)) PORTB &= 0xF7;
    else
      PORTB |= 0x08;
    PORTB |= 0x10;
#endif
  }
}


void cyclePower(int l)
{
#if defined(__AVR_ATmega328P__) || defined(__AVR_ATmega168__)
  // Super Fast Data Toggler
  PORTD &= 0xFB;
  if (l % 2 > 0 )
    PORTD |= 0x08;
  else
    PORTD &= 0xF7;

  if (l % 4 > 1)
    PORTD |= 0x10;
  else
    PORTD &= 0xEF;

  PORTD |= 0x04;
  delayMicroseconds(LED_BRIGHTNESS);
  PORTD &= 0xFB;

#endif
}


uint8_t msg_board[] =
{
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
};


void display_msg(String eightchars, int delaySec)
{
  while (delaySec--) {
    display_msg(eightchars);

  }
}


void display_msg(String eightchars)
{
  NO_CHAR = 4 * NO_UNITS;
  // Reduce string length to 8
  if (eightchars.length() > NO_CHAR)
    eightchars = eightchars.substring(0, NO_CHAR);

  // Optimize string with spaces to 8
  if (eightchars.length() < NO_CHAR)
  {
    String temp = "";
    for (int i = 0; i < (NO_CHAR - eightchars.length()) / 2; i++)
      temp += " ";
    temp += eightchars;
    for (int i = 0; i < (((NO_CHAR - eightchars.length()) / 2) + eightchars.length() % 2); i++)
      temp += " ";
    eightchars = temp;
  }


  // Set msgBoardData here
  for (int y = 0; y < NO_CHAR; y++)
    for (int x = 0; x < CHAR_LINES; x++)
    {
      msg_board[(CHAR_LINES * y) + x] = pgm_read_word_near(font8x16_basic + (eightchars.charAt(y) * CHAR_LINES) + x);
    }
  digitalWrite(en_74138, LOW);//Turn off display
  for (int i = 0; i < NO_SLAB * NO_SLICE; i++)
  {
    for (int l = 3; l >= 0 ; l--)
    {
      for (int k = 0; k < REP_COUNT; k++)
      {
        for (int m = 0; m < NO_CHAR ; m++)
        {
          shiftOut(msg_board[m * CHAR_LINES + ((4 * 3) + l)]);
          shiftOut(msg_board[m * CHAR_LINES + ((4 * 2) + l)]);
          shiftOut(msg_board[m * CHAR_LINES + ((4 * 1) + l)]);
          shiftOut(msg_board[m * CHAR_LINES + (l)]);
          PORTB &= 0xFE;
          PORTB |= 0x01;
          PORTB &= 0xFE;
        }
      }

      cyclePower(l);
    }
  }
}


void display_msgfilled()
{

  digitalWrite(en_74138, LOW);//Turn off display
  for (int i = 0; i < NO_SLAB * NO_SLICE; i++)
  {
    for (int l = 3; l >= 0 ; l--)
    {
      for (int k = 0; k < REP_COUNT; k++)
      {
        for (int m = 0; m < NO_CHAR ; m++)
        {
          int repeatfour = 4;

          while (repeatfour--)
            shiftOut(0xFF);

          PORTB &= 0xFE;
          PORTB |= 0x01;
          PORTB &= 0xFE;
          //delay(10);
        }
      }

      cyclePower(l);
    }
  }
}

void displayScroll_msg_left(String longString)
{
  NO_CHAR = 4 * NO_UNITS;
  String spaces = "";
  for ( int i = 0; i < NO_CHAR; i++)
    spaces += " ";

  longString = spaces + longString; //+spaces; // Needs to be looked into regarding the jump of last part

  for (int m = 0; m < longString.length(); m ++)
  {
    String shortString = "";
    if (m + NO_CHAR < longString.length())
      shortString = longString.substring(m, m + NO_CHAR + 1);
    else
      shortString = longString.substring(m);

    int TEMP_BS_LIMIT = BS_LIMIT;
    if (BITSCROLL_ENABLED) {
      if (m + NO_CHAR < longString.length())
        shortString = longString.substring(m, m + NO_CHAR + 1);
      else
        shortString = longString.substring(m);// Substituted m+1 with m
    }
    else
    {
      TEMP_BS_LIMIT = 1;
    }

    for (int bsc = 0; bsc < TEMP_BS_LIMIT ; bsc++)
    {
      // Set msgBoardData here
      for (int y = 0; y < NO_CHAR; y++)
        for (int x = 0; x < CHAR_LINES; x++)
        {
          msg_board[(CHAR_LINES * y) + x] = (unsigned char)pgm_read_word_near(font8x16_basic + (shortString.charAt(y) * CHAR_LINES) + x) << bsc;//disappearing
          msg_board[(CHAR_LINES * y) + x] |= (unsigned char)pgm_read_word_near(font8x16_basic + (shortString.charAt(y + 1) * CHAR_LINES) + x) >> (BS_LIMIT - bsc) ; //appearing
        }
      int k = sSpeed;
      if (BITSCROLL_ENABLED) {
        k = k / 6; // / 3;
      }
      while (k--)
      {
        digitalWrite(en_74138, LOW);//Turn off display
        for (int i = NO_SLAB * NO_SLICE; i > 0 ; i--) //for (int i = 0; i < NO_SLAB * NO_SLICE; i++)
        {
          for (int l = 3; l >= 0 ; l--)
          {
            for (int m = 0; m < NO_CHAR; m++)
            {
              shiftOut(msg_board[m * CHAR_LINES + ((4 * 3) + l)]);
              shiftOut(msg_board[m * CHAR_LINES + ((4 * 2) + l)]);
              shiftOut(msg_board[m * CHAR_LINES + ((4 * 1) + l)]);
              shiftOut(msg_board[m * CHAR_LINES + (l)]);
              PORTB &= 0xFE;
              PORTB |= 0x01;
              PORTB &= 0xFE;
            }
            cyclePower(l);
          }
        }
      }
    }
  }
}

void display_flush()
{
  PORTD &= 0xFB;
  for (int l = 0; l < NO_SLAB * NO_SLICE ; l++)
  {
    shiftOut(0);
    PORTB &= 0xFE;
    PORTB |= 0x01;
    PORTB &= 0xFE;
  }
  PORTD |= 0x04;
}

void init_matrixDisplay()
{
  pinMode(latchPin, OUTPUT);
  pinMode(clockPin, OUTPUT);
  pinMode(dataPin, OUTPUT);
  pinMode(en_74138, OUTPUT);
  pinMode(la_74138, OUTPUT);
  pinMode(lb_74138, OUTPUT);
  pinMode(lc_74138, OUTPUT);
  pinMode(ld_74138, OUTPUT);

  pinMode(dispAll_IO, INPUT);

  digitalWrite(lc_74138, LOW);
  digitalWrite(ld_74138, LOW);
  digitalWrite(en_74138, LOW);
  display_flush();
}

boolean testLED = false;
String inputString = "";         // a String to hold incoming data
bool stringComplete = false;  // whether the string is complete


void setup()
{
  Serial.begin(115200); //Serial.setTimeout(50);
  wifiSerial.begin(9600);// btSerial.setTimeout(50);
  inputString.reserve(100);

  NO_UNITS = 2;

  init_matrixDisplay();

  pinMode(resetPin, INPUT_PULLUP);
  display_flush();
  displayScroll_msg_left(dispString);

  contLen = dispString.length();
  if (EEPROM.read(cPath) < 3)
  {
    EEPROM.write(cPath, contLen);
    for (int i = 0; i < dispString.length(); i++)
    {
      int count = cPath + 1 + i;
      EEPROM.write(count, dispString.charAt(i));
    }
    EEPROM.write(spPath, 25);
    EEPROM.write(bPath, 80);
    EEPROM.write(sPath, 1);
    EEPROM.write(stylepath, 0);
  }


}


void loop() {

  if (stringComplete) {
    changeContent(inputString, cPath);
    inputString = "";
    stringComplete = false;
  }

  if (getCont) {
    getCont = false;
    contLen = EEPROM.read(cPath);
    content = "";
    for (int j = 0; j < contLen; j++)
    {
      int coV = cPath + 1 + j;
      content += char(EEPROM.read(coV));
    }
    dispString = content;
    sSpeed = 90;//EEPROM.read(spPath); // adding new value to scrolling speed from bluetooth.
    sSpeed = 200 - sSpeed * 2;
    LED_BRIGHTNESS = EEPROM.read(bPath); // adding new value to scrolling speed from bluetooth.
    LED_BRIGHTNESS = LED_BRIGHTNESS * 15;
    style = EEPROM.read(stylepath);
  }

  displayScroll_msg_left(dispString);
}


void serialEvent() {
  inputString = "";
  while (Serial.available())
  {
    delay(1);
    if (Serial.available() > 0)
    {
      char c = Serial.read();
      inputString += c;
    }
  }

//  Serial.println(inputString);

  if (inputString.startsWith("*{") && inputString.endsWith("}*\r\n"))
  {
    inputString = inputString.substring(inputString.indexOf('{') + 1, inputString.indexOf('}'));
    stringComplete = true;
  } else {
    inputString = "";
    stringComplete = false;
  }

}

void changeContent(String data, int pathLen)
{
  getCont = true;
  dispString = data;
  int cLen = data.length();
  EEPROM.write(pathLen, cLen);
  for (int i = 0; i < data.length(); i++)
  {
    int count = pathLen + 1 + i;
    EEPROM.write(count, data.charAt(i));
  }
}

void setDataAttribute(int data, int pathLen)
{
  EEPROM.write(pathLen, data);
}

void clearEEpromData()
{
  for (int i = 0 ; i < EEPROM.length() ; i++)
  {
    EEPROM.write(i, 0);
  }
}
