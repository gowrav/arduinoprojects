

uint64_t mystart = 50000ULL;
void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);

}

bool reverse = false;
void loop() {
  // put your main code here, to run repeatedly:
  Serial.println(uint64ToString(mystart));
  delay(10);
  if (mystart > 25050000ULL)
    reverse = true;
  if (mystart == 0ULL)
    reverse = false;
    
  if (reverse)
    mystart = mystart - 50000ULL;
  else
    mystart = mystart + 50000ULL;


}


String uint64ToString(uint64_t input) {
  String result = "";
  uint8_t base = 10;

  do {
    char c = input % base;
    input /= base;

    if (c < 10)
      c += '0';
    else
      c += 'A' - 10;
    result = c + result;
  } while (input);
  return result;
}
