
// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(13, OUTPUT);
  pinMode(29, OUTPUT);
  pinMode(30, OUTPUT);
  pinMode(31, OUTPUT);
  pinMode(34, OUTPUT);
  pinMode(35, OUTPUT);
  pinMode(A3, OUTPUT);
  pinMode(A7, OUTPUT);
  pinMode(2, OUTPUT);
  pinMode(3, OUTPUT);
  pinMode(8, OUTPUT);
  pinMode(9, OUTPUT);
  pinMode(12, OUTPUT);

  pinMode(22, OUTPUT);
  pinMode(23, OUTPUT);
  pinMode(24, OUTPUT);
  pinMode(25, OUTPUT);
  pinMode(26, OUTPUT);
  pinMode(27, OUTPUT);
  pinMode(28, OUTPUT);

  pinMode(A6, OUTPUT);
    digitalWrite(A6, HIGH);   // turn the LED on (HIGH is the voltage level)

}

// the loop function runs over and over again forever
void loop() {
  digitalWrite(9, LOW);   // turn the LED on (HIGH is the voltage level)
  digitalWrite(13, LOW);   // turn the LED on (HIGH is the voltage level)
  digitalWrite(29, LOW);   // turn the LED on (HIGH is the voltage level)
  digitalWrite(34, LOW);   // turn the LED on (HIGH is thevoltage level)

  digitalWrite(31, HIGH);   // turn the LED on (HIGH is the voltage level)
  digitalWrite(A7, HIGH);   // turn the LED on (HIGH is the voltage level)
  digitalWrite(2, HIGH);   // turn the LED on (HIGH is the voltage level)
  digitalWrite(3, HIGH);   // turn the LED on (HIGH is the voltage level)

  digitalWrite(30, LOW);   // turn the LED on (HIGH is the voltage level)
  digitalWrite(35, LOW);   // turn the LED on (HIGH is the voltage level)
  digitalWrite(8, LOW);   // turn the LED on (HIGH is the voltage level)
  digitalWrite(12, LOW);   // turn the LED on (HIGH is the voltage level)


  delay(3000);                       // wait for a second


    digitalWrite(9, HIGH);   // turn the LED on (HIGH is the voltage level)
  digitalWrite(13, HIGH);   // turn the LED on (HIGH is the voltage level)
  digitalWrite(29, HIGH);   // turn the LED on (HIGH is the voltage level)
  digitalWrite(34, HIGH);   // turn the LED on (HIGH is thevoltage level)

  digitalWrite(31, LOW);   // turn the LED on (HIGH is the voltage level)
  digitalWrite(A7, LOW);   // turn the LED on (HIGH is the voltage level)
  digitalWrite(2, LOW);   // turn the LED on (HIGH is the voltage level)
  digitalWrite(3, LOW);   // turn the LED on (HIGH is the voltage level)

  digitalWrite(30, LOW);   // turn the LED on (HIGH is the voltage level)
  digitalWrite(35, LOW);   // turn the LED on (HIGH is the voltage level)
  digitalWrite(8, LOW);   // turn the LED on (HIGH is the voltage level)
  digitalWrite(12, LOW);   // turn the LED on (HIGH is the voltage level)


  delay(1000);                       // wait for a second


    digitalWrite(9, LOW);   // turn the LED on (HIGH is the voltage level)
  digitalWrite(13, LOW);   // turn the LED on (HIGH is the voltage level)
  digitalWrite(29, LOW);   // turn the LED on (HIGH is the voltage level)
  digitalWrite(34, LOW);   // turn the LED on (HIGH is thevoltage level)

  digitalWrite(31, LOW);   // turn the LED on (HIGH is the voltage level)
  digitalWrite(A7, LOW);   // turn the LED on (HIGH is the voltage level)
  digitalWrite(2, LOW);   // turn the LED on (HIGH is the voltage level)
  digitalWrite(3, LOW);   // turn the LED on (HIGH is the voltage level)

  digitalWrite(30, HIGH);   // turn the LED on (HIGH is the voltage level)
  digitalWrite(35, HIGH);   // turn the LED on (HIGH is the voltage level)
  digitalWrite(8, HIGH);   // turn the LED on (HIGH is the voltage level)
  digitalWrite(12, HIGH);   // turn the LED on (HIGH is the voltage level)


  delay(1000);                       // wait for a second


    digitalWrite(9, LOW);   // turn the LED on (HIGH is the voltage level)
  digitalWrite(13, LOW);   // turn the LED on (HIGH is the voltage level)
  digitalWrite(29, LOW);   // turn the LED on (HIGH is the voltage level)
  digitalWrite(34, LOW);   // turn the LED on (HIGH is thevoltage level)

  digitalWrite(31, LOW);   // turn the LED on (HIGH is the voltage level)
  digitalWrite(A7, LOW);   // turn the LED on (HIGH is the voltage level)
  digitalWrite(2, LOW);   // turn the LED on (HIGH is the voltage level)
  digitalWrite(3, LOW);   // turn the LED on (HIGH is the voltage level)

  digitalWrite(30, LOW);   // turn the LED on (HIGH is the voltage level)
  digitalWrite(35, LOW);   // turn the LED on (HIGH is the voltage level)
  digitalWrite(8, LOW);   // turn the LED on (HIGH is the voltage level)
  digitalWrite(12, LOW);   // turn the LED on (HIGH is the voltage level)

  delay(200);                       // wait for a second


//  digitalWrite(22, HIGH);   // turn the LED on (HIGH is the voltage level)
//  digitalWrite(23, HIGH);   // turn the LED on (HIGH is the voltage level)
//  digitalWrite(24, HIGH);   // turn the LED on (HIGH is the voltage level)
//  digitalWrite(25, HIGH);   // turn the LED on (HIGH is the voltage level)
//  digitalWrite(26, HIGH);   // turn the LED on (HIGH is the voltage level)
//  digitalWrite(27, HIGH);   // turn the LED on (HIGH is the voltage level)
//  digitalWrite(28, HIGH);   // turn the LED on (HIGH is the voltage level)

    delay(500);                       // wait for a second


  digitalWrite(22, LOW);   // turn the LED on (HIGH is the voltage level)
  digitalWrite(23, LOW);   // turn the LED on (HIGH is the voltage level)
  digitalWrite(24, LOW);   // turn the LED on (HIGH is the voltage level)
  digitalWrite(25, LOW);   // turn the LED on (HIGH is the voltage level)
  digitalWrite(26, LOW);   // turn the LED on (HIGH is the voltage level)
  digitalWrite(27, LOW);   // turn the LED on (HIGH is the voltage level)
  digitalWrite(28, LOW);   // turn the LED on (HIGH is the voltage level)

    delay(500);                       // wait for a second


  digitalWrite(A6, LOW);   // turn the LED on (HIGH is the voltage level)
  delay(500);                       // wait for a second
    digitalWrite(A6, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(500);                       // wait for a second

}
