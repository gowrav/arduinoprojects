
#include <SoftwareSerial.h>
// software serial #1: RX = digital pin 10, TX = digital pin 9
SoftwareSerial portOne(10, 9);

void setup() {
  // Open serial communications and wait for port to open:
  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }
  // Start each software serial port
  portOne.begin(9600);
}

void loop() {
  // By default, the last intialized port is listening.
  // when you want to listen on a port, explicitly select it:
  portOne.listen();
  // while there is data coming in, read it
  // and send to the hardware serial port:
  while (portOne.available() > 0) {
    char inByte = portOne.read();
    Serial.write(inByte);
  }
  while (Serial.available() > 0) {
    char inByte = Serial.read();
    portOne.write(inByte);
  }
}
