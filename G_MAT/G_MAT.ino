//Author: cantone-electonics
//More information welcome to : http://www.canton-electronics.com
//Arduino 1.0.4
//Arduino MEGA2560 R3
//64x16 Matrix LED

#define COL_PIXEL     64
#define ROW_PIXEL     16

int latchPin = 8; //LT Latches after Data Set
int clockPin = 12; //SK
int dataPin = 11; //R1 Same as Clock
//int dataPin2 = 11; //G1 Always 0

int en_74138 = 2; // Triggers Display
int la_74138 = 3; // varies 0 and 1 fast
int lb_74138 = 4; // varies 0 and 1 slow


int lc_74138 = 5;//C Always 0
int ld_74138 = 6;//D Always 0

unsigned int ROW_xPixel;
unsigned int ROW_num;
unsigned char Col_num_1;
unsigned char Col_num_2;
unsigned char Col_num_3;
unsigned char Col_num_4;
unsigned char Col_num_5;
unsigned char Col_num_6;
unsigned char Col_num_7;
unsigned char Col_num_8;

//Data code: Horizontal modulus ,Bytes reverse order
unsigned char  Bmp_0[] =
{
  /*------------------------------------------------------------------------------
    ;  Width X height (pixels): 64X16
    ;stTe
    ------------------------------------------------------------------------------*/
  0xFF, 0xFF, 0xFF, 0xFF, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00


};

void shiftOut(unsigned char dataOut)
{

  for (int i = 0; i <= 7; i++)
  {
    digitalWrite(clockPin, LOW);

    if (dataOut & (0x01 << i))
      digitalWrite(dataPin, HIGH);
    else
      digitalWrite(dataPin, LOW);

    digitalWrite(clockPin, HIGH);

  }
}


//display one picture
void display_martix(unsigned char *BMP)
{
  //Display count
  unsigned int dis_cnt = 64;//256;
  unsigned int i;

  for (i = 0; i <= dis_cnt * 16; i++)
  {

    digitalWrite(en_74138, HIGH);//Turn off display

    //Col scanning
    shiftOut(Col_num_1);
    shiftOut(Col_num_2);
    shiftOut(Col_num_3);
    shiftOut(Col_num_4);
    shiftOut(Col_num_5);
    shiftOut(Col_num_6);
    shiftOut(Col_num_7);
    shiftOut(Col_num_8);



    // Change Latches Here
    digitalWrite(la_74138, i % 2 > 0);
    digitalWrite(lb_74138, i % 4 > 1);

    Serial.print("la_74138:");
    Serial.println(i % 2 > 0);
    Serial.print("lb_74138:");
    Serial.println(i % 4 > 0);


    //Row scanning
    // AVR Port Operation
    //PORTD = ((ROW_xPixel << 3 ) & 0X78) | (PORTD & 0X87);//Write PIN 3 4 5 6 la_74138 lb_74138 lc_74138 ld_74138
    //    PORTE = ((ROW_xPixel << (5 - 0) ) & 0X20) | (PORTE & 0XDF); ////Write PIN 3(PE5) la_74138
    //    PORTG = ((ROW_xPixel << (5 - 1) ) & 0X20) | (PORTG & 0XDF); ////Write PIN 4(PG5) lb_74138
    //    PORTE = ((ROW_xPixel << (3 - 2) ) & 0X08) | (PORTE & 0XF7); ////Write PIN 5(PE3) lc_74138
    //    PORTH = ((ROW_xPixel << (3 - 3) ) & 0X08) | (PORTH & 0XF7); ////Write PIN 6(PH3) ld_74138


    digitalWrite(en_74138, LOW);//Turn on display



    if (ROW_xPixel == 15) ROW_xPixel = 0; else ROW_xPixel++;


    // Single color,1 bits/pixel
    Col_num_1 = ~BMP[(COL_PIXEL / 8) * ROW_xPixel];
    Col_num_2 = ~BMP[(COL_PIXEL / 8) * ROW_xPixel + 1];
    Col_num_3 = ~BMP[(COL_PIXEL / 8) * ROW_xPixel + 2];
    Col_num_4 = ~BMP[(COL_PIXEL / 8) * ROW_xPixel + 3];
    Col_num_5 = ~BMP[(COL_PIXEL / 8) * ROW_xPixel + 4];
    Col_num_6 = ~BMP[(COL_PIXEL / 8) * ROW_xPixel + 5];
    Col_num_7 = ~BMP[(COL_PIXEL / 8) * ROW_xPixel + 6];
    Col_num_8 = ~BMP[(COL_PIXEL / 8) * ROW_xPixel + 7];

    //delayMicroseconds(1000);

  }

}

void setup()
{
  pinMode(latchPin, OUTPUT);
  pinMode(clockPin, OUTPUT);
  pinMode(dataPin, OUTPUT);

  pinMode(en_74138, OUTPUT);

  pinMode(la_74138, OUTPUT);
  pinMode(lb_74138, OUTPUT);
  pinMode(lc_74138, OUTPUT);
  pinMode(ld_74138, OUTPUT);


  digitalWrite(lc_74138, LOW);
  digitalWrite(ld_74138, LOW);


  digitalWrite(en_74138, LOW);

  //  pinMode(dataPin2, OUTPUT);
  //  digitalWrite(dataPin2, LOW);

  Serial.begin(9600);

}




void loop()
{

  //flushData();

  //display_martix(Bmp_0);

  // while(true);
  setData();
}

void flushData()
{
  digitalWrite(en_74138, HIGH);//Turn off display

  for (int i = 0; i <= 1024; i++)
  {
    shiftOut(0xFF);
  }

  digitalWrite(en_74138, LOW);//Turn on display

}

void setData()
{
  digitalWrite(en_74138, HIGH);//Turn off display

  for (int i = 0; i <= 1024; i++)
  {
    shiftOut(0xFF);
  }

  digitalWrite(en_74138, LOW);//Turn on display

}



