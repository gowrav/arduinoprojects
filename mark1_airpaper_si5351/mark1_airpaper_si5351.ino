
#include "si5351.h"
#include "Wire.h"

Si5351 si5351;
//Si5351 si5351(0x61);
unsigned long ppm_factor = 232000ULL;

unsigned long bfo = 4500000000ULL;
unsigned long vfo = 62500000ULL;

bool i2c_found;

void setup()
{

  // Start serial and initialize the Si5351
  Serial.begin(115200);
  Serial.println("Connected.!");

  i2c_found = si5351.init(SI5351_CRYSTAL_LOAD_8PF, 0, 0);
  if (!i2c_found)
  {
    Serial.println("Device not found on I2C bus!");
    delay(1000); setup(); // Keep Looping till device is found
  }
  Serial.println("Device found.!, now Starting...");

  // Set Correction Factor
  setCorrectionFactor();

  // Set CLK0 to output 14 MHz
  si5351.set_freq(bfo, SI5351_CLK0);

  // Set CLK0 to output 24 MHz
  si5351.set_freq(vfo+bfo, SI5351_CLK2);

  //  Set CLK1 to output 175 MHz
  //  si5351.set_ms_source(SI5351_CLK1, SI5351_PLLB);
  //  si5351.set_freq_manual(17500000000ULL, 70000000000ULL, SI5351_CLK1);


  si5351.output_enable(SI5351_CLK0, 1);//use a 0 to disable and 1 to enable
  //  si5351.drive_strength(SI5351_CLK0, SI5351_DRIVE_4MA);//The drive strength is the amount of current into a 50Ω load. 2 mA roughly corresponds to 3 dBm output and 8 mA is approximately 10 dBm output.
  //  si5351.set_clock_invert(SI5351_CLK0, 1); // Invert clock output

}

void loop()
{
  delay(500);
}


void statusUpdate() {
  // Query a status update and wait a bit to let the Si5351 populate the
  // status flags correctly.
  si5351.update_status();
  delay(500);
  Serial.print("SYS_INIT: ");
  Serial.print(si5351.dev_status.SYS_INIT);
  Serial.print("  LOL_A: ");
  Serial.print(si5351.dev_status.LOL_A);
  Serial.print("  LOL_B: ");
  Serial.print(si5351.dev_status.LOL_B);
  Serial.print("  LOS: ");
  Serial.print(si5351.dev_status.LOS);
  Serial.print("  REVID: ");
  Serial.println(si5351.dev_status.REVID);
}

void setCorrectionFactor() {
  si5351.set_correction(ppm_factor, SI5351_PLL_INPUT_XO);
}

