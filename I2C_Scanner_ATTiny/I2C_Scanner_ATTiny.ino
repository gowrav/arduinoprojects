#include <USIWire.h>
#include <SoftwareSerial.h>

SoftwareSerial tinySerial(3, 4);

void setup()
{
  tinySerial.begin(9600);
  tinySerial.println("\nI2C Scanner");
  Wire.begin();

}


void loop()
{

  byte error, address;
  int nDevices;

  tinySerial.println("Scanning...");
  
  nDevices = 0;
  for (address = 1; address < 127; address++ )
  {
    // The i2c_scanner uses the return value of
    // the Write.endTransmisstion to see if
    // a device did acknowledge to the address.
    Wire.beginTransmission(address);
    error = Wire.endTransmission();

    if (error == 0)
    {
      tinySerial.print("I2C device found at address 0x");
      if (address < 16)
        tinySerial.print("0");
      tinySerial.print(address, HEX);
      tinySerial.println("  !");

      nDevices++;
    }
    else if (error == 4)
    {
      tinySerial.print("Unknown error at address 0x");
      if (address < 16)
        tinySerial.print("0");
      tinySerial.println(address, HEX);
    }
  }
  if (nDevices == 0)
    tinySerial.println("No I2C devices found\n");
  else
    tinySerial.println("done\n");



}
