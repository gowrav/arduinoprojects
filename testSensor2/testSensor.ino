#include <NewPing.h>

#define TRIGGER_PIN  2  // Arduino pin tied to trigger pin on the ultrasonic sensor.
#define ECHO_PIN     3  // Arduino pin tied to echo pin on the ultrasonic sensor.
#define MAX_DISTANCE 200 // Maximum distance we want to ping for (in centimeters). Maximum sensor distance is rated at 400-500cm.
int dOut_in = 4;
int dOut_out = 5;
int laser_1 = 6;
int laser_2 = 7;
long incoming;
long outgoing;
long inside;
long totalInOut;
int delayonIntercept = 5000;

int avgStrike = 5;
int sonarDist = 0;

boolean triggerEnabled = false;
NewPing sonar(TRIGGER_PIN, ECHO_PIN, MAX_DISTANCE); // NewPing setup of pins and maximum distance.

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  // mySerial.begin(9600);
  pinMode(laser_1, OUTPUT);
  pinMode(laser_2, OUTPUT);

  pinMode(dOut_in, INPUT);
  pinMode(dOut_out, INPUT);

  digitalWrite(laser_1, HIGH);
  digitalWrite(laser_2, HIGH);
  incoming = 0;
  outgoing = 0;
  inside = 0;
  totalInOut = 0;
  triggerEnabled = true;

  Serial.println("Init"); // Send ping, get distance in cm and print result (0 = outside set distance range)
  delay(2000);
  Serial.println("LetsBegin"); // Send ping, get distance in cm and print result (0 = outside set distance range)

}


void loop() {

  while ((digitalRead(dOut_in) && digitalRead(dOut_out)));
  {
    delayMicroseconds(500);//check for bounce and confirm
    int inTrig = digitalRead(dOut_in);
    int outTrig = digitalRead(dOut_out);
    if (!(inTrig && outTrig))
    {

      if (timeTrapper())
        if (!inTrig)
          incoming++;
        else
          outgoing++;

      digitalWrite(laser_1, HIGH);
      digitalWrite(laser_2, HIGH);

      Serial.print(incoming); // Send ping, get distance in cm and print result (0 = outside set distance range)
      Serial.print(','); // Send ping, get distance in cm and print result (0 = outside set distance range)
      Serial.print(outgoing); // Send ping, get distance in cm and print result (0 = outside set distance range)
      Serial.print(','); // Send ping, get distance in cm and print result (0 = outside set distance range)
      Serial.println(incoming - outgoing); // Send ping, get distance in cm and print result (0 = outside set distance range)
    }
  }

  delay(2);
}

bool timeTrapper()
{
  digitalWrite(laser_1, LOW);
  digitalWrite(laser_2, LOW);
  // get current time stamp
  // only need one for both if-statements
  unsigned long currentMillis = millis();
  int repeatCount = 5;
  boolean sonarClear = false;
  do
  {
    int curStrike = 0;
    sonarDist = 0;
    while (curStrike < avgStrike)
    {
      int temp = sonar.ping_cm();
      if (temp > 0) {
        sonarDist += temp;
        curStrike++;
      }
      delayMicroseconds(500);//check for bounce and confirm
    }
    sonarDist = sonarDist / 5;

    if (sonarDist >= 50)
    {
      sonarClear = true;
    }
    if ((millis() >= currentMillis + (delayonIntercept * repeatCount)))
    {
      return false;
    }

  }
  while (((millis() < currentMillis + delayonIntercept) && !sonarClear));
  return true;

}

void sendData(long incoming, long outgoing, long inside, long totalInOut)
{
  String data = "{";
  data.concat(incoming);
  data.concat(",");
  data.concat(outgoing);
  data.concat(",");
  data.concat(inside);
  data.concat(",");
  data.concat(totalInOut);
  data.concat("}");

  char* buf1;
  data.toCharArray(buf1, data.length() + 1);
  Serial.println(data);
  Serial.write(buf1);
}
