/* Encoder Library - TwoKnobs Example
   http://www.pjrc.com/teensy/td_libs_Encoder.html

   This example code is in the public domain.
*/

#include <Encoder.h>

Encoder knob(2,3);

void setup() {
  Serial.begin(9600);
  Serial.println("Knob Encoder Test:");
}

long positionNow  = -999;
long newVal;

void loop() {
  newVal = knob.read();
  if (newVal != positionNow) {
    Serial.print("Val = ");
    Serial.println(newVal);
    positionNow = newVal;
  }
  // if a character is sent from the serial monitor,
  // reset both back to zero.
  if (Serial.available()) {
    Serial.read();
    Serial.println("Reset knob to zero");
    knob.write(0);
  }
}
