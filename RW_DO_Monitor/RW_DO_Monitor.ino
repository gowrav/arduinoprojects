/*
  This code is an intellectual property of GeekSynergy Technologies Pvt. Ltd.

  created
  by Gowrav L

  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  Code Does the following in an Arduino Uno + Boards
  Maintain RTC
  Coonect to LCD
  Show Time / DO values on LCD Display
  Connects to WiFi
  Connects to GSM Shield
  Monitors GSM SMS / Ping Request
  Logs DO Sensors Level
  Posts the Sensor Value on to WebSite
  ESP 8266 -> for Debugging
*/


#include <String.h>
#include <Wire.h>
#include "RTClib.h"
#include <TimeLib.h>
#include <TimeAlarms.h>

// include the library code:
#include <LiquidCrystal.h>


#define dbgSerial Serial

/////////////////////////// End All Includes Here /////////////////

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~GSM SETUP~~~~~~~~~~~~~~~~~~~~~~~~~
// Refer: http://www.seeedstudio.com/wiki/GPRS_Shield_V1.0 , http://linksprite.com/wiki/index.php5?title=SIM900_GPRS/GSM_Shield
//GSM PIN Connections
// GSM Rst -> 7 // Not Used Currently
// GSM Tx  -> Serial1
// GSM Rx  -> Serial1

// PIN Number for SIM if Locked
#define PINNUMBER ""

// APN data
//#define GPRS_APN       "rcomnet" // replace your GPRS APN
#define GPRS_LOGIN     ""    // replace with your GPRS login
#define GPRS_PASSWORD  "" // replace with your GPRS password
String GPRS_APN = "airtelgprs.com";// Phone 9148660455

// URL, path & port (for example: arduino.cc)
char serverDomainName[] = "borninmud.com";//"arduino.cc";
char LocalserverDomainName[] = "192.168.1.58";//"arduino.cc";
char Localpath[] = "/sewage-monitoring-system/insert.php?LocationID=5&InputDO=25&OutputDO=45&Update=unknownseq";//"/asciilogo.txt"; // http://192.168.1.53/sewage-monitoring-system/insert.php?LocationID=HAS_TEST&InputDO=25&OutputDO=45&Update=unknownseq
char path[] = "/insert.php?LocationID=5&InputDO=25&OutputDO=45&Update=unknownseq";//"/asciilogo.txt"; // http://192.168.1.53/sewage-monitoring-system/insert.php?LocationID=HAS_TEST&InputDO=25&OutputDO=45&Update=unknownseq
int port = 80; // port 80 is the default for HTTP

// Array to hold the number a SMS is retreived from
char senderNumber[20] = "+919743784016";
int upload_Interval = 60;

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ESP8266 SETUP~~~~~~~~~~~~~~~~~~~~~~~~~
/*ESP8266 library
  // Refer:
  // also Library from https://github.com/itead/ITEADLIB_Arduino_ESP8266

  When you use with UNO board, uncomment the follow line in uartWIFI.h.
  #define UNO

  When you use with MEGA board, uncomment the follow line in uartWIFI.h.
  #define MEGA

  Connection:
  When you use it with UNO board, the connection should be like these:
  ESP8266_TX->D0
  ESP8266_RX->D1
  ESP8266_CH_PD->3.3V
  ESP8266_VCC->3.3V
  ESP8266_GND->GND

  FTDI_RX->D3      //The baud rate of software serial can't be higher that 19200, so we use software serial as a debug port
  FTDI_TX->D2

  When you use it with MEGA board, the connection should be like these:
  ESP8266_TX->RX1(D19)
  ESP8266_RX->TX1(D18)
  ESP8266_CH_PD->3.3V
  ESP8266_VCC->3.3V
  ESP8266_GND->GND

  When you want to output the debug information, please use DebugdbgSerial. For example,

  DebugdbgSerial.println("hello");
  Note:  The size of message from ESP8266 is too big for arduino sometimes, so the library can't receive the whole buffer because
  the size of the hardware serial buffer which is defined in HardwaredbgSerial.h is too small.

  Open the file from \arduino\hardware\arduino\avr\cores\arduino\HardwaredbgSerial.h.
  See the follow line in the HardwaredbgSerial.h file.

  #define SERIAL_BUFFER_SIZE 64

  The default size of the buffer is 64. Change it into a bigger number, like 256 or more.

  // Comment //#include <SoftwaredbgSerial.h> from ITEADLIB -> UartWIFI.h Library

*/


#define SSID       "GeekSynergy_Base" // Change to YOUR SSID or add an option do it from srial
#define PASSWORD   "p!a@s#s$w%o^r&d*"  // Change to YOUR SSID Password or add an option do it from srial

#include "uartWIFI.h"
//#include <SoftwaredbgSerial.h>
WIFI wifi;

unsigned long lastConnectionTime = 0;          // last time you connected to the server, in milliseconds
boolean lastConnected = false;                 // state of the connection last time through the main loop
const unsigned long postingInterval = 8 * 1000; // delay between updates, in milliseconds

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~RTC SETUP~~~~~~~~~~~~~~~~~~~~~~~~~

// Refer: http://microcontrollerslab.com/real-time-clock-ds1307-interfacing-arduino/
// also Library from https://github.com/adafruit/RTClib but take care of this issue /RTClib.h:79:31: error: redeclaration of 'OFF'
// https://learn.adafruit.com/ds1307-real-time-clock-breakout-board-kit/understanding-the-code
RTC_DS1307 RTC;
// set alarm through this forum https://forums.adafruit.com/viewtopic.php?f=31&p=207652
// and code taken from http://playground.arduino.cc/Code/Time


//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Sesnor SETUP~~~~~~~~~~~~~~~~~~~~~~~~~

//int inSensorPin = A2;
//int outSensorPin = A3;

int totalSensorReadings = 50;
float inDOReading[50];
float outDOReading[50];

int lastUpdatedPointer;
int lastUploadedPointer;

//--------------------------------- Atlas Scientific ------------------------------------


//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Shared Variables~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

// timeout
const unsigned long __TIMEOUT__ = 10 * 1000;

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


String IMEI_ID = "";
bool imei_obtained = false;

// initialize the library with the numbers of the interface pins
LiquidCrystal lcd(6, 7, 5, 4, 3, 2);

void GSMsetup(long baudRate)
{
  Serial1.begin(baudRate);               // the GPRS baud rate
  delay(500);
  //  dbgSerial.println("Waiting for Modem to settle in....8"); delay(1000);
  //  Serial1.flush(); // remove everthing in the buffer untill now
  //  dbgSerial.println("Waiting for Modem to settle in....7"); delay(1000);
  //  dbgSerial.println("Waiting for Modem to settle in....6"); delay(1000);
  //  Serial1.flush(); // remove everthing in the buffer untill now
  dbgSerial.println("Waiting for Modem to settle in....5"); delay(1000);
  dbgSerial.println("Waiting for Modem to settle in....4"); delay(1000);
  Serial1.flush(); // remove everthing in the buffer untill now
  dbgSerial.println("Waiting for Modem to settle in....3"); delay(1000);
  dbgSerial.println("Waiting for Modem to settle in....2"); delay(1000);
  Serial1.flush(); // remove everthing in the buffer untill now
  dbgSerial.println("Waiting for Modem to settle in....1"); delay(1000);
  Serial1.flush(); // remove everthing in the buffer untill now
  Serial1.print("AT+CGSN\r");    //Because we want to send the IMEI in text mode
  delay(30);
  dbgSerial.println(Serial1.readStringUntil(13));
  dbgSerial.println(Serial1.readStringUntil(13));
  dbgSerial.println(Serial1.readStringUntil('\n'));
  IMEI_ID = Serial1.readStringUntil(13);         //read the string until we see a <CR>
  dbgSerial.print("IMEI : ");
  dbgSerial.println(IMEI_ID);
  //dbgSerial.println("End of IMEI");
}

void GSMsendSMS()
{
  Serial1.print("AT+CMGF=1\r");    //Because we want to send the SMS in text mode
  delay(100);
  Serial1.println("AT + CMGS = \"" + String(senderNumber) + "\""); //send sms message, be careful need to add a country code before the cellphone number
  delay(100);
  Serial1.println("A test message!");//the content of the message
  delay(100);
  Serial1.println((char)26);//the ASCII code of the ctrl+z is 26
  delay(100);
  Serial1.println();
}

void GSMvoicCall()
{
  Serial1.println("ATD + " + String(senderNumber) + ";"); //dial the number
  delay(100);
  Serial1.println();
}
void GSMclientPostGPRS()
{
  Serial1.println("AT+CSQ");
  delay(100);

  ShowSerialData();// this code is to show the data from gprs shield, in order to easily see the process of how the gprs shield submit a http request, and the following is for this purpose too.

  Serial1.println("AT+CGATT?");
  delay(100);

  ShowSerialData();

  Serial1.println("AT+SAPBR=3,1,\"CONTYPE\",\"GPRS\"");//setting the SAPBR, the connection type is using gprs
  delay(1000);

  ShowSerialData();

  Serial1.println("AT+SAPBR=3,1,\"APN\",\"" + GPRS_APN + "\""); //setting the APN, the second need you fill in your local apn server
  delay(4000);

  //  Serial1.println("AT+SAPBR=3,1,\"PHONENUM\",\"*99#\"");//setting the APN, the second need you fill in your local apn server
  //  delay(4000);

  ShowSerialData();

  Serial1.println("AT+SAPBR=1,1");//setting the SAPBR, for detail you can refer to the AT command mamual
  delay(2000);

  ShowSerialData();

  Serial1.println("AT+HTTPINIT"); //init the HTTP request

  delay(2000);
  ShowSerialData();

  Serial1.println("AT+HTTPPARA=\"URL\",\"" + String(serverDomainName) + ":" + String(port) + String(path) + "\""); // setting the httppara, the second parameter is the website you want to access
  delay(1000);

  ShowSerialData();

  Serial1.println("AT+HTTPACTION=0");//submit the request
  delay(10000);//the delay is very important, the delay time is base on the return from the website, if the return datas are very large, the time required longer.
  //while(!Serial1.available());

  ShowSerialData();

  Serial1.println("AT+HTTPREAD");// read the data from the website you access
  delay(300);

  ShowSerialData();

  Serial1.println("");
  delay(100);
  dbgSerial.println("Action Complete");// read the data from the website you access
}

void ShowSerialData()
{
  while (Serial1.available() != 0)
  {
    String temp = Serial1.readStringUntil(13);
    dbgSerial.println(temp);
    //    lcd.setCursor(0, 1);
    //    lcd.print("Dbg:" + temp);
  }

  // This is for WiFi data
  char message[64];
  if (wifi.ReceiveMessage(message))
  {
    dbgSerial.println(message);
  }
  // This is for Sensor data
  int  temp = (lastUpdatedPointer - 1) % totalSensorReadings;
  //dbgSerial.print(inDOReading[temp]); dbgSerial.print(","); dbgSerial.println(outDOReading[temp]);
  dbgSerial.print(inDOReading[temp]); dbgSerial.print("  -DO->  "); dbgSerial.println(outDOReading[temp]);
}

void ESP8266Init(int baudRate)
{
  wifi.begin(baudRate);
  bool b = wifi.Initialize(STA, SSID, PASSWORD);
  if (!b)
  {
    dbgSerial.println("Init error");
  }
  delay(8000);  //make sure the module can have enough time to get an IP address
  String ipstring  = wifi.showIP();
  dbgSerial.println("My IP address:");
  dbgSerial.println(ipstring);    //show the ip address of module

  String wifistring  = wifi.showJAP();
  dbgSerial.println(wifistring);    //show the name of current wifi access port

}

void getESP8266RemoteServerStatus()
{

}

void ESP8266UpdateInfoLocally()
{
  // if there's a successful connection:
  if (wifi.ipConfig(TCP, LocalserverDomainName, port)) {
    dbgSerial.println("connecting...");
    // send the HTTP PUT request:
    //wifi.Send("GET / HTTP/1.0\r\n\r\n");
    int  temp = (lastUpdatedPointer - 1) % totalSensorReadings;
    //dbgSerial.print(inDOReading[temp]); dbgSerial.print(","); dbgSerial.println(outDOReading[temp]);
    String wifiSendString = "GET /sewage-monitoring-system/insert.php?LocationID=5&InputDO=" + String(inDOReading[temp]) + "&OutputDO=" +  String(outDOReading[temp]) + "&Update=" + RTC_CurrentDateTimeString() + "\" HTTP/1.0\r\n\r\n";
    //dbgSerial.println(wifiSendString);
    wifi.Send(wifiSendString);
    // note the time that the connection was made:
    lastConnectionTime = millis();
  }
  else {
    // if you couldn't make a connection:
    dbgSerial.println("connection failed");
    dbgSerial.println("disconnecting.");
    wifi.closeMux();
  }
}

void ESP8266UpdateInfoHosted()
{
  // if there's a successful connection:
  if (wifi.ipConfig(TCP, serverDomainName, port)) {
    dbgSerial.println("connecting...");
    // send the HTTP PUT request:
    //wifi.Send("GET / HTTP/1.0\r\n\r\n");
    int  temp = (lastUpdatedPointer - 1) % totalSensorReadings;
    String wifiSendString = "GET /insert.php?LocationID=5&InputDO=" + String(inDOReading[temp]) + "&OutputDO=" +  String(outDOReading[temp]) + "&Update=" + "test" + " HTTP/1.0\r\n\r\n";
    //"GET /insert.php?LocationID=5&InputDO=-1.07&OutputDO=-2.10&Update=2016/3/23>13:4/3" HTTP/1.0
    dbgSerial.println(wifiSendString);
    //dbgSerial.println(wifiSendString);
    wifi.Send(wifiSendString);
    // note the time that the connection was made:
    lastConnectionTime = millis();
  }
  else {
    // if you couldn't make a connection:
    dbgSerial.println("connection failed");
    dbgSerial.println("disconnecting.");
    wifi.closeMux();
  }
}

void ESP8266TestArduino()
{
  // if there's a successful connection:
  if (wifi.ipConfig(TCP, LocalserverDomainName, port)) {
    dbgSerial.println("connecting...");
    // send the HTTP PUT request:
    //wifi.Send("GET / HTTP/1.0\r\n\r\n");
    String wifiSendString = "GET " + String(Localpath) + " HTTP/1.0\r\n\r\n";
    //dbgSerial.println(wifiSendString);
    wifi.Send(wifiSendString);
    // note the time that the connection was made:
    lastConnectionTime = millis();
  }
  else {
    // if you couldn't make a connection:
    dbgSerial.println("connection failed");
    dbgSerial.println("disconnecting.");
    wifi.closeMux();
  }

}


void RTC_init()
{
  Wire.begin();
  RTC.begin();
  if (!RTC.isrunning()) {
    dbgSerial.println("RTC is NOT running!");
    // following line sets the RTC to the date & time this sketch was compiled
    RTC.adjust(DateTime(__DATE__, __TIME__));
  }
}

void RTC_currentDateTime()
{
  DateTime now = RTC.now();
  dbgSerial.print(now.year(), DEC); dbgSerial.print('/'); dbgSerial.print(now.month(), DEC); dbgSerial.print('/'); dbgSerial.print(now.day(), DEC);
  dbgSerial.print(' '); dbgSerial.print(now.hour(), DEC); dbgSerial.print(':'); dbgSerial.print(now.minute(), DEC); dbgSerial.print(':'); dbgSerial.println(now.second(), DEC);
}
String RTC_CurrentDateTimeString()
{
  DateTime now = RTC.now();
  return String(now.year()) + "/" + String(now.month()) +  "/"  + String(now.day()) +  ">"  + String(now.hour()) +  ":"  + String(now.minute()) +  "/"  + String(now.second());
}



void lcd_init() {
  // set up the LCD's number of columns and rows:
  lcd.begin(16, 2);
  // Print a message to the LCD.
  lcd.print("Rite-Ways Enviro");
}


void disp_lcd() {
  // set the cursor to column 0, line 1
  // (note: line 1 is the second row, since counting begins with 0):
  lcd.setCursor(0, 1);
  // print the number of seconds since reset:
  //lcd.print(millis() / 1000);
  int  temp = (lastUpdatedPointer - 1) % totalSensorReadings;
  float inReadings = (inDOReading[temp - 1] + inDOReading[temp]) / 2;
  float outReadings = (outDOReading[temp - 1] + outDOReading[temp]) / 2;
  lcd.print(inReadings); lcd.print(" >-DO-> "); lcd.print(outReadings);

}



String insensorstring = "";                             //a string to hold the data from the Atlas Scientific product
String outsensorstring = "";                             //a string to hold the data from the Atlas Scientific product
boolean sensor_string_complete = false;               //have we received all the data from the Atlas Scientific product
float inDO;
float outDO;

void sensorInit()
{
  //memset( inDOReading, 0, 50*sizeof(float) );
  //memset( outDOReading, 0, 50*sizeof(float) );

  Serial3.begin(9600);                                //set baud rate for software serial port_3 to 9600
  Serial2.begin(9600);                                //set baud rate for software serial port_3 to 9600
  insensorstring.reserve(30);                           //set aside some bytes for receiving data from Atlas Scientific product
  outsensorstring.reserve(30);                           //set aside some bytes for receiving data from Atlas Scientific product
}

void serialEvent3() {                                 //if the hardware serial port_3 receives a char
  insensorstring = Serial3.readStringUntil(13);         //read the string until we see a <CR>
  sensor_string_complete = true;                      //set the flag used to tell if we have received a completed string from the PC
}
void serialEvent2() {                                 //if the hardware serial port_3 receives a char
  outsensorstring = Serial2.readStringUntil(13);         //read the string until we see a <CR>
  sensor_string_complete = true;                      //set the flag used to tell if we have received a completed string from the PC
}

void latestSensorData()
{
  //  inDOReading[lastUpdatedPointer] = mapfloat(analogRead(inSensorPin), 200, 800, 0.0, 20.00); //analogRead(inSensorPin);
  //  outDOReading[lastUpdatedPointer] = mapfloat(analogRead(outSensorPin),200, 800, 0.0, 20.00); //analogRead(outSensorPin);
  //  inDOReading[lastUpdatedPointer] = 0;

  if (sensor_string_complete == true) {               //if a string from the Atlas Scientific product has been received in its entirety
    //dbgSerial.println(sensorstring);                     //send that string to the PC's serial monitor
    if (isdigit(insensorstring[0])) {                   //if the first character in the string is a digit
      inDO = insensorstring.toFloat();                    //convert the string to a floating point number so it can be evaluated by the Arduino
      //      if (inDO >= 6.0) {                                //if the DO is greater than or equal to 6.0
      //        //dbgSerial.println("high");                       //print "high" this is demonstrating that the Arduino is evaluating the DO as a number and not as a string
      //      }
      //      if (inDO <= 5.99) {                               //if the DO is less than or equal to 5.99
      //        //dbgSerial.println("low");                        //print "low" this is demonstrating that the Arduino is evaluating the DO as a number and not as a string
      //      }
    }

    if (isdigit(outsensorstring[0])) {                   //if the first character in the string is a digit
      outDO = outsensorstring.toFloat();                    //convert the string to a floating point number so it can be evaluated by the Arduino
      //      if (inDO >= 6.0) {                                //if the DO is greater than or equal to 6.0
      //        //dbgSerial.println("high");                       //print "high" this is demonstrating that the Arduino is evaluating the DO as a number and not as a string
      //      }
      //      if (inDO <= 5.99) {                               //if the DO is less than or equal to 5.99
      //        //dbgSerial.println("low");                        //print "low" this is demonstrating that the Arduino is evaluating the DO as a number and not as a string
      //      }
    }

    outDOReading[lastUpdatedPointer] = outDO;
    inDOReading[lastUpdatedPointer] = inDO;
    lastUpdatedPointer++;
    lastUpdatedPointer = lastUpdatedPointer % totalSensorReadings;
  }

  insensorstring = "";                                  //clear the string:
  outsensorstring = "";                                  //clear the string:
  sensor_string_complete = false;                     //reset the flag used to tell if we have received a completed string from the Atlas Scientific product
}


void GSMupdateSensorData()
{
  // Upload the data untill the circular register storage of lastupdatedPointer = lastuploadedPointer
  if (!imei_obtained)
  {
    GSMsetup(115200);
    imei_obtained = true;
  }
  //    // Force IMEI
  //    IMEI_ID = "867330028595281";
  //     lcd.setCursor(0, 1);
  //    lcd.print("IMEI:"); lcd.print(IMEI_ID);
  Serial1.flush(); // remove everthing in the buffer untill now

  int  temp = (lastUpdatedPointer - 1) % totalSensorReadings;

  float inReadings = (inDOReading[temp - 1] + inDOReading[temp]) / 2;
  float outReadings = (outDOReading[temp - 1] + outDOReading[temp]) / 2;

  Serial1.println("AT+CSQ");
  delay(100);

  ShowSerialData();// this code is to show the data from gprs shield, in order to easily see the process of how the gprs shield submit a http request, and the following is for this purpose too.
  lcd.setCursor(0, 1);
  lcd.print("IMEI:"); lcd.print(IMEI_ID);
  delay(300);

  Serial1.println("AT+CGATT?");
  delay(100);

  Serial1.println("AT+SAPBR=3,1,\"CONTYPE\",\"GPRS\"");//setting the SAPBR, the connection type is using gprs
  delay(1000);

  ShowSerialData();
  ShowSerialData();

  Serial1.println("AT+SAPBR=3,1,\"APN\",\"" + GPRS_APN + "\""); //setting the APN, the second need you fill in your local apn server
  lcd.setCursor(0, 1);
  lcd.print(inReadings); lcd.print(" >-DO-> "); lcd.print(outReadings);
  delay(4000);

  //  Serial1.println("AT+SAPBR=3,1,\"PHONENUM\",\"*99#\"");//setting the APN, the second need you fill in your local apn server
  //  delay(4000);

  ShowSerialData();

  Serial1.println("AT+SAPBR=1,1");//setting the SAPBR, for detail you can refer to the AT command mamual
//  lcd.setCursor(0, 1);
//  lcd.print("UpdatingRecord:---->");
  delay(2000);

  ShowSerialData();

  Serial1.println("AT+HTTPINIT"); //init the HTTP request

  delay(2000);
  ShowSerialData();

  dbgSerial.println("AT+HTTPPARA=\"URL\",\"" + String(serverDomainName) + ":" + String(port) + "/insert.php?LocationID=0&InputDO=" + inReadings + "&OutputDO=" +  outReadings + "&IMEI_ID=" + IMEI_ID + "&Update=" + RTC_CurrentDateTimeString() + "\""); // setting the httppara, the second parameter is the website you want to access
  Serial1.println("AT+HTTPPARA=\"URL\",\"" + String(serverDomainName) + ":" + String(port) + "/insert.php?LocationID=0&InputDO=" + inReadings + "&OutputDO=" +  outReadings + "&IMEI_ID=" + IMEI_ID + "&Update=" + RTC_CurrentDateTimeString() + "\""); // setting the httppara, the second parameter is the website you want to access


  delay(2000);
  ShowSerialData();

  Serial1.println("AT+HTTPACTION=0");//submit the request
  delay(10000);//the delay is very important, the delay time is base on the return from the website, if the return datas are very large, the time required longer.
  //while(!Serial1.available());

  ShowSerialData();

  Serial1.println("AT+HTTPREAD");// read the data from the website you access
  delay(300);

  ShowSerialData();

  Serial1.println("");
  delay(100);
  dbgSerial.println("Action Complete");// read the data from the website you access
  lcd.setCursor(0, 1);
  lcd.print("ActionComplete:Upld");
  Serial1.println("AT+HTTPTERM");// read the data from the website you access
  delay(300);


  Serial1.println("");
  delay(100);
  dbgSerial.println("AT Link Terminated");// read the data from the website you access

  lcd.setCursor(0, 1);
  lcd.print("AT Link:Termin~d");
  delay(400);
  Serial1.flush(); // remove everthing in the buffer untill now

  //Increment this if there is a success or exit
  {
    lastUploadedPointer++;
  }

}

void RTC_Schedule()
{
  // setSyncProvider(syncProvider);     //reference our syncProvider function instead of RTC_DS1307::get()
  Alarm.timerRepeat(upload_Interval, upload_DO_onGSM);            // timer for every 5 minutes
  // Alarm.timerRepeat(30, upload_DO_onWiFi);            // timer for every 30 seconds
}

void upload_DO_onGSM()
{
  dbgSerial.println("GSM-DataUpdate Started");
  GSMupdateSensorData();
  dbgSerial.println("GSM-DataUpdate Complete");

}

void upload_DO_onWiFi()
{
  dbgSerial.println("WiFi-DataUpdate Started");
  //ESP8266UpdateInfoLocally();
  ESP8266UpdateInfoHosted();
  dbgSerial.println("WiFi-DataUpdate Complete");
}





float mapfloat(long x, long in_min, long in_max, long out_min, long out_max) // Map the large int value to floating numbers
{
  return (float)(x - in_min) * (out_max - out_min) / (float)(in_max - in_min) + out_min;
}

boolean led_state = true;
int led_pin = 13;
void setup() {
  pinMode(led_pin, OUTPUT);
  digitalWrite(led_pin, led_state);   // turn the LED on (HIGH is the voltage level)

  // Setup All Debug and Interface Communications
  lcd_init();
  dbgSerial.begin(115200);
  RTC_init(); // First Step is to fire up the timer

  //GSMsetup(115200); // could have been 115200 if changed from int to long
  //  ESP8266Init(9600);

  // Sensor Callibration and Initiation
  sensorInit();

  RTC_Schedule(); // Set data upload path here

}

void loop() {

  // timerLookOut();
  // watchDogLookOut();
  //  GSMupdateSensorData();
  // Continous debug Monitoring
  RTC_currentDateTime();
  latestSensorData();
  ShowSerialData();

  disp_lcd();
  Alarm.delay(1000);
  led_state = !led_state;
  digitalWrite(led_pin, led_state);   // turn the LED on (HIGH is the voltage level)

}


