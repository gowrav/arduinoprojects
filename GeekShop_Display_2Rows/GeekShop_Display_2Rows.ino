#include <SoftwareSerial.h>
#include <EEPROM.h>
#include "default_font.h"

#define NO_SLAB  1
#define NO_SLICE  1
#define NO_CHAR   8
#define REP_COUNT   2
#define CHAR_LINES   16

int LED_BRIGHTNESS =  400; // 0 to 1000 // 400 is optimum

String dispString = "AIR - Digital Radio Mondiale";
String content1 = "First Line";
String content2 = "Second Line";


float f = 123.456f;
int cPath = 12;  // content storage address
int bPath = 2;  // 112  brightness controll address
int sPath = 4;  // 115 scrolling direction controll address , ic left or right, 0 means left to right and 1 means right to left
int spPath = 6;  //  117 speed of the scrolling address
int stylepath = 8;  //  display style address

int bVal = 72;  //  brightness value
int sVal = 82;  //  scrolling direction value , ic left or right
int spVal = 92;  //  speed of the scrolling value, it is integer value
int contLen = 0;
boolean getCont = false;//true;
int sSpeed = 45;
int style = 1;  // 0 means static and 1 means scrolling


int latchPin = A3;//8; //LT
int clockPin = 13;//12; //SK
int dataPin = 7;//11; //R1

int en_74138 = A2;//2;

int la_74138 = 5;//3;
int lb_74138 = 6;//4;

int k = 0;
int lTimes = 2;


void shiftOut(unsigned char dataOut)
{
  for (int i = 7; i >= 0  ; i--)
  {
#if defined(__AVR_ATmega1280__) || defined(__AVR_ATmega2560__)
    // Fast Data Toggler
    PORTB &= ~(1 << (6));
    if (dataOut & (0x01 << i)) PORTB &= ~(1 << (5));
    else
      PORTB |= 1 << (5);
    PORTB |= 1 << (6);
#endif
//
//#if defined(__AVR_ATmega328P__) || defined(__AVR_ATmega168__)
//    // Super Fast Data Toggler
//    PORTB &= 0xDF;
//    if (dataOut & (0x01 << i)) PORTD &= 0x7F;
//    else
//      PORTD |= 0x80;
//    PORTB |= 0x20;
//#endif
  }
}


void cyclePower(int l)
{
  digitalWrite(en_74138, LOW);
  digitalWrite(la_74138, l % 2 > 0);
  digitalWrite(lb_74138, l % 4 > 1);
  digitalWrite(en_74138, HIGH);
  delayMicroseconds(LED_BRIGHTNESS);
  digitalWrite(en_74138, LOW);
}




uint8_t msg_board[] =
{
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
};



uint8_t msgfilled_board[] =
{
  0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
  0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
  0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
  0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
  0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
  0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
  0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
  0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF
};


void display_msg(String eightchars, int delaySec)
{
  while (delaySec--) {
    display_msg(eightchars);

  }
}


void display_msg(String eightchars)
{

  // Reduce string length to 8
  if (eightchars.length() > 8)
    eightchars = eightchars.substring(0, 8);

  // Optimize string with spaces to 8
  if (eightchars.length() < 8)
  {
    String temp = "";
    for (int i = 0; i < (8 - eightchars.length()) / 2; i++)
      temp += " ";
    temp += eightchars;
    for (int i = 0; i < (((8 - eightchars.length()) / 2) + eightchars.length() % 2); i++)
      temp += " ";
    eightchars = temp;
  }


  // Set msgBoardData here
  for (int y = 0; y < NO_CHAR; y++)
    for (int x = 0; x < CHAR_LINES; x++)
    {
      msg_board[(CHAR_LINES * y) + x] = pgm_read_word_near(font8x16_basic + (eightchars.charAt(y) * CHAR_LINES) + x);
    }
  digitalWrite(en_74138, LOW);//Turn off display
  for (int i = 0; i < NO_SLAB * NO_SLICE; i++)
  {
    for (int l = 3; l >= 0 ; l--)
    {
      for (int k = 0; k < REP_COUNT; k++)
      {
        for (int m = 0; m < NO_CHAR ; m++)
        {
          shiftOut(msg_board[m * CHAR_LINES + ((4 * 3) + l)]);
          shiftOut(msg_board[m * CHAR_LINES + ((4 * 2) + l)]);
          shiftOut(msg_board[m * CHAR_LINES + ((4 * 1) + l)]);
          shiftOut(msg_board[m * CHAR_LINES + (l)]);
          digitalWrite(latchPin, LOW); digitalWrite(latchPin, HIGH); digitalWrite(latchPin, LOW);
          //delay(10);
        }
      }

      cyclePower(l);
    }
  }
}


void display_msgfilled()
{

  digitalWrite(en_74138, LOW);//Turn off display
  for (int i = 0; i < NO_SLAB * NO_SLICE; i++)
  {
    for (int l = 3; l >= 0 ; l--)
    {
      for (int k = 0; k < REP_COUNT; k++)
      {
        for (int m = 0; m < NO_CHAR ; m++)
        {
          shiftOut(msgfilled_board[m * CHAR_LINES + ((4 * 3) + l)]);
          shiftOut(msgfilled_board[m * CHAR_LINES + ((4 * 2) + l)]);
          shiftOut(msgfilled_board[m * CHAR_LINES + ((4 * 1) + l)]);
          shiftOut(msgfilled_board[m * CHAR_LINES + (l)]);
          digitalWrite(latchPin, LOW); digitalWrite(latchPin, HIGH); digitalWrite(latchPin, LOW);
          //delay(10);
        }
      }

      cyclePower(l);
    }
  }
}



void displayScroll_msg(String longString)
{

  longString = "        " + longString;

  for (int m = 0; m < longString.length(); m ++)
  {
    String shortString = "";
    if (m + 8 < longString.length())
      shortString = longString.substring(m, m + 8);
    else
      shortString = longString.substring(m);


    // Set msgBoardData here
    for (int y = 0; y < NO_CHAR; y++)
      for (int x = 0; x < CHAR_LINES; x++)
      {
        msg_board[(CHAR_LINES * y) + x] = pgm_read_word_near(font8x16_basic + (shortString.charAt(y) * CHAR_LINES) + x);
      }
    int k = sSpeed;
    while (k--)
    {

      digitalWrite(en_74138, LOW);//Turn off display
      for (int i = 0; i < NO_SLAB * NO_SLICE; i++)
      {
        for (int l = 3; l >= 0 ; l--)
        {
          for (int m = 0; m < NO_CHAR; m++)
          {
            shiftOut(msg_board[m * CHAR_LINES + ((4 * 3) + l)]);
            shiftOut(msg_board[m * CHAR_LINES + ((4 * 2) + l)]);
            shiftOut(msg_board[m * CHAR_LINES + ((4 * 1) + l)]);
            shiftOut(msg_board[m * CHAR_LINES + (l)]);
            digitalWrite(latchPin, LOW); digitalWrite(latchPin, HIGH); digitalWrite(latchPin, LOW);
          }
          cyclePower(l);
        }
      }
    }

  }
}

void display_flush()
{
  digitalWrite(en_74138, LOW);
  for (int l = 0; l < NO_SLAB * NO_SLICE ; l++)
  {
    shiftOut(0);
    digitalWrite(latchPin, LOW); digitalWrite(latchPin, HIGH); digitalWrite(latchPin, LOW);
  }
  digitalWrite(en_74138, HIGH);

}


void display_flood()
{
  int delaySec1 = 300;
  while (delaySec1--) {
    display_msgfilled();
  }
}

void init_matrixDisplay()
{
  pinMode(latchPin, OUTPUT);
  pinMode(clockPin, OUTPUT);
  pinMode(dataPin, OUTPUT);
  pinMode(la_74138, OUTPUT);
  pinMode(lb_74138, OUTPUT);
  pinMode(en_74138, OUTPUT);

  digitalWrite(en_74138, LOW);
  display_flush();
}

boolean testLED = false;

void setup()
{
  Serial.begin(9600); Serial.setTimeout(50);
  init_matrixDisplay();
  display_flush();
  for (int r = 0; r < 300; r++)
  {
    display_msg(dispString);
  }
  contLen = dispString.length();
  if (EEPROM.read(cPath) < 3)
  {
    EEPROM.write(cPath, contLen);
    for (int i = 0; i < dispString.length(); i++)
    {
      int count = cPath + 1 + i;
      EEPROM.write(count, dispString.charAt(i));
    }
    EEPROM.write(spPath, 25);
    EEPROM.write(bPath, 80);
    EEPROM.write(sPath, 1);
    EEPROM.write(stylepath, 0);
  }
}

int content_count = 0;
void loop() {
  //   initialize all the value from eeprom before start.

  dispString = content1;
  if (getCont) {
    getCont = false;
    contLen = EEPROM.read(cPath);
    content1 = "";
    for (int j = 0; j < contLen; j++)
    {
      int coV = cPath + 1 + j;
      content1 += char(EEPROM.read(coV));
    }
    dispString = content1;
    sSpeed = EEPROM.read(spPath); // adding new value to scrolling speed from bluetooth.
    sSpeed = 200 - sSpeed * 2;
    LED_BRIGHTNESS = EEPROM.read(bPath); // adding new value to scrolling speed from bluetooth.
    LED_BRIGHTNESS = LED_BRIGHTNESS * 15;
    style = EEPROM.read(stylepath);
  }
  else {
    if (style == 0)
    {
      display_msg(dispString, 100);
    }
    else
    {
      displayScroll_msg(dispString);
    }
  }
}

void changeContent(String data, int pathLen)
{
  getCont = true;
  dispString = data;
  int cLen = data.length();
  EEPROM.write(pathLen, cLen);
  for (int i = 0; i < data.length(); i++)
  {
    int count = pathLen + 1 + i;
    EEPROM.write(count, data.charAt(i));
  }
}


void setDataAttribute(int data, int pathLen)
{
  EEPROM.write(pathLen, data);
}

void clearEEpromData()
{
  for (int i = 0 ; i < EEPROM.length() ; i++)
  {
    EEPROM.write(i, 0);
  }
}
