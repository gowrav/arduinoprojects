/*
  This is a test sketch for the Adafruit assembled Motor Shield for Arduino v2
  It won't work with v1.x motor shields! Only for the v2's with built in PWM
  control

  For use with the Adafruit Motor Shield v2
  ---->	http://www.adafruit.com/products/1438
*/

#include <Wire.h>
#include <Adafruit_MotorShield.h>
#include "utility/Adafruit_MS_PWMServoDriver.h"

int ALTERNATE = 31;
float FULLCYCLE_STEP = 200.0;

int opListSize = 5;
char* opList[] = {"MIN","MAX", "DIR", "STP", "MOT"};


int MINDEG = 0;
int MAXDEG = 0;
int DIR = ALTERNATE;
int STP = DOUBLE;
int MOT = 0;

String cmdStr;

// Create the motor shield object with the default I2C address
Adafruit_MotorShield AFMS = Adafruit_MotorShield();
// Or, create it with a different I2C address (say for stacking)
// Adafruit_MotorShield AFMS = Adafruit_MotorShield(0x61);

// Connect a stepper motor with 200 steps per revolution (1.8 degree)
// to motor port #2 (M3 and M4)

Adafruit_StepperMotor *motorA = AFMS.getStepper(FULLCYCLE_STEP, 1);
Adafruit_StepperMotor *motorB = AFMS.getStepper(FULLCYCLE_STEP, 2);

void setup() {
  Serial.begin(115200);           // set up Serial library at 9600 bps
  Serial.println("DEVICE READY");

  AFMS.begin();  // create with the default frequency 1.6KHz
//  AFMS.begin(3000);  // OR with a different frequency, say 1KHz

  // Set Motor Speed
  motorA->setSpeed(300);  // 300 rpm
  motorB->setSpeed(300);  // 300 rpm
}

void loop() {
  int DEG =random(MINDEG, MAXDEG+1);
  runMotor((int)  abs((DEG * FULLCYCLE_STEP) / 360.0 ), DIR, STP , MOT); // ( 100 / 50 / 200 / 10,  FORWARD / BACKWARD ,  SINGLE/DOUBLE/INTERLEAVE/MICROSTEP , )
}

void runMotor(int stepcnt, int dir, int stepsize, int motor_no) {
  switch (dir) {
    case FORWARD : rotateClock(stepcnt, stepsize, motor_no); break;
    case BACKWARD : rotateCounterClock(stepcnt, stepsize, motor_no); break;
    default:
      {
        rotateClock(stepcnt, stepsize, motor_no);
        rotateCounterClock(stepcnt, stepsize, motor_no);
      }
  }

}
void rotateClock(int stepcnt, int stepsize, int motor_no) {
  switch (motor_no)
  {
    case 0: motorA->step(stepcnt, FORWARD, stepsize); break;
    case 1: motorB->step(stepcnt, FORWARD, stepsize); break;
    default:
      {
        motorA->step(stepcnt, FORWARD, stepsize);
        motorB->step(stepcnt, FORWARD, stepsize);
      }
  }

}

void rotateCounterClock(int stepcnt, int stepsize, int motor_no) {
  switch (motor_no)
  {
    case 0: motorA->step(stepcnt, BACKWARD, stepsize); break;
    case 1: motorB->step(stepcnt, BACKWARD, stepsize); break;
    default:
      {
        motorA->step(stepcnt, BACKWARD, stepsize);
        motorB->step(stepcnt, BACKWARD, stepsize);
      }
  }

}

/*
  SerialEvent occurs whenever a new data comes in the
  hardware serial RX.  This routine is run between each
  time loop() runs, so using delay inside loop can delay
  response.  Multiple bytes of data may be available.
*/
void serialEvent() {
  boolean newdata = false;
  while (Serial.available() > 0)
  {
    char carbyte = Serial.read();
    if (carbyte == '\n' ||carbyte == '\r' )
      break;
    else
      cmdStr += carbyte;
    delayMicroseconds(100);
    newdata = true;
  }
  if (newdata) {
    switch (IsArrayContains(cmdStr.substring(0, 3), opList))
    {
//      {"MIN","MAX", "DIR", "STP", "MOT"};
      case 0  : setMINDegree(cmdStr.substring(4));  cmdStr.remove(0); break;
      case 1  : setMAXDegree(cmdStr.substring(4));  cmdStr.remove(0); break;
      case 2  : setDirection(cmdStr.substring(4)); cmdStr.remove(0); break;
      case 3  : setStep(cmdStr.substring(4)); cmdStr.remove(0); break;
      case 4  : setMotor(cmdStr.substring(4)); cmdStr.remove(0); break;
      default : Serial.println("ERROR"); cmdStr.remove(0); break;
    }
  }
}

void setMINDegree(String operand) {
  MINDEG = operand.toInt();
  Serial.println("MIN:" + String(MINDEG));
}
void setMAXDegree(String operand) {
  MAXDEG = operand.toInt();
  Serial.println("MAX:" + String(MAXDEG));
}

void setDirection(String operand) {
  DIR = (operand.equals("FORWARD") ? FORWARD : (operand.equals("BACKWARD")? BACKWARD : ALTERNATE));
  Serial.println("DIR:" + String(DIR));
}

void setStep(String operand) {
  //  SINGLE/DOUBLE/INTERLEAVE/MICROSTEP
  STP = (operand.equals("SINGLE") ? SINGLE : (operand.equals("DOUBLE") ? DOUBLE : (operand.equals("INTERLEAVE") ? INTERLEAVE : MICROSTEP)));
  Serial.println("STP:" + String(STP));
}

void setMotor(String operand) {//0 (motorA) , 1 (motorB), 2 (both Motors / default)
  MOT = operand.toInt();
  Serial.println("MOT:" + String(MOT));
}


int IsArrayContains(String searchValue, char* searchList[])
{
  searchValue.toUpperCase();
  {
    for (int i = 0; i < opListSize ; i++)
      if (searchValue == searchList[i])
        return i;
  }
  return -1;
}
