/*
  Blink
  Turns on an LED on for one second, then off for one second, repeatedly.

  Most Arduinos have an on-board LED you can control. On the Uno and
  Leonardo, it is attached to digital pin 13. If you're unsure what
  pin the on-board LED is connected to on your Arduino model, check
  the documentation at http://www.arduino.cc

  This example code is in the public domain.

  modified 8 May 2014
  by Scott Fitzgerald
*/

int k = 0;

// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin 13 as an output.
  pinMode(13, OUTPUT);
  pinMode(8, INPUT);
  Serial.begin(115200);

  digitalWrite(13, HIGH);

}

// the loop function runs over and over again forever
void loop() {
  digitalWrite(13, HIGH);
  if (!digitalRead(8))
  {
    delayMicroseconds(50);
    if (!digitalRead(8))
    {
      digitalWrite(13, LOW);
      Serial.print("Tripped");
      Serial.println(++k);
      delay(200);
    }

  }


}


