//This code was written to be easy to understand.
//Code efficiency was not considered.
//Modify this code as you see fit.
//This code will output data to the Arduino serial monitor.
//Type commands into the Arduino serial monitor to control the DO circuit.
//This code was written in the Arduino 1.6.5 IDE
//An Arduino Mega was used to test this code.


String inputstring = "";                              //a string to hold incoming data from the PC
String insensorstring = "";                             //a string to hold the data from the Atlas Scientific product
String outsensorstring = "";                             //a string to hold the data from the Atlas Scientific product
boolean input_string_complete = false;                //have we received all the data from the PC
boolean insensor_string_complete = false;               //have we received all the data from the Atlas Scientific product
boolean outsensor_string_complete = false;               //have we received all the data from the Atlas Scientific product
float inDO;                                             //used to hold a floating point number that is the inDO
float outDO;                                             //used to hold a floating point number that is the outDO


void setup() {                                        //set up the hardware
  Serial.begin(115200);                                 //set baud rate for the hardware serial port_0 to 9600
  Serial2.begin(9600);                                //set baud rate for software serial port_3 to 9600
  Serial3.begin(9600);                                //set baud rate for software serial port_3 to 9600
  inputstring.reserve(10);                            //set aside some bytes for receiving data from the PC
  insensorstring.reserve(30);                           //set aside some bytes for receiving data from Atlas Scientific product
  outsensorstring.reserve(30);                           //set aside some bytes for receiving data from Atlas Scientific product
}

void serialEvent() {                                  //if the hardware serial port_0 receives a char
  inputstring = Serial.readStringUntil(13);           //read the string until we see a <CR>
  input_string_complete = true;                       //set the flag used to tell if we have received a completed string from the PC
  if (inputstring.equals("Cal")) {
    Serial2.println("Cal");
    Serial3.println("Cal");
  }

  if (inputstring.equals("Factory")) {
    Serial2.println("Factory");
    Serial3.println("Factory");
  }
  
 if (inputstring.equals("CAL,0")) {
    Serial2.println("CAL,0");
    Serial3.println("CAL,0");
  }

  
}


void serialEvent2() {                                 //if the hardware serial port_2 receives a char
  insensorstring = Serial2.readStringUntil(13);         //read the string until we see a <CR>
  insensor_string_complete = true;                      //set the flag used to tell if we have received a completed string from the PC
}

void serialEvent3() {                                 //if the hardware serial port_3 receives a char
  outsensorstring = Serial3.readStringUntil(13);         //read the string until we see a <CR>
  outsensor_string_complete = true;                      //set the flag used to tell if we have received a completed string from the PC
}


void loop() {                                         //here we go...


  if (input_string_complete == true) {                //if a string from the PC has been received in its entirety
    Serial3.print(inputstring);                       //send that string to the Atlas Scientific product
    Serial3.print('\r');                              //add a <CR> to the end of the string
    Serial2.print(inputstring);                       //send that string to the Atlas Scientific product
    Serial2.print('\r');                              //add a <CR> to the end of the string
    inputstring = "";                                 //clear the string
    input_string_complete = false;                    //reset the flag used to tell if we have received a completed string from the PC
  }


  if (insensor_string_complete == true) {               //if a string from the Atlas Scientific product has been received in its entirety
    Serial.println(insensorstring);                     //send that string to the PC's serial monitor
    if (isdigit(insensorstring[0])) {                   //if the first character in the string is a digit
      inDO = outsensorstring.toFloat();                    //convert the string to a floating point number so it can be evaluated by the Arduino
      if (inDO >= 6.0) {                                //if the DO is greater than or equal to 6.0
        Serial.println("In DO high");                       //print "high" this is demonstrating that the Arduino is evaluating the DO as a number and not as a string
      }
      if (inDO <= 5.99) {                               //if the DO is less than or equal to 5.99
        Serial.println("In DO low");                        //print "low" this is demonstrating that the Arduino is evaluating the DO as a number and not as a string
      }
    }
  }
  insensorstring = "";                                  //clear the string:
  insensor_string_complete = false;                     //reset the flag used to tell if we have received a completed string from the Atlas Scientific product


  if (outsensor_string_complete == true) {               //if a string from the Atlas Scientific product has been received in its entirety
    Serial.println(outsensorstring);                     //send that string to the PC's serial monitor
    if (isdigit(outsensorstring[0])) {                   //if the first character in the string is a digit
      outDO = outsensorstring.toFloat();                    //convert the string to a floating point number so it can be evaluated by the Arduino
      if (outDO >= 6.0) {                                //if the DO is greater than or equal to 6.0
        Serial.println("Out DO high");                       //print "high" this is demonstrating that the Arduino is evaluating the DO as a number and not as a string
      }
      if (outDO <= 5.99) {                               //if the DO is less than or equal to 5.99
        Serial.println("Out DO low");                        //print "low" this is demonstrating that the Arduino is evaluating the DO as a number and not as a string
      }
    }
  }
  outsensorstring = "";                                  //clear the string:
  outsensor_string_complete = false;                     //reset the flag used to tell if we have received a completed string from the Atlas Scientific product


}



