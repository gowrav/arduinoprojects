#ifndef HEADER_GASMONITOR
#define HEADER_GASMONITOR

//The MQ-2 sensor has 4 pins.
//
//Pin-------------------------------------Wiring to Arduino Uno
//A0-------------------------------------Analog pins
//D0-------------------------------------Digital pins
//GND-----------------------------------GND
//VCC------------------------------------5V

class GasMon {
  public:
    GasMon();
    ~GasMon();
    boolean initGasMonAnalog(int sensorAPin);
    boolean initGasMonDigital(int sensorDPin);
    boolean setThreshold(double threshold);
    boolean readState();
    double readValue();
};

#endif
