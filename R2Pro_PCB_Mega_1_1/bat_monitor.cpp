#include "Arduino.h"
#include "bat_monitor.h"
#include "TimeLib.h"
#include "TimeAlarms.h"

byte _batsensorPin;  // Pin used as trigPin
double _smallR;  // Pin used as trigPin
double _bigR;  // Pin used as trigPin
long _vref_scale_constant;// typically 1125300L
boolean _BatMoninitialized;
byte _samplecount;
float _cal_voltage;

//<<constructor>> setup the BatMon
BatMon::BatMon() {
  // Construction Parameter here
  _BatMoninitialized = false;
  _vref_scale_constant = 1125300L;
}


//<<destructor>>
BatMon::~BatMon() {
  /*nothing to destruct*/
}

//<<constructor>> setup the BatMon
boolean BatMon::initBatMon(int sensorPin, double smallR , double bigR) {
  // Construction Parameter here
  _batsensorPin = sensorPin;
  _smallR = smallR;
  _bigR = bigR;
  _samplecount = BAT_NUM_SAMPLES;
  _cal_voltage=CALIBRATE_VOLTAGE;
  //digitalWrite(_batsensorPin, INPUT_PULLUP);  // set pullup and _sensorPin as Analog Input
  _BatMoninitialized = true;
  return _BatMoninitialized;
}

//<<constructor>> setup the BatMon
void BatMon::setVrefScaleConstant(long vref_scale_constant) {
  _vref_scale_constant = vref_scale_constant;
}

//<<constructor>> setup the BatMon
void BatMon::setSampleCount(byte count) {
  _samplecount = count;
}
//<<constructor>> setup the BatMon caliber voltage to measure accurate voltage value
void BatMon::setCalibrateVoltage(float ref) {
  _cal_voltage = ref;
}
 

//<<constructor>> setup the BatMon
double BatMon::readVcc() {
  // Read VCC value
  // Read 1.1V reference against AVcc
  // set the reference to Vcc and the measurement to the internal 1.1V reference
#if defined(__AVR_ATmega32U4__) || defined(__AVR_ATmega1280__) || defined(__AVR_ATmega2560__)
  ADMUX = _BV(REFS0) | _BV(MUX4) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1);
#elif defined (__AVR_ATtiny24__) || defined(__AVR_ATtiny44__) || defined(__AVR_ATtiny84__)
  ADMUX = _BV(MUX5) | _BV(MUX0);
#elif defined (__AVR_ATtiny25__) || defined(__AVR_ATtiny45__) || defined(__AVR_ATtiny85__)
  ADMUX = _BV(MUX3) | _BV(MUX2);
#else
  ADMUX = _BV(REFS0) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1);
#endif

  Alarm.delay(2); // Wait for Vref to settle
  ADCSRA |= _BV(ADSC); // Start conversion
  while (bit_is_set(ADCSRA, ADSC)); // measuring

  uint8_t low  = ADCL; // must read ADCL first - it then locks ADCH
  uint8_t high = ADCH; // unlocks both

  long result = (high << 8) | low;

  // Replace 1125300L with a new constant:
  //vref_scale_constant = internal1.1Ref * 1023 * 1000 where internal1.1Ref = 1.1 * Vcc1 (per voltmeter) / Vcc2 (per readVcc() function)

  return ( (float) _vref_scale_constant / (float)result) /1000; // Calculate Vcc (in mV); 1125300 = 1.1*1023*1000; // Vcc in millivolts
}



// Measure the voltage of any analog pin by providing the reference voltages and voltage deviders
double BatMon::getVolt() {
  if (_BatMoninitialized)
  {
    int sum = 0;                    // sum of samples taken
    unsigned char sample_count = 0; // current sample number
 while (sample_count < _samplecount) {
        sum += analogRead(_batsensorPin);
        sample_count++;
        Alarm.delay(BAT_SAMPLINGDELAY);
    }
    sum=sum/_samplecount;
    return (((float)sum / 1024) * readVcc() *(float)(_smallR + _bigR)/(float)_smallR);
  }
}

double BatMon::getVoltDirect() {
  float voltage = 0.0; 
  if (_BatMoninitialized)
  {
    int sum = 0;                    // sum of samples taken
    unsigned char sample_count = 0; // current sample number
 while (sample_count < _samplecount) {
        sum += analogRead(_batsensorPin);
        sample_count++;
        Alarm.delay(BAT_SAMPLINGDELAY);
    }
    // calculate the voltage
    // use 5.0 for a 5.0V ADC reference voltage
    // 5.015V is the calibrated reference voltage
    //voltage = ((float)sum / (float)NUM_SAMPLES * 5.015) / 1024.0;
    voltage = ( (float)sum / (float)_samplecount * readVcc()) / 1024.0;
    // send voltage for display on Serial Monitor
    // voltage multiplied by 11 when using voltage divider that
    // divides by 11. 11.132 is the calibrated voltage divide
    // value
    //Serial.print(F(voltage * _cal_voltage));
    //Serial.print(voltage * 11.09);
    //Serial.println (F(" V"));
    return voltage * _cal_voltage;
  }
}
