#include "Arduino.h"
#include "debugMonitor.h"
#include "gprs_comm.h"
#include "SoftwareSerial.h"
#include "TinyGPS++.h" // Include the TinyGPS++ library
#include "TimeLib.h"
#include "TimeAlarms.h"

TinyGPSPlus _gpstinyplus;
boolean response;
//SoftwareSerial gprsSerial(_gprsRXPIN, _gprsTXPIN); // RX, TX
#define gprsSerial Serial1

void initGPRS()
{
  gprsSerial.begin(_gprsBAUD);
  sendGPRSCommand(F("AT"), 100);
}

void onGPS()
{
  sendGPRSCommand(F("AT+CGPSPWR=1"), 300); // we can turn this off now and then to save power
}

void offGPS()
{
  sendGPRSCommand(F("AT+CGPSPWR=0"), 300);
}

boolean postUpdate(String body)
{

  sendGPRSCommand(F("AT+SAPBR=3,1,\"Contype\",\"GPRS\""), 1000); // setting internet option
  sendGPRSCommand(F("AT+SAPBR=3,1,\"APN\",\"airtelgprs.com\""), 2000);
  sendGPRSCommand(F("AT+SAPBR=1,1"), 2000);
  sendGPRSCommand(F("AT+HTTPINIT"), 1000);

  // sendGPRSCommand("AT+HTTPSSL=0"),500);//   -> wait for OK (1 when URL starts with "https://")

  sendGPRSCommand(F("AT+HTTPPARA=\"CID\",1"), 500);

  //sendGPRSCommand(F("AT+HTTPPARA=\"URL\",\"https://54.236.198.240:443/v2/dweets\""),500);
  //sendGPRSCommand(F("AT+HTTPPARA=\"URL\","),50);

  //sendGPRSCommand(F("\"https://dweetpro.io/v2/dweets:443\""),500);
  //sendGPRSCommand(F("\"http://dweetpro.io/v2/dweets\""),500);
  //sendGPRSCommand(F("AT+HTTPPARA=\"URL\",\"https://wyke.in\""),500);
  //sendGPRSCommand(F("AT+HTTPPARA=\"URL\",\"http://34.203.32.119/v2/dweets\""),500);

  sendGPRSCommand(F("AT+HTTPPARA=\"URL\",\"http://52.37.157.105:8008/\""), 500);
  sendGPRSCommandCont(F("AT+HTTPPARA=\"USERDATA\",\"X-DWEET-AUTH:"), 50);
  sendGPRSCommandCont(F("eyJyb2xlIjoiYXV0byIsImNvbXBhbnki"), 50);
  sendGPRSCommandCont(F("OiJHRUVLU1lORVJHWSIsImdlbmVyYXRl"), 50);
  sendGPRSCommandCont(F("ZCI6MTUwMDgwNzU2NjYyMn0"), 50);
  sendGPRSCommandCont(F("\=.39dadd2c2d5a27b71ab8605922800"), 50);
  sendGPRSCommandCont(F("948916eddd1f60af214ca4e7eaedaa8"), 50);
  //sendGPRSCommandCont(F("AwNTQ5OTI4MTU3LCJhY2NvdW50IjpbeyJu"), 50);
  //sendGPRSCommandCont(F("YW1lIjoiR0VFS1NZTkVSR1kiLCJyb2xlIj"), 50);
  //sendGPRSCommandCont(F("oiYWRtaW4ifV0sImNvbXBhbnkiOiJHRUV"), 50);
  //sendGPRSCommandCont(F("LU1lORVJHWSIsInZhbGlkaXR5I"), 50);
   sendGPRSCommand(F("d1ca\""), 500);
  
  
  sendGPRSCommand(F("AT+HTTPPARA=\"CONTENT\",\"application/json\""), 500);

  //ATCommand(F("AT+HTTPPARA=\"USERDATA\",\"NameKey1: " + String(ValueKey1) + "\\r\\nNameKey2: " + String(ValueKey2) +"\""));
  //ATCommand(F("AT+HTTPPARA=\"CONTENT\",\"application/json\""));

  //ATCommand(F("AT+HTTPPARA?"));  // To see all the parameters

  //mySerial.println(F("AT+HTTPPARA?"));delay(200);
  //ShowSerialData();

  // String abody = "{\"location_id\": 238}";

  sendGPRSCommandCont(F("AT+HTTPDATA="), 500);
  sendGPRSCommandCont(String(body.length(), DEC), 500);
  sendGPRSCommand(F(",10000"), 500);
  sendGPRSCommand(body, 500);

  sendGPRSCommand(F("AT+HTTPSSL=0"), 500); //   -> wait for OK (1 when URL starts with "https://")

  sendGPRSCommand(F("AT+HTTPACTION=1"), 1000);
  sendGPRSCommand(F("AT+HTTPREAD"), 1000);
  sendGPRSCommand(F(" AT+HTTPTERM"), 500);
  sendGPRSCommand(F("AT+SAPBR=1,1"), 2000);
  sendGPRSCommand(F(""), 300);
  sendGPRSCommand(F("AT"), 300);
  //updating=false;
   //while (gprsSerial.available() != 0)
    //dbgSerial.write(gprsSerial.read());
    return response;
}

TinyGPSPlus getGPSVal()
{
  if (_gpstinyplus.location.isUpdated())
    return _gpstinyplus;

  ///onGPS();
  //sendGPRSCommand(F("AT+CGPSSTATUS?"), 500);
  //sendGPRSCommand(F("AT+CGPSOUT=32"), 300);

  unsigned long ms = 1000;
  unsigned long start = millis();
  do
  { // If data has come in from the GPS module
    while (gprsSerial.available())
      _gpstinyplus.encode(gprsSerial.read()); //dbgSerial.write(gpsSerial.read());
  } while (millis() - start < ms);

  //sendGPRSCommand(F("AT+CGPSOUT=0"), 300);
  //offGPS();

  return _gpstinyplus;
}

String getGPSSource()
{
  if (_gpstinyplus.location.isUpdated())
    return _gpsFIX_3D;
  else
    return _gpsFIX_DEFAULT;
}

void sendGPRSCommandCont(String command, long delaymilliseconds)
{
  gprsSerial.print(command);
  Alarm.delay(delaymilliseconds);
  ShowGPRSSerialData();
}

void sendGPRSCommand(String command, long delaymilliseconds)
{
  gprsSerial.println(command);
  Alarm.delay(delaymilliseconds);
  ShowGPRSSerialData();
}

void ShowGPRSSerialData()
{
  while (gprsSerial.available() != 0){
    String resData=gprsSerial.readString();
    if(resData.indexOf("+HTTPACTION:") > 0){
      dbgSerial.println("response is showing");
      String data=resData.substring(resData.indexOf(",")+1,resData.indexOf(",")+4);
      if(data=="200"){  // upload is success. 
        response=true;
      }else{
        response=false;
      }
       dbgSerial.println(data);
    }
    dbgSerial.println(resData);
    //dbgSerial.write(gprsSerial.read());
  }
}
