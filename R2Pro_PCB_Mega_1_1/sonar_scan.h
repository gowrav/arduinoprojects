#ifndef HEADER_SONARSCAN
#define HEADER_SONARSCAN


class Sonar {
  public:
    Sonar();
    ~Sonar();
    boolean initSonar(int triggerPin, int echoPin);
    int getSonarDistance();
    void printSonarDistance();
};

#endif
