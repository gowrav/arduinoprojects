#include "Arduino.h"
#include "sonar_scan.h"
#include "debugMonitor.h"
// #include "TimeLib.h"
// #include "TimeAlarms.h"

byte _triggerPin;  // Pin used as trigPin
byte _echoPin; // Pin used as echoPin
boolean _initialized;
int avg_samples = 3;

//<<constructor>> setup the Sonar
Sonar::Sonar() {
  // Construction Parameter here
  _initialized = false;
}


//<<destructor>>
Sonar::~Sonar() {
  /*nothing to destruct*/
}

//<<constructor>> setup the Sonar
boolean Sonar::initSonar(int triggerPin, int echoPin) {
  // Construction Parameter here
  _triggerPin = triggerPin;
  _echoPin = echoPin;
  pinMode(_triggerPin, OUTPUT); // Sets the trigPin as an Output
  pinMode(_echoPin, INPUT); // Sets the echoPin as an Input
  _initialized = true;
  return _initialized;
}

//<<get Sonar Distance>>
int Sonar::getSonarDistance() {
  if (_initialized) {
    int i = avg_samples;
    long tot_distance = 0;
    while(i)
    {
      digitalWrite(_triggerPin, LOW);
     // Alarm.microdelay(2);
      digitalWrite(_triggerPin, HIGH);
     // Alarm.microdelay(10);
      digitalWrite(_triggerPin, LOW);
      long duration = pulseIn(_echoPin, HIGH);
      long distance = duration * 0.034 / 2;
      if(distance < 500 && distance > -1)
        {
          tot_distance += distance;
          i--;
        }else{
          Serial.print(F("Error "));
          Serial.println(distance);
        }
    }
    return tot_distance / avg_samples;
  }
}

//<<print Sonar Distance>>
void Sonar::printSonarDistance() {
  if (_initialized) {
  int i = avg_samples;
    long tot_distance = 0;
    while(i)
    {
      digitalWrite(_triggerPin, LOW);
      //Alarm.microdelay(2);
      delayMicroseconds(2);
      digitalWrite(_triggerPin, HIGH);
      //Alarm.microdelay(10);
      delayMicroseconds(10);
      
      digitalWrite(_triggerPin, LOW);
      long duration = pulseIn(_echoPin, HIGH);
      long distance = duration * 0.034 / 2;
      Serial.print("duration");
      Serial.println(duration);
      Serial.print("distance");
      Serial.println(distance);
      if(distance < 500 && distance > -1)
        {
          tot_distance += distance;
          i--;
        }
    }
    dbgSerial.println(tot_distance / avg_samples);
  }
}

