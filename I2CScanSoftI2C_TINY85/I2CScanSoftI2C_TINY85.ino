// -*-c++-*-
// Scan I2C bus for device responses


#define I2C_TIMEOUT 0
#define I2C_NOINTERRUPT 0
#define I2C_FASTMODE 0
#define FAC 1
#define I2C_CPUFREQ (F_CPU/FAC)

/* Corresponds to 2/0 - the hardware I2C pins on Arduinos */
/* Adjust to your own liking */
#define SDA_PORT PORTB
#define SDA_PIN 2
#define SCL_PORT PORTB
#define SCL_PIN 0
#define I2C_FASTMODE 0


#include <SoftI2CMaster.h>
#include <avr/io.h>
#include <SoftwareSerial.h>

SoftwareSerial tinySerial(3, 4);

//------------------------------------------------------------------------------
void CPUSlowDown(int fac) {
  // slow down processor by a fac
    CLKPR = _BV(CLKPCE);
    CLKPR = _BV(CLKPS1) | _BV(CLKPS0);
}
  


void setup(void) {
#if FAC != 1
  CPUSlowDown(FAC);
#endif

  tinySerial.begin(9600); // change baudrate to 2400 on terminal when low CPU freq!
  tinySerial.println(F("Intializing ..."));
  tinySerial.print("I2C delay counter: ");
  tinySerial.println(I2C_DELAY_COUNTER);
  if (!i2c_init()) 
    tinySerial.println(F("Initialization error. SDA or SCL are low"));
  else
    tinySerial.println(F("...done"));
}

void loop(void)
{
  uint8_t add = 0;
  int found = false;
  tinySerial.println("Scanning ...");
  tinySerial.println("       8-bit 7-bit addr");
  
  // try read
  do {
    delay(100);
    if (i2c_start(add | I2C_READ)) {
      found = true;
      i2c_read(true);
      i2c_stop();
      tinySerial.print("Read:   0x");
      if (add < 0x0F) tinySerial.print(0, HEX);
      tinySerial.print(add+I2C_READ, HEX);
      tinySerial.print("  0x");
      if (add>>1 < 0x0F) tinySerial.print(0, HEX);
      tinySerial.println(add>>1, HEX);
    } else i2c_stop();
    add += 2;
  } while (add);

  // try write
  add = 0;
  do {
    if (i2c_start(add | I2C_WRITE)) {
      found = true;
      i2c_stop();
      tinySerial.print("Write:  0x");    
      if (add < 0x0F) tinySerial.print(0, HEX);  
      tinySerial.print(add+I2C_WRITE, HEX);
      tinySerial.print("  0x");
      if (add>>1 < 0x0F) tinySerial.print(0, HEX);
      tinySerial.println(add>>1, HEX);
    } else i2c_stop();
    i2c_stop();
    add += 2;
  } while (add);
  
  if (!found) tinySerial.println(F("No I2C device found."));
  tinySerial.println("Done\n\n");
  delay(1000/FAC);
}
