// P10 Pixel Driver
// In this panel, first, A , B , LAT , CLK , DAT , and OE pass through the 74HC245 (8-channel transceiver) of the general-purpose logic IC.
// The A and B lines of the connector (after passing 74HC245) are connected to the input of 74HC138 (3 to 8 decoder ) of the general purpose logic IC
// and are used to drive the source driver FET at the output side.
// In addition, the DAT line, CLK and LAT are connected to the 74HC595 (shift register ) and used as a sink driver.
// The input data is in negative logic with DAT ( written as /DAT specification ).
// When the 74HC595 terminal goes to 0 V, suction occurs due to the potential difference with the source driver side, and the LED lights up.

// chunks for 4 rows can be addressed and updated at any time but only one line of this chunk is stored
// the chunks should be of 8columnns and 4 rows.

boolean chunk_test[4][8] = {
  {1, 0, 0, 1, 1, 0, 0, 1},
  {0, 1, 1, 0, 0, 1, 1, 0},
  {0, 1, 1, 0, 0, 1, 1, 0},
  {1, 0, 0, 1, 1, 0, 0, 1}
};

boolean chunk_zero[4][8] = {
  {0, 0, 0, 0, 0, 0, 0, 0},
  {0, 0, 0, 0, 0, 0, 0, 0},
  {0, 0, 0, 0, 0, 0, 0, 0},
  {0, 0, 0, 0, 0, 0, 0, 0}
};


// Total pins A B CLK STORE DATA Enable.
// 8bits * 4 Combinations = 32LEDS * 16 IC

//Pin Setup
int OE = 8;
int FET_A = 7;
int FET_B = 6;
int CLK = 5;
int STORE = 4;
int DATA = 3;

int rows = 16; // to be multiple s of 4
int columns = 32;// to be multiples os 8

void setup() {

  init_Display_Pins();
  Reset_Display();
}

void write_eightpixel(int pixel_location_8bit) {
  digitalWrite(STORE, LOW);
  for (int i = 7; i >= 0; i--)
  {
    digitalWrite(CLK, LOW);
    if (bitRead(pixel_location_8bit, i) == 1)
      digitalWrite(DATA, LOW);
    else
      digitalWrite(DATA, HIGH);
    digitalWrite(CLK, HIGH);
  }
  digitalWrite(STORE, HIGH);
}

void write_onepixel(int pixel_location_8pos) {

  digitalWrite(STORE, LOW);
  for (int i = 7; i >= 0; i--)
  {
    digitalWrite(CLK, LOW);
    if (i == pixel_location_8pos)
      digitalWrite(DATA, LOW);
    else
      digitalWrite(DATA, HIGH);
    digitalWrite(CLK, HIGH);
  }
  digitalWrite(STORE, HIGH);
}



void Reset_Display() {
  // Update first row of every chunk and move on to the next row
  int k = 4;
  while (k > 0)
  { // Off the display
    Disable_Display();
    switch (--k) {
      case 0: digitalWrite(FET_A, LOW); digitalWrite(FET_B, LOW); break;
      case 1: digitalWrite(FET_A, HIGH); digitalWrite(FET_B, LOW); break;
      case 2: digitalWrite(FET_A, LOW); digitalWrite(FET_B, HIGH); break;
      case 3: digitalWrite(FET_A, HIGH); digitalWrite(FET_B, HIGH); break;
    }
    // On the display
    for (int j = (rows / 4) * (columns / 8); j > 0; j--)
    {
      // Repeat chunks of chunk_zero

      digitalWrite(STORE, LOW);
      for (int i = 7; i >= 0; i--)
      {
        digitalWrite(CLK, LOW);

        if (chunk_zero[k][i])
          digitalWrite(DATA, LOW);
        else
          digitalWrite(DATA, HIGH);

        digitalWrite(CLK, HIGH);
      }
      digitalWrite(STORE, HIGH);

    }
    Enable_Display();
    // On the display
  }
}


void Display_Test() {
  // Update first row of every chunk and move on to the next row
  int k = 4;
  while (k > 0)
  { // Off the display
    Disable_Display();
    switch (--k) {
      case 0: digitalWrite(FET_A, LOW); digitalWrite(FET_B, LOW); break;
      case 1: digitalWrite(FET_A, HIGH); digitalWrite(FET_B, LOW); break;
      case 2: digitalWrite(FET_A, LOW); digitalWrite(FET_B, HIGH); break;
      case 3: digitalWrite(FET_A, HIGH); digitalWrite(FET_B, HIGH); break;
    }
    for (int j = (rows / 4) * (columns / 8); j > 0; j--)
    {
      // Repeat chunks of chunk_zero
      digitalWrite(STORE, LOW);
      for (int i = 7; i >= 0; i--)
      {
        digitalWrite(CLK, LOW);

        if (chunk_test[k][i])
          digitalWrite(DATA, LOW);
        else
          digitalWrite(DATA, HIGH);

        digitalWrite(CLK, HIGH);
      }
      digitalWrite(STORE, HIGH);

    }
    Enable_Display();
    // On the display
  }
}


void init_Display_Pins() {
  pinMode(OE, OUTPUT);
  pinMode(FET_A, OUTPUT);
  pinMode(FET_B, OUTPUT);
  pinMode(CLK, OUTPUT);
  pinMode(STORE, OUTPUT);
  pinMode(DATA, OUTPUT);

  digitalWrite(OE, LOW);
  digitalWrite(FET_A, LOW);
  digitalWrite(FET_B, LOW);
  digitalWrite(CLK, LOW);
  digitalWrite(STORE, LOW);
  digitalWrite(DATA, LOW);
}

void Disable_Display() {
  digitalWrite(OE, LOW);
}
void Enable_Display() {
  digitalWrite(OE, HIGH);
}

void loop() {
  Display_Test();
}
