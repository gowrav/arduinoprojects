#ifndef GAP_AES
#define GAP_AES

void GAP_aes_encrypt(unsigned char stat_Dynamic, unsigned char Block_Segmentation, unsigned char *state, unsigned char *key);

void GAP_aes_decrypt(unsigned char stat_Dynamic, unsigned char Block_Segmentation, unsigned char *cipher_state, unsigned char *key);

#endif
