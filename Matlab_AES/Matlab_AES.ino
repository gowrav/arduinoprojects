#include "GAP_AES.h"
#include "GAP_AES.c"


unsigned char  key[]   = {0x41, 0x20, 0x56, 0x47, 0x4f, 0x4f, 0x44, 0x20,
                           0x50, 0x41, 0x53, 0x53, 0x57, 0x4f, 0x52, 0x44};
                           
                           
unsigned char inputByte[16]; // Encrypts only 16 Bytes i.e 1 Block of Data

boolean st_dy = 0;  // 1 for Dynamic and  0 for Static
boolean Re_segmentation = 1; // For Verilog Based FPGA implementation to Arduino

char ENC_DEC;
int i;

void setup() {
  delay(500);
  Serial.begin(9600); 
  

}

void loop() {
  // put your main code here, to run repeatedly:
   while(Serial.available() <= 0);
    ENC_DEC = (char)Serial.read();
      for(i=0;i<16;i++)
         {
            while(Serial.available() <= 0);
            delay(5);
            inputByte[i] = Serial.read();
//            Serial.print(char(inputByte[i]));   
            delay(5);
         }
         
         
         
    if(ENC_DEC == 'E')  
         {
           GAP_aes_encrypt(st_dy,Re_segmentation,inputByte,key);  // Provide Static/Dynamic ResegmentationRequirement StateData(Overwritten after Encryption) and CipherKey
           for(i=0;i<16;i++)
             Serial.print(char(inputByte[i]));   
         }   
         
    if(ENC_DEC == 'D')  
         {
           GAP_aes_decrypt(st_dy,Re_segmentation,inputByte,key);  // Provide Static/Dynamic ResegmentationRequirement StateData(Overwritten after Encryption) and CipherKey  
           for(i=0;i<16;i++)
             Serial.print(char(inputByte[i]));  
         } 

}
