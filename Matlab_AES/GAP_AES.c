// Code written by Gowrav under GPL, Hence code is free to be modified and redistribute//
// This is a C code for Pseudo Dynamic AES Encryption //
// In this We implement 128-bit AES Encryption in both Std and Pseudo Dynamic Method //
// For reference on both contact gowrav.hassan@gmail.com //
// This code is a C adoptation of the Original Verilog code available here .. gowrav.hassan@gmail.com //
// The code has few similaritites with Texas Instruments AES Library for MSP430 //




// First lets create a standard 128-bit SBOX for future use //
// We have kept this const so as to avoid any unwanted modifications to these values

const unsigned char sta_sbox[256] =   { 
//0     1    2      3     4    5     6     7      8    9     A      B    C     D     E     F
0x63, 0x7c, 0x77, 0x7b, 0xf2, 0x6b, 0x6f, 0xc5, 0x30, 0x01, 0x67, 0x2b, 0xfe, 0xd7, 0xab, 0x76, //0
0xca, 0x82, 0xc9, 0x7d, 0xfa, 0x59, 0x47, 0xf0, 0xad, 0xd4, 0xa2, 0xaf, 0x9c, 0xa4, 0x72, 0xc0, //1
0xb7, 0xfd, 0x93, 0x26, 0x36, 0x3f, 0xf7, 0xcc, 0x34, 0xa5, 0xe5, 0xf1, 0x71, 0xd8, 0x31, 0x15, //2
0x04, 0xc7, 0x23, 0xc3, 0x18, 0x96, 0x05, 0x9a, 0x07, 0x12, 0x80, 0xe2, 0xeb, 0x27, 0xb2, 0x75, //3
0x09, 0x83, 0x2c, 0x1a, 0x1b, 0x6e, 0x5a, 0xa0, 0x52, 0x3b, 0xd6, 0xb3, 0x29, 0xe3, 0x2f, 0x84, //4
0x53, 0xd1, 0x00, 0xed, 0x20, 0xfc, 0xb1, 0x5b, 0x6a, 0xcb, 0xbe, 0x39, 0x4a, 0x4c, 0x58, 0xcf, //5
0xd0, 0xef, 0xaa, 0xfb, 0x43, 0x4d, 0x33, 0x85, 0x45, 0xf9, 0x02, 0x7f, 0x50, 0x3c, 0x9f, 0xa8, //6
0x51, 0xa3, 0x40, 0x8f, 0x92, 0x9d, 0x38, 0xf5, 0xbc, 0xb6, 0xda, 0x21, 0x10, 0xff, 0xf3, 0xd2, //7
0xcd, 0x0c, 0x13, 0xec, 0x5f, 0x97, 0x44, 0x17, 0xc4, 0xa7, 0x7e, 0x3d, 0x64, 0x5d, 0x19, 0x73, //8
0x60, 0x81, 0x4f, 0xdc, 0x22, 0x2a, 0x90, 0x88, 0x46, 0xee, 0xb8, 0x14, 0xde, 0x5e, 0x0b, 0xdb, //9
0xe0, 0x32, 0x3a, 0x0a, 0x49, 0x06, 0x24, 0x5c, 0xc2, 0xd3, 0xac, 0x62, 0x91, 0x95, 0xe4, 0x79, //A
0xe7, 0xc8, 0x37, 0x6d, 0x8d, 0xd5, 0x4e, 0xa9, 0x6c, 0x56, 0xf4, 0xea, 0x65, 0x7a, 0xae, 0x08, //B
0xba, 0x78, 0x25, 0x2e, 0x1c, 0xa6, 0xb4, 0xc6, 0xe8, 0xdd, 0x74, 0x1f, 0x4b, 0xbd, 0x8b, 0x8a, //C
0x70, 0x3e, 0xb5, 0x66, 0x48, 0x03, 0xf6, 0x0e, 0x61, 0x35, 0x57, 0xb9, 0x86, 0xc1, 0x1d, 0x9e, //D
0xe1, 0xf8, 0x98, 0x11, 0x69, 0xd9, 0x8e, 0x94, 0x9b, 0x1e, 0x87, 0xe9, 0xce, 0x55, 0x28, 0xdf, //E
0x8c, 0xa1, 0x89, 0x0d, 0xbf, 0xe6, 0x42, 0x68, 0x41, 0x99, 0x2d, 0x0f, 0xb0, 0x54, 0xbb, 0x16 }; //F   

// Now to get back the Original Data lets create an Inverse SBOX //
// We have kept this const so as to avoid any unwanted modifications to these values

const unsigned char sta_rsbox[256] =
{ 0x52, 0x09, 0x6a, 0xd5, 0x30, 0x36, 0xa5, 0x38, 0xbf, 0x40, 0xa3, 0x9e, 0x81, 0xf3, 0xd7, 0xfb
, 0x7c, 0xe3, 0x39, 0x82, 0x9b, 0x2f, 0xff, 0x87, 0x34, 0x8e, 0x43, 0x44, 0xc4, 0xde, 0xe9, 0xcb
, 0x54, 0x7b, 0x94, 0x32, 0xa6, 0xc2, 0x23, 0x3d, 0xee, 0x4c, 0x95, 0x0b, 0x42, 0xfa, 0xc3, 0x4e
, 0x08, 0x2e, 0xa1, 0x66, 0x28, 0xd9, 0x24, 0xb2, 0x76, 0x5b, 0xa2, 0x49, 0x6d, 0x8b, 0xd1, 0x25
, 0x72, 0xf8, 0xf6, 0x64, 0x86, 0x68, 0x98, 0x16, 0xd4, 0xa4, 0x5c, 0xcc, 0x5d, 0x65, 0xb6, 0x92
, 0x6c, 0x70, 0x48, 0x50, 0xfd, 0xed, 0xb9, 0xda, 0x5e, 0x15, 0x46, 0x57, 0xa7, 0x8d, 0x9d, 0x84
, 0x90, 0xd8, 0xab, 0x00, 0x8c, 0xbc, 0xd3, 0x0a, 0xf7, 0xe4, 0x58, 0x05, 0xb8, 0xb3, 0x45, 0x06
, 0xd0, 0x2c, 0x1e, 0x8f, 0xca, 0x3f, 0x0f, 0x02, 0xc1, 0xaf, 0xbd, 0x03, 0x01, 0x13, 0x8a, 0x6b
, 0x3a, 0x91, 0x11, 0x41, 0x4f, 0x67, 0xdc, 0xea, 0x97, 0xf2, 0xcf, 0xce, 0xf0, 0xb4, 0xe6, 0x73
, 0x96, 0xac, 0x74, 0x22, 0xe7, 0xad, 0x35, 0x85, 0xe2, 0xf9, 0x37, 0xe8, 0x1c, 0x75, 0xdf, 0x6e
, 0x47, 0xf1, 0x1a, 0x71, 0x1d, 0x29, 0xc5, 0x89, 0x6f, 0xb7, 0x62, 0x0e, 0xaa, 0x18, 0xbe, 0x1b
, 0xfc, 0x56, 0x3e, 0x4b, 0xc6, 0xd2, 0x79, 0x20, 0x9a, 0xdb, 0xc0, 0xfe, 0x78, 0xcd, 0x5a, 0xf4
, 0x1f, 0xdd, 0xa8, 0x33, 0x88, 0x07, 0xc7, 0x31, 0xb1, 0x12, 0x10, 0x59, 0x27, 0x80, 0xec, 0x5f
, 0x60, 0x51, 0x7f, 0xa9, 0x19, 0xb5, 0x4a, 0x0d, 0x2d, 0xe5, 0x7a, 0x9f, 0x93, 0xc9, 0x9c, 0xef
, 0xa0, 0xe0, 0x3b, 0x4d, 0xae, 0x2a, 0xf5, 0xb0, 0xc8, 0xeb, 0xbb, 0x3c, 0x83, 0x53, 0x99, 0x61
, 0x17, 0x2b, 0x04, 0x7e, 0xba, 0x77, 0xd6, 0x26, 0xe1, 0x69, 0x14, 0x63, 0x55, 0x21, 0x0c, 0x7d };


// Now lets create a Variable Space of 256 bytes in order to store the new and improved key based
// Dynamcic SBOX which will be generated on the fly using the above Static SBOX.
// unsigned char dyn_sbox[256]; // would have done the trick but it looks better this way
// Also see that this array is not declared as constant since it will be changed at RunTime
//unsigned char dyn_sbox[256] =   { 
////0     1    2      3     4    5     6     7      8    9     A      B    C     D     E     F
//0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, //0
//0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, //1
//0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, //2
//0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, //3
//0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, //4
//0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, //5
//0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, //6
//0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, //7
//0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, //8
//0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, //9
//0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, //A
//0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, //B
//0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, //C
//0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, //D
//0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, //E
//0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }; //F   
//
//
//
//
//// The following is the 256 bytes of data storage area for the Key based 
//// Dynamcic Inverse SBOX which will be generated on the fly using the above Static Inverse SBOX.
//// unsigned char dyn_sta_rsbox[256]; // would have done the trick but it looks better this way
//// Also see that this array is not declared as constant since it will be changed at RunTime
//unsigned char dyn_sta_rsbox[256] =   { 
////0     1    2      3     4    5     6     7      8    9     A      B    C     D     E     F
//0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, //0
//0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, //1
//0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, //2
//0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, //3
//0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, //4
//0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, //5
//0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, //6
//0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, //7
//0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, //8
//0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, //9
//0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, //A
//0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, //B
//0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, //C
//0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, //D
//0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, //E
//0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }; //F   



// Now lets create a RCON (Round Constant) box which is a standard 10 bytes of constants as shown
const unsigned char Rcon[10] = {
            0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80, 0x1b, 0x36};  // Texas RCON has 11 bytes zeroth byte being the 0x8d 
  

 // multiply by 2 in the galois field
 unsigned char galois2_mul(unsigned char value)
	{
		if (value>>7)
		{
			value = value << 1;
			return (value^0x1b);
		} else
			return value<<1;
	} 


	
 // KeyCopier - This fuction does the copy of the origianl key	
 void key_copier(unsigned char *originalkey, unsigned char *newcopy)
	{
	    char i;
	    for(i=0;i<16;i++)
		{
		  newcopy[i]=originalkey[i];
		}
	}
	

 // Offset Calculator - For the Round Key	
  unsigned char  Key_Offset(unsigned char *Key_in)
	{   
            unsigned char key_box[16];
            unsigned char key_sorted[16];
            
            char i;
            for(i=0; i<16 ; i++)
                {
                   key_box[i] = Key_in[i];
                }
                
            for(i=0; i<4 ; i++)
                  {
                     key_sorted[i]    = key_box[0+i*4];
                     key_sorted[i+4]  = key_box[1+i*4];
                     key_sorted[i+8]  = key_box[2+i*4];
                     key_sorted[i+12] = key_box[3+i*4];
                  } 
  
  	  unsigned char temp_key1 = 0;
	  unsigned char temp_key2 = 0;
          unsigned char Selectbit; // This is to XOR and select 1 bit from each byte of Key String
          
	  unsigned char Modkey = Key_in[0] % 8;  // Take the remainder of the First Byte by performing Modulo 8 Division

            switch (Modkey)
              {
                case 0x00 : Selectbit = 0x01; break;
                case 0x01 : Selectbit = 0x02; break;
                case 0x02 : Selectbit = 0x04; break;
                case 0x03 : Selectbit = 0x08; break;
                case 0x04 : Selectbit = 0x10; break;
                case 0x05 : Selectbit = 0x20; break;
                case 0x06 : Selectbit = 0x40; break;
                case 0x07 : Selectbit = 0x80; break;
                default: break;
              }
              
            unsigned char k;  
                
                if ((Selectbit & key_sorted[0])   > 0x00) temp_key1 += 0x80;
                if ((Selectbit & key_sorted[1])   > 0x00) temp_key2 += 0x80;
                if ((Selectbit & key_sorted[2])   > 0x00) temp_key1 += 0x40;
                if ((Selectbit & key_sorted[3])   > 0x00) temp_key2 += 0x40;
                
                if ((Selectbit & key_sorted[4])   > 0x00) temp_key1 += 0x20;
                if ((Selectbit & key_sorted[5])   > 0x00) temp_key2 += 0x20;
                if ((Selectbit & key_sorted[6])   > 0x00) temp_key1 += 0x10;
                if ((Selectbit & key_sorted[7])   > 0x00) temp_key2 += 0x10;
                
                if ((Selectbit & key_sorted[8])   > 0x00) temp_key1 += 0x08;
                if ((Selectbit & key_sorted[9])   > 0x00) temp_key2 += 0x08;
                if ((Selectbit & key_sorted[10])  > 0x00) temp_key1 += 0x04;
                if ((Selectbit & key_sorted[11])  > 0x00) temp_key2 += 0x04;

                if ((Selectbit & key_sorted[12])  > 0x00) temp_key1 += 0x02;
                if ((Selectbit & key_sorted[13])  > 0x00) temp_key2 += 0x02;
                if ((Selectbit & key_sorted[14])  > 0x00) temp_key1 += 0x01;
                if ((Selectbit & key_sorted[15])  > 0x00) temp_key2 += 0x01;
                
        
          return (temp_key1 ^ temp_key2);   
                
          
        }
	
	
 // Dynamic-SBOX - Substitution of Bytes using Dynamic_SBOX
 void Dynamic_Sbox(char static_or_dynamic, unsigned char *sboxed_states, unsigned char *dynamic_keys)
	{	

            if(static_or_dynamic == 0) 
                {
                  char i;
                  for(i=0;i<16;i++)
                    {
                       sboxed_states[i] =  sta_sbox[sboxed_states[i]];
                    }                
                }  
            else
                {
      		  unsigned char offset;
                  offset = Key_Offset(dynamic_keys);
                  char i;
                  for(i=0;i<16;i++)
                    {
                       sboxed_states[i] =  sta_sbox[sboxed_states[i]] ^ offset;
                    }
                }
	}
	
	
 void Shift_Rows(unsigned char *rowshift_States)	
	{
		unsigned char buf1, buf2, buf3;
		
		// First Row requires No Shifting
		//		rowshift_States[0] = rowshift_States[0];
		//		rowshift_States[1] = rowshift_States[1];
		//		rowshift_States[2] = rowshift_States[2];
		//		rowshift_States[3] = rowshift_States[3];
		

		// Second Row requires Rotating / Shifting by one column to left
				buf1 = rowshift_States[4];
				rowshift_States[4] = rowshift_States[5];
				rowshift_States[5] = rowshift_States[6];
				rowshift_States[6] = rowshift_States[7];
				rowshift_States[7] = buf1;
		
		// Third Row requires Rotating / Shifting by two column to left
				buf1 = rowshift_States[8];
				buf2 = rowshift_States[9];
				rowshift_States[8] = rowshift_States[10];
				rowshift_States[9] = rowshift_States[11];
				rowshift_States[10] = buf1;
				rowshift_States[11] = buf2;
		
		// Fourth Row requires Rotating / Shifting by three column to left or one column by right
				buf1 = rowshift_States[12];
				buf2 = rowshift_States[13];
				buf3 = rowshift_States[14];
				rowshift_States[12] = rowshift_States[15];
				rowshift_States[13] = buf1;
				rowshift_States[14] = buf2;
				rowshift_States[15] = buf3;		
	
	}
	
 void  Mixed_Columns(unsigned char *mixColumn_States)
       {
         unsigned char j;
         unsigned char buf1, buf2, buf3;
         for(j=0;j<4;j++)
           {
              buf1 = mixColumn_States[0+j] ^ mixColumn_States[4+j] ^ mixColumn_States[8+j] ^ mixColumn_States[12+j];
              buf2 = mixColumn_States[0+j];
              
              buf3 = mixColumn_States[0+j] ^ mixColumn_States[4+j];
              buf3 = galois2_mul(buf3);
              mixColumn_States[0+j] = mixColumn_States[0+j] ^ buf3 ^ buf1;
              
              buf3 = mixColumn_States[4+j] ^ mixColumn_States[8+j];
              buf3 = galois2_mul(buf3);
              mixColumn_States[4+j] = mixColumn_States[4+j] ^ buf3 ^ buf1;
              
              buf3 = mixColumn_States[8+j] ^ mixColumn_States[12+j];
              buf3 = galois2_mul(buf3);
              mixColumn_States[8+j] = mixColumn_States[8+j] ^ buf3 ^ buf1;
              
              buf3 = mixColumn_States[12+j] ^ buf2;
              buf3 = galois2_mul(buf3);
              mixColumn_States[12+j] = mixColumn_States[12+j] ^ buf3 ^ buf1;
           }
            
       }	
       
       
       
 
 void RoundKey_Xor(char static_or_dynamic, unsigned char rnd_no,unsigned char *Round_Key_Xor_States,unsigned char *prev_next_roundkey)
      {
          if (rnd_no < 10)
              {   
                  // Calculate Dynamic Offset in Advance
                  
                  unsigned char offset; 
                  offset = Key_Offset(prev_next_roundkey);

              
                  unsigned char lastcolumn_buf[4];
                  // Take a copy of last row for future use in the pre-final step of this function
                  lastcolumn_buf[0]  = prev_next_roundkey[3];
                  lastcolumn_buf[1]  = prev_next_roundkey[7];
                  lastcolumn_buf[2]  = prev_next_roundkey[11];
                  lastcolumn_buf[3]  = prev_next_roundkey[15];
                  
                  // Rotate last column through top by one row
                  unsigned char buf1;
                  buf1 = prev_next_roundkey[3];
                  prev_next_roundkey[3]  = prev_next_roundkey[7];
                  prev_next_roundkey[7]  = prev_next_roundkey[11];
                  prev_next_roundkey[11] = prev_next_roundkey[15];
                  prev_next_roundkey[15] = buf1;
                  
                  if(static_or_dynamic == 0)  // Or one can also set offset to zero using if statement and use the following 4 lines to carry out the SBOX byte Substitution
                      {
                          // Sbox Substitution for the keys // currently static
                          prev_next_roundkey[3]   =  sta_sbox[prev_next_roundkey[3]];
                          prev_next_roundkey[7]   =  sta_sbox[prev_next_roundkey[7]];
                          prev_next_roundkey[11]  =  sta_sbox[prev_next_roundkey[11]];
                          prev_next_roundkey[15]  =  sta_sbox[prev_next_roundkey[15]];                        
                      }
                  
                  else
                      {
                          prev_next_roundkey[3]   =  sta_sbox[prev_next_roundkey[3]]  ^  offset;
                          prev_next_roundkey[7]   =  sta_sbox[prev_next_roundkey[7]]  ^  offset;
                          prev_next_roundkey[11]  =  sta_sbox[prev_next_roundkey[11]] ^  offset;
                          prev_next_roundkey[15]  =  sta_sbox[prev_next_roundkey[15]] ^  offset;
                      }                  

                  // New Set of Keys for Future Calculation  
                  prev_next_roundkey[0]   =  prev_next_roundkey[0]  ^ prev_next_roundkey[3]    ^ Rcon[rnd_no];
                  prev_next_roundkey[4]   =  prev_next_roundkey[4]  ^ prev_next_roundkey[7]    ^ 0x00;
                  prev_next_roundkey[8]   =  prev_next_roundkey[8]  ^ prev_next_roundkey[11]   ^ 0x00;
                  prev_next_roundkey[12]  =  prev_next_roundkey[12] ^ prev_next_roundkey[15]   ^ 0x00;

                  prev_next_roundkey[1]   =  prev_next_roundkey[1]   ^ prev_next_roundkey[0];
                  prev_next_roundkey[5]   =  prev_next_roundkey[5]   ^ prev_next_roundkey[4];
                  prev_next_roundkey[9]   =  prev_next_roundkey[9]   ^ prev_next_roundkey[8];
                  prev_next_roundkey[13]  =  prev_next_roundkey[13]  ^ prev_next_roundkey[12];

                  prev_next_roundkey[2]   =  prev_next_roundkey[2]   ^ prev_next_roundkey[1];
                  prev_next_roundkey[6]   =  prev_next_roundkey[6]   ^ prev_next_roundkey[5];
                  prev_next_roundkey[10]  =  prev_next_roundkey[10]  ^ prev_next_roundkey[9];
                  prev_next_roundkey[14]  =  prev_next_roundkey[14]  ^ prev_next_roundkey[13];

                  prev_next_roundkey[3]   =  lastcolumn_buf[0]   ^ prev_next_roundkey[2];
                  prev_next_roundkey[7]   =  lastcolumn_buf[1]   ^ prev_next_roundkey[6];
                  prev_next_roundkey[11]  =  lastcolumn_buf[2]   ^ prev_next_roundkey[10];
                  prev_next_roundkey[15]  =  lastcolumn_buf[3]   ^ prev_next_roundkey[14];
                    
                  unsigned char i;
        	  for(i=0;i<16;i++)
        	      {
        	          Round_Key_Xor_States[i] = Round_Key_Xor_States[i] ^ prev_next_roundkey[i];
        	      }  
              }     
              
      } 
      
      


 void All_Round_Keys(unsigned char static_or_dynamic,unsigned char *copykey, unsigned char *Round_Keys)
 
     {
       // Lets Generate a Set of 11 Round Keys // First one is Obviously CipherKey
       key_copier(copykey,Round_Keys); // Pre defined function to copy the 128 bits of data
       
       // We have to use the Dynamic SBOX to generate all the Round Keys
       // 
       unsigned char l; // this is just to keep track of rounds
        for(l=1;l<11;l++) // l = 0 corresponds to Original set of Keys
              {   
                  // Calculate Dynamic Offset in Advance
                  
                  unsigned char offset; 
                  offset = Key_Offset(&Round_Keys[16*(l-1)]);
              
                  unsigned char lastcolumn_buf[4];
                  
                 
                  if(static_or_dynamic == 0)  // Or one can also set offset to zero using if statement and use the following 4 lines to carry out the SBOX byte Substitution
                      {
                          // Sbox Substitution for the keys // currently static
                          lastcolumn_buf[0]  =  sta_sbox[Round_Keys[16*(l-1)+7]];
                          lastcolumn_buf[1]  =  sta_sbox[Round_Keys[16*(l-1)+11]];
                          lastcolumn_buf[2]  =  sta_sbox[Round_Keys[16*(l-1)+15]];
                          lastcolumn_buf[3]  =  sta_sbox[Round_Keys[16*(l-1)+3]];                        
                      }

                  else
                      {
                          lastcolumn_buf[0]   =  sta_sbox[Round_Keys[16*(l-1)+7] ] ^  offset;
                          lastcolumn_buf[1]   =  sta_sbox[Round_Keys[16*(l-1)+11]] ^  offset;
                          lastcolumn_buf[2]   =  sta_sbox[Round_Keys[16*(l-1)+15]] ^  offset;
                          lastcolumn_buf[3]   =  sta_sbox[Round_Keys[16*(l-1)+3] ] ^  offset;
                      }                  

                  // New Set of Keys for Future Calculation
                  
                  Round_Keys[16*l+0]   =  Round_Keys[16*(l-1)+0]  ^ lastcolumn_buf[0]  ^ Rcon[l-1]; // Provide appropriate Round Number
                  Round_Keys[16*l+4]   =  Round_Keys[16*(l-1)+4]  ^ lastcolumn_buf[1]  ^ 0x00;
                  Round_Keys[16*l+8]   =  Round_Keys[16*(l-1)+8]  ^ lastcolumn_buf[2]  ^ 0x00;
                  Round_Keys[16*l+12]  =  Round_Keys[16*(l-1)+12] ^ lastcolumn_buf[3]  ^ 0x00;

                  Round_Keys[16*l+1]   =  Round_Keys[16*(l-1)+1]   ^ Round_Keys[16*l+0];
                  Round_Keys[16*l+5]   =  Round_Keys[16*(l-1)+5]   ^ Round_Keys[16*l+4];
                  Round_Keys[16*l+9]   =  Round_Keys[16*(l-1)+9]   ^ Round_Keys[16*l+8];
                  Round_Keys[16*l+13]  =  Round_Keys[16*(l-1)+13]  ^ Round_Keys[16*l+12];

                  Round_Keys[16*l+2]   =  Round_Keys[16*(l-1)+2]   ^ Round_Keys[16*l+1];
                  Round_Keys[16*l+6]   =  Round_Keys[16*(l-1)+6]   ^ Round_Keys[16*l+5];
                  Round_Keys[16*l+10]  =  Round_Keys[16*(l-1)+10]  ^ Round_Keys[16*l+9];
                  Round_Keys[16*l+14]  =  Round_Keys[16*(l-1)+14]  ^ Round_Keys[16*l+13];

                  Round_Keys[16*l+3]   =  Round_Keys[16*(l-1)+3]   ^ Round_Keys[16*l+2];
                  Round_Keys[16*l+7]   =  Round_Keys[16*(l-1)+7]   ^ Round_Keys[16*l+6];
                  Round_Keys[16*l+11]  =  Round_Keys[16*(l-1)+11]  ^ Round_Keys[16*l+10];
                  Round_Keys[16*l+15]  =  Round_Keys[16*(l-1)+15]  ^ Round_Keys[16*l+14];

              }  
       
     }

 // Using the sets of Round Keys previous Calculated XOR
 void Decry_RoundKey(unsigned char static_or_dynamic,unsigned char rnd_no,unsigned char *All_Round_Keys,unsigned char *current_Cipher_state)
         {  
             // static_or_dynamic is not needed at this stage since Round Key generation is previously Acheived using required Strategy
             unsigned char i;
      	     for(i=0;i<16;i++)
        	 {
        	   current_Cipher_state[i] = current_Cipher_state[i] ^ All_Round_Keys[rnd_no*16+i];
        	 }            
         }


 // Perform invMixColumns = barreto + mixColumns // this is independent of key so no worries here
 void Decry_Mixed_Columns(unsigned char *current_Cipher_state)
        {
            unsigned char buf1, buf2, buf3;
            // barreto
            //col1
            buf1 = galois2_mul(galois2_mul(current_Cipher_state[0]^current_Cipher_state[8]));
            buf2 = galois2_mul(galois2_mul(current_Cipher_state[4]^current_Cipher_state[12]));
            current_Cipher_state[0] ^= buf1;
            current_Cipher_state[4] ^= buf2;
            current_Cipher_state[8] ^= buf1;
            current_Cipher_state[12] ^= buf2;
            //col2
            buf1 = galois2_mul(galois2_mul(current_Cipher_state[1]^current_Cipher_state[9]));
            buf2 = galois2_mul(galois2_mul(current_Cipher_state[5]^current_Cipher_state[13]));
            current_Cipher_state[1] ^= buf1;
            current_Cipher_state[5] ^= buf2;
            current_Cipher_state[9] ^= buf1;
            current_Cipher_state[13] ^= buf2;
            //col3
            buf1 = galois2_mul(galois2_mul(current_Cipher_state[2]^current_Cipher_state[10]));
            buf2 = galois2_mul(galois2_mul(current_Cipher_state[6]^current_Cipher_state[14]));
            current_Cipher_state[2]  ^= buf1;
            current_Cipher_state[6]  ^= buf2;
            current_Cipher_state[10] ^= buf1;
            current_Cipher_state[14] ^= buf2;
            //col4
            buf1 = galois2_mul(galois2_mul(current_Cipher_state[3]^current_Cipher_state[11]));
            buf2 = galois2_mul(galois2_mul(current_Cipher_state[7]^current_Cipher_state[15]));
            current_Cipher_state[3] ^= buf1;
            current_Cipher_state[7] ^= buf2;
            current_Cipher_state[11] ^= buf1;
            current_Cipher_state[15] ^= buf2;
            
            // mixcolums //////////
            // col1
            buf1 = current_Cipher_state[0] ^ current_Cipher_state[4] ^ current_Cipher_state[8] ^ current_Cipher_state[12];
            buf2 = current_Cipher_state[0];
            buf3 = current_Cipher_state[0] ^ current_Cipher_state[4];
            buf3=galois2_mul(buf3);
            current_Cipher_state[0] = current_Cipher_state[0] ^ buf3 ^ buf1;
            buf3 = current_Cipher_state[4] ^ current_Cipher_state[8];
            buf3=galois2_mul(buf3);
            current_Cipher_state[4] = current_Cipher_state[4] ^ buf3 ^ buf1;
            buf3 = current_Cipher_state[8] ^ current_Cipher_state[12];
            buf3=galois2_mul(buf3);
            current_Cipher_state[8] = current_Cipher_state[8] ^ buf3 ^ buf1;
            buf3 = current_Cipher_state[12] ^ buf2;
            buf3=galois2_mul(buf3);
            current_Cipher_state[12] = current_Cipher_state[12] ^ buf3 ^ buf1;
            // col2
            buf1 = current_Cipher_state[1] ^ current_Cipher_state[5] ^ current_Cipher_state[9] ^ current_Cipher_state[13];
            buf2 = current_Cipher_state[1];
            buf3 = current_Cipher_state[1] ^ current_Cipher_state[5];
            buf3=galois2_mul(buf3);
            current_Cipher_state[1] = current_Cipher_state[1] ^ buf3 ^ buf1;
            buf3 = current_Cipher_state[5] ^ current_Cipher_state[9];
            buf3=galois2_mul(buf3);
            current_Cipher_state[5] = current_Cipher_state[5] ^ buf3 ^ buf1;
            buf3 = current_Cipher_state[9] ^ current_Cipher_state[13];
            buf3=galois2_mul(buf3);
            current_Cipher_state[9] = current_Cipher_state[9] ^ buf3 ^ buf1;
            buf3 = current_Cipher_state[13] ^ buf2;
            buf3=galois2_mul(buf3);
            current_Cipher_state[13] = current_Cipher_state[13] ^ buf3 ^ buf1;
            // col3
            buf1 = current_Cipher_state[2] ^ current_Cipher_state[6] ^ current_Cipher_state[10] ^ current_Cipher_state[14];
            buf2 = current_Cipher_state[2];
            buf3 = current_Cipher_state[2] ^ current_Cipher_state[6];
            buf3=galois2_mul(buf3);
            current_Cipher_state[2] = current_Cipher_state[2] ^ buf3 ^ buf1;
            buf3 = current_Cipher_state[6] ^ current_Cipher_state[10];
            buf3=galois2_mul(buf3);
            current_Cipher_state[6] = current_Cipher_state[6] ^ buf3 ^ buf1;
            buf3 = current_Cipher_state[10] ^ current_Cipher_state[14];
            buf3=galois2_mul(buf3);
            current_Cipher_state[10] = current_Cipher_state[10] ^ buf3 ^ buf1;
            buf3 = current_Cipher_state[14] ^ buf2;
            buf3=galois2_mul(buf3);
            current_Cipher_state[14] = current_Cipher_state[14] ^ buf3 ^ buf1;
            // col4
            buf1 = current_Cipher_state[3] ^ current_Cipher_state[7] ^ current_Cipher_state[11] ^ current_Cipher_state[15];
            buf2 = current_Cipher_state[3];
            buf3 = current_Cipher_state[3] ^ current_Cipher_state[7];
            buf3=galois2_mul(buf3);
            current_Cipher_state[3] = current_Cipher_state[3] ^ buf3 ^ buf1;
            buf3 = current_Cipher_state[7] ^ current_Cipher_state[11];
            buf3=galois2_mul(buf3);
            current_Cipher_state[7] = current_Cipher_state[7] ^ buf3 ^ buf1;
            buf3 = current_Cipher_state[11] ^ current_Cipher_state[15];
            buf3=galois2_mul(buf3);
            current_Cipher_state[11] = current_Cipher_state[11] ^ buf3 ^ buf1;
            buf3 = current_Cipher_state[15] ^buf2;
            buf3=galois2_mul(buf3);
            current_Cipher_state[15] = current_Cipher_state[15] ^ buf3 ^ buf1;    
                  
        }
  

 // rightshift instead of leftshift  this is independent of keys
 void Decry_Shift_Rows(unsigned char *current_Cipher_state) 
        {
            unsigned char buf1, buf2, buf3;
               // No Need of Shifting for the First Row
                //  current_Cipher_state[ 3]  = current_Cipher_state[ 3];
                //  current_Cipher_state[ 2]  = current_Cipher_state[ 2];
                //  current_Cipher_state[ 1]  = current_Cipher_state[ 1];
                //  current_Cipher_state[ 0]  = current_Cipher_state[ 0];
                
              // row 1  Shift this Row by 1 Column to Right
              buf1 =       current_Cipher_state[7];
              current_Cipher_state[ 7]  = current_Cipher_state[ 6];
              current_Cipher_state[ 6]  = current_Cipher_state[ 5];
              current_Cipher_state[ 5]  = current_Cipher_state[ 4];
              current_Cipher_state[ 4]  = buf1;
              
              // row 2  Shift this Row by 1 Column to Right
              buf1 =       current_Cipher_state[ 11];
              buf2 =       current_Cipher_state[ 10];
              current_Cipher_state[11]  = current_Cipher_state[9];
              current_Cipher_state[10]  = current_Cipher_state[8];
              current_Cipher_state[ 9]  = buf1;
              current_Cipher_state[ 8]  = buf2;
              
              // row 3  Shift this Row by 3 Column to Right  or 1 Column to Left
              buf1 =       current_Cipher_state[ 15];
              buf2 =       current_Cipher_state[ 14];
              buf3 =       current_Cipher_state[ 13];
              current_Cipher_state[15]  = current_Cipher_state[ 12];
              current_Cipher_state[14]  = buf1;
              current_Cipher_state[13]  = buf2;
              current_Cipher_state[12]  = buf3;
                     
          
        }
 
 
 
 // Using rsbox which is inverse of sbox
 void Dynamic_Inv_Sbox(unsigned char static_or_dynamic,unsigned char *current_Cipher_state,unsigned char rnd_no,unsigned char *All_Round_Keys) // The SBOX is Dynamic by using Previous Keys
        {
          
           if(static_or_dynamic == 0) 
                {
                  char i;
                  for(i=0;i<16;i++)
                    {
                       current_Cipher_state[i] =  sta_rsbox[current_Cipher_state[i]];
                    }                
                }  
            else
                {
      		  unsigned char offset;
                  offset = Key_Offset(&All_Round_Keys[16*(rnd_no-1)]);  // Choosing the proper round for key structure is essential


                  char i;
                  for(i=0;i<16;i++)
                    {
                        // This Steps the Offset that has been added by the Encryption
                       current_Cipher_state[i] =  sta_rsbox[current_Cipher_state[i] ^ offset ];
                    }
                    

                }
          
        }
        
        

// Now lets create a function which takes in the STATE and the KEY as Input and gives Encrypted States
void GAP_aes_encrypt(unsigned char stat_Dynamic, unsigned char Block_Segmentation, unsigned char *state, unsigned char *key) // Make a copy of the STATES since it will be Overwritten
	{	
                // Universal Declaration of Variables //
                unsigned char i,j;
                // lets take a copy of the key and use that copy from now on
                unsigned char copykey[16]; // the key is just 16 bytes of Unsigned Data
          	unsigned char data_temp[16];
                
                if(Block_Segmentation)   // If Yes Do Resegmentation as follows // Other than 0 holds true
                    {
                        for(i=0; i<16 ; i++)
                          {
                             data_temp[i] = state[i];
                          }
                        for(i=0; i < 4 ; i++)
                          {
                             state[0+i*4] = data_temp[i];
                             state[1+i*4] = data_temp[i+4];
                             state[2+i*4] = data_temp[i+8];
                             state[3+i*4] = data_temp[i+12];
                          }
                          
        	       // The Segmentation of Key is Not reflected on the Original Key
                       for(i=0; i < 4 ; i++)
                          {
                             copykey[0+i*4] = key[i];
                             copykey[1+i*4] = key[i+4];
                             copykey[2+i*4] = key[i+8];
                             copykey[3+i*4] = key[i+12];
                          }  
                      
                    }
                    
                  else
                  
                     {
              		key_copier(key,copykey); // This fuction does the copy of the origianl key                      
                     }
  

		
		// First Step of Encryption is Simple XOR with Original KEY
		for(i=0;i<16;i++)
			{
				state[i] = copykey[i] ^ state[i];
			}
		
		// Now we start nine rounds of SBOX substitution, ShiftRows, MixedColumns, RoundKeyXoring

          	for(j=0;j<9;j++)
	              {
		          Dynamic_Sbox(stat_Dynamic,state,copykey); // The SBOX is Dynamic by using Previous Keys
			  Shift_Rows(state); // This shift rows of the STATES by a different offsets
			  Mixed_Columns(state); // This performs MixedColumns on the STATES blocks
			  RoundKey_Xor(stat_Dynamic,j,state,copykey);// This include alteration of both STATES and COPYKEY

		      }
                
		// The 10th round consists of SBOX substitution, ShiftRows & RoundKeyXoring since Performing a Mixed Column is just a waste of resource at this point..
		
		// j = 9; // The 10 th Round incrementation is done in For Loop
		
			Dynamic_Sbox(stat_Dynamic,state,copykey); // The SBOX is Dynamic by using Previous Keys
			Shift_Rows(state); // This shift rows of the STATES by a different offsets
			RoundKey_Xor(stat_Dynamic,j,state,copykey); // This include alteration of both STATES and COPYKEY, and J is necessary to choose RCON(Round Constant)		
		
                   
                   // This can be removed if the obtained data is in compatible matrix format
                   // but for Decrypting Using the Same Library Perform this Step
                   if(Block_Segmentation)   // If Yes Do Resegmentation to reverse the segmentation as follows
                    {
                        for(i=0; i<16 ; i++)
                          {
                             data_temp[i] = state[i];
                          }      
                          
                        for(i=0; i<4 ; i++)
                          {
                             state[0+i*4] = data_temp[i];
                             state[1+i*4] = data_temp[i+4];
                             state[2+i*4] = data_temp[i+8];
                             state[3+i*4] = data_temp[i+12];
                          }
                    }

	}







void GAP_aes_decrypt(unsigned char stat_Dynamic, unsigned char Block_Segmentation, unsigned char *cipher_state, unsigned char *key)
        {
          
          // Esspecially for Decryption we need large amount of Data Space since one has to create all the round keys in advance Using the Cipher Key
          // This is going to take a lot of time
          
           // Universal Declaration of Variables //
                unsigned char i,j;
                // lets take a copy of the key and use that copy from now on
                unsigned char copykey[16]; // the key is just 16 bytes of Unsigned Data
          	unsigned char data_temp[16];
                
                if(Block_Segmentation)   // If Yes Do Resegmentation as follows // Other than 0 holds true
                    {
                        for(i=0; i<16 ; i++)
                          {
                             data_temp[i] = cipher_state[i];
                          }
                        for(i=0; i < 4 ; i++)
                          {
                             cipher_state[0+i*4] = data_temp[i];
                             cipher_state[1+i*4] = data_temp[i+4];
                             cipher_state[2+i*4] = data_temp[i+8];
                             cipher_state[3+i*4] = data_temp[i+12];
                          }
                          
        	       // The Segmentation of Key is Not reflected on the Original Key
                       for(i=0; i < 4 ; i++)
                          {
                             copykey[0+i*4] = key[i];
                             copykey[1+i*4] = key[i+4];
                             copykey[2+i*4] = key[i+8];
                             copykey[3+i*4] = key[i+12];
                          }  
                      
                    }
                    
                  else
                  
                     {
              		key_copier(key,copykey); // This fuction does the copy of the origianl key                      
                     }
  
  
  
                // Lets generate all the Round keys using the Cipher Key..at Once
                unsigned char Round_Keys[16*11]; // for all one + ten rounds
                
                All_Round_Keys(stat_Dynamic,copykey,Round_Keys); // This Function just Generates all the Round Keys and calculates Offsets

////////////////////// This is Just for Vizulization /////////////////////////////               
//                key_copier(&Round_Keys[160], key);
//                
//                   if(Block_Segmentation)   // If Yes Do Resegmentation to reverse the segmentation as follows
//                    {
//                        for(i=0; i<16 ; i++)
//                          {
//                             copykey[i] = key[i];
//                          }      
//                          
//                        for(i=0; i<4 ; i++)
//                          {
//                             key[0+i*4] = copykey[i];
//                             key[1+i*4] = copykey[i+4];
//                             key[2+i*4] = copykey[i+8];
//                             key[3+i*4] = copykey[i+12];
//                          }
//                    }                
                
////////////////////// This is Just for Vizulization /////////////////////////////               

                j = 10;
               
                	
		// First Step of Decryption is a Simple XOR of STATE data with Final Round Key that has been generated...
		for(i=0;i<16;i++)
			{
				cipher_state[i] = Round_Keys[160+i] ^ cipher_state[i];  // Choose Round Key From Last Round 
			}

	          Decry_Shift_Rows(cipher_state); // This shift rows of the STATES by a different offsets

                  Dynamic_Inv_Sbox(stat_Dynamic,cipher_state,j,Round_Keys); // The SBOX is Dynamic by using Previous Keys


//		// Now we start nine rounds of SBOX substitution, ShiftRows, MixedColumns, RoundKeyXoring
          	for(j=9;j>0;j--)
	              {
		          Decry_RoundKey(stat_Dynamic,j,Round_Keys,cipher_state);  // Use of stat_Dynamic variable has no Use at this junction
			  Decry_Mixed_Columns(cipher_state); // This performs MixedColumns on the STATES blocks
			  Decry_Shift_Rows(cipher_state); // This shift rows of the STATES by a different offsets
                          Dynamic_Inv_Sbox(stat_Dynamic,cipher_state,j,Round_Keys); // The SBOX is Dynamic by using Previous Keys
		      }
                
		    

                    Decry_RoundKey(stat_Dynamic,j,Round_Keys,cipher_state);
		
                   
                   // This can be removed if the obtained data is in compatible matrix format
                   if(Block_Segmentation)   // If Yes Do Resegmentation to reverse the segmentation as follows
                    {
                        for(i=0; i<16 ; i++)
                          {
                             data_temp[i] = cipher_state[i];
                          }      
                          
                        for(i=0; i<4 ; i++)
                          {
                             cipher_state[0+i*4] = data_temp[i];
                             cipher_state[1+i*4] = data_temp[i+4];
                             cipher_state[2+i*4] = data_temp[i+8];
                             cipher_state[3+i*4] = data_temp[i+12];
                          }
                    }

          
          
          
        }


// Now lets create a function which takes in the STATE and the KEY as Input and gives Decrypted States






 
