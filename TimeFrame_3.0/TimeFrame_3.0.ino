// TimeFrame V3.1 - simple version
// Copyright (C) 2016 Cubc-Print

// get the latest source core here: http://www.github.com/cubic-print/timeframe
// video: http://youtu.be/LlGywKkifcI
// order your DIY kit here: http://www.cubic-print.com/TimeFrame

//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    GNU General Public License terms: <http://www.gnu.org/licenses/>.


#define DEBUG
//uncomment to check serial monitor and see LED heartbeat
//Tactile Switch needs to be pressed longer in debug mode to change mode
#define ROTARY_MAXVALUE 50
#define ROTARY_MINVALUE 0
//Base frequency and trimmer Ranges
#define BASE_FREQ 80.0 //80 in on the spot for many flowers. Feel free to play with this +/-5Hz
#define MIN_PHASE_SHIFT 0.1
#define MAX_PHASE_SHIFT 5.0
#define DEFAULT_PHASE_SHIFT 0.1 // Update Reference 0.1
#define MIN_BRIGHTNESS 2 //if too low the movement will be only visible in darker rooms
#define MAX_BRIGHTNESS 10.0 //with high settings flickering will occur
#define DEFAULT_BRIGHTNESS 9.84 // Update Reference 7

#define MIN_STRENGTH 8 //low for small items / thinner prong
#define MAX_STRENGTH 30 //strong field for thicker prong
#define DEFAULT_STRENGTH 30 // Update Reference

const int LED = 13; //on board LED
const int SW = A4; //button for mode selection

int cur_ctrl = 0;// default brightness 1 phase shift , 2 magnetic strength
const int ROT_CLK = 2; //4//button for mode selection
const int ROT_DAT = 4; //2//button for mode selection
int counter = 0;
int encState;
int encLastState;

int mode_changed = 1;
int mode = 1; //toggeled by SW button
//mode 1 = normal slow motion mode (power on)
//mode 2 = distorted reality mode
//mode 3 = magnet off
//mode 4 = completely off

float phase_shift = DEFAULT_PHASE_SHIFT; //eg. f=0.5 -> T=2 -> 2 seconds per slow motion cycle

//Timer 2 for Magnet
//Prescaler = 1024 = CS111 = 64us/tick
//PIN 3
float duty_mag = DEFAULT_STRENGTH; //12 // be carefull not overheat the magnet by setting > 15. better adjust force through magnet position
float frequency_mag = BASE_FREQ;
long time_mag = round(16000000 / 1024 / frequency_mag); //bit count ~ 1024

//Timer 1 for LED
//Prescaler = 8 = CS010 = 0.5 us/tick
//PIN 10
float duty_led = DEFAULT_BRIGHTNESS;
float frequency_led = frequency_mag + phase_shift;
long time_led = round(16000000 / 8 / frequency_led);

long cur_rot_brightness = 1;//26//-(MAX_BRIGHTNESS - MIN_BRIGHTNESS) / ROTARY_MAXVALUE * (cur_rot_brightness) + MAX_BRIGHTNESS // -(10-2)/50*(1)+10
long cur_rot_phase_shift = 44;//49//50 will give absolute 0.1 phase shift // -(MAX_PHASE_SHIFT - MIN_PHASE_SHIFT) / ROTARY_MAXVALUE * (cur_rot_phase_shift) + MAX_PHASE_SHIFT; //-(5.0-0.1)/50*(44)+5.0
long cur_rot_strength = 0;//0 will give absolute 15 full strength

long autoresetduration = 3000;
long autoreset_counter = 1000;

// Best Performance by default
//Frame Mode: 1
//Phase Shift: 0.69
//Force: 30.00
//Freq: 80.00
//Brightness: 9.84


void setup() {
  Serial.begin(115200);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }

  pinMode(LED, OUTPUT); //Heart Beat LED

  pinMode(ROT_CLK, INPUT_PULLUP);
  pinMode(ROT_DAT, INPUT_PULLUP);
  // Reads the initial state of the outputA
  encLastState = digitalRead(ROT_CLK);

  pinMode(SW, INPUT_PULLUP); //button pin //pull up for the Analog Pin

  pinMode(3, OUTPUT); //MAG: Timer 2B cycle output
  pinMode(10, OUTPUT); //LED: Timer 1B cycle output

  // #ifdef DEBUG_PINS
  //   pinMode(11, OUTPUT); //Timer 2A half frequency at 50% duty output for debugging halbe frequenz! 50% duty
  //   pinMode(9, OUTPUT); //Timer 1A half frequency at 50% duty output for debuggin
  // #endif

  duty_led = -(MAX_BRIGHTNESS - MIN_BRIGHTNESS) / ROTARY_MAXVALUE * (cur_rot_brightness) + MAX_BRIGHTNESS; //Brightness: duty_led 2..20
  phase_shift = -(MAX_PHASE_SHIFT - MIN_PHASE_SHIFT) / ROTARY_MAXVALUE * (cur_rot_phase_shift) + MAX_PHASE_SHIFT; //Speed: 0.1 .. 5 Hz
  duty_mag = -(MAX_STRENGTH - MIN_STRENGTH) / ROTARY_MAXVALUE * (cur_rot_strength) + MAX_STRENGTH; //8 to 30

  mag_on();
  OCR2A = round(time_mag); //Hierraus frequenz output compare registers
  OCR2B = round(duty_mag * time_mag / 100L); //hierraus frequency output compare registers
  led_on();
  OCR1A = round(time_led); //Hierraus frequenz output compare registers
  OCR1B = round(duty_led * time_led / 100L); //hierraus frequency output compare registers
  sei();
}

void loop() {

  getEncoderRotUpdates();
  getEncoderSwitchUpdates();

  frequency_led = frequency_mag * mode + phase_shift;

  updateModeParams();
  //updateDebugReport();
} //main loop

void getEncoderSwitchUpdates() {
  if (digitalRead(SW) == LOW) //Read in switch
  {
    autoreset_counter = autoresetduration;
    long init_press = millis();
    cur_ctrl = 0;
    while (!digitalRead(SW)) {
      long elapsedtime = init_press - millis();
      elapsedtime = abs(elapsedtime);
      if (elapsedtime > 50 && elapsedtime <= 500)
      {
        cur_ctrl = -1;
      }
      if (elapsedtime > 500 && elapsedtime <= 1000 && cur_ctrl != 1)
      {
        cur_ctrl = 1;
        // blink LED here
        digitalWrite(LED, HIGH); // LED off
        delay(100);
        digitalWrite(LED, LOW); // LED on
      }
      if (elapsedtime > 1000)
      {
        cur_ctrl = 2;
        // blink LED here
        digitalWrite(LED, HIGH); // LED off
        delay(100);
        digitalWrite(LED, LOW); // LED on
        continue;
      }
    }
    if (cur_ctrl == -1)
    {
      cur_ctrl = 0;
      mode += 1;
      if (mode >= 5) mode = 1; //rotary menu
      mode_changed = 1;
    }
    updateDebugReport();
  }
  else
  {
    // reset switch to light / brightness ctrl after 3 seconds
    if (autoreset_counter >= 0)
    {
      autoreset_counter--;
      delay(1);
    }
    if (autoreset_counter == 0)
    {
      cur_ctrl = 0;
      // blink LED here
      digitalWrite(LED, HIGH); // LED off
      delay(100);
      digitalWrite(LED, LOW); // LED on
      updateDebugReport();
    }
  }
}

void updateModeParams() {
  // Manage Mode Changes
  if (mode_changed == 1)
  {
    if (mode == 1)
    {
      frequency_mag = BASE_FREQ;
      mag_on();
      led_on();
      //mode_changed = 0;
    }//mode = 1
    if (mode == 2)
    {
      //frequency doubleing already done in main loop
      //mode_changed = 0;
    }//mode = 2
    if (mode == 3)
    {
      mag_off(); //mode = 2
      //mode_changed = 0;
    }//mode = 3
    if (mode == 4)
    {
      led_off(); //mode = 4
      //mode_changed = 0;
    }//mode = 4
  }
  // Bring back Mode Change
  mode_changed = 0;
  time_mag = round(16000000L / 1024L / frequency_mag);
  time_led = round(16000000L / 8L / frequency_led);

  OCR2A = round(time_mag); //to calculate frequency of output compare registers
  OCR2B = round(duty_mag * time_mag / 100L);
  OCR1A = round(time_led);
  OCR1B = round(duty_led * time_led / 100L);
}

void getEncoderRotUpdates() {
  encState = digitalRead(ROT_CLK); // Reads the "current" state of the outputA
  // If the previous and the current state of the outputA are different, that means a Pulse has occured
  if (encState != encLastState) {
    // If the outputB state is different to the outputA state, that means the encoder is rotating clockwise
    if (digitalRead(ROT_DAT) != encState) {
      switch (cur_ctrl) {
        case 0 :
          if (ROTARY_MINVALUE < cur_rot_brightness - 1)
            cur_rot_brightness = --cur_rot_brightness % ROTARY_MAXVALUE;
          cur_rot_brightness = abs(cur_rot_brightness);
          duty_led = -(MAX_BRIGHTNESS - MIN_BRIGHTNESS) / ROTARY_MAXVALUE * (cur_rot_brightness) + MAX_BRIGHTNESS; //Brightness: duty_led 2..20
          break;
        case 1 :
          if (ROTARY_MINVALUE < cur_rot_phase_shift - 1)
            cur_rot_phase_shift = --cur_rot_phase_shift % ROTARY_MAXVALUE;
          cur_rot_phase_shift = abs(cur_rot_phase_shift);
          phase_shift = -(MAX_PHASE_SHIFT - MIN_PHASE_SHIFT) / ROTARY_MAXVALUE * (cur_rot_phase_shift) + MAX_PHASE_SHIFT; //Speed: 0.1 .. 5 Hz
          break;
        case 2 :
          if (ROTARY_MINVALUE < cur_rot_strength - 1)
            cur_rot_strength = --cur_rot_strength % ROTARY_MAXVALUE;
          cur_rot_strength = abs(cur_rot_strength);
          duty_mag = -(MAX_STRENGTH - MIN_STRENGTH) / ROTARY_MAXVALUE * (cur_rot_strength) + MAX_STRENGTH; //core strength variations
          break;
      }
    } else {
      switch (cur_ctrl) {
        case 0 :
          if (ROTARY_MAXVALUE > cur_rot_brightness + 1)
            cur_rot_brightness = ++cur_rot_brightness % ROTARY_MAXVALUE;
          cur_rot_brightness = abs(cur_rot_brightness);
          duty_led = -(MAX_BRIGHTNESS - MIN_BRIGHTNESS) / ROTARY_MAXVALUE * (cur_rot_brightness) + MAX_BRIGHTNESS; //Brightness: duty_led 2..20
          break;
        case 1 :
          if (ROTARY_MAXVALUE > cur_rot_phase_shift + 1)
            cur_rot_phase_shift = ++cur_rot_phase_shift % ROTARY_MAXVALUE;
          cur_rot_phase_shift = abs(cur_rot_phase_shift);
          phase_shift = -(MAX_PHASE_SHIFT - MIN_PHASE_SHIFT) / ROTARY_MAXVALUE * (cur_rot_phase_shift) + MAX_PHASE_SHIFT; //Speed: 0.1 .. 5 Hz
          break;
        case 2 :
          if (ROTARY_MAXVALUE > cur_rot_strength + 1)
            cur_rot_strength = ++cur_rot_strength % ROTARY_MAXVALUE;
          cur_rot_strength = abs(cur_rot_strength);
          duty_mag = -(MAX_STRENGTH - MIN_STRENGTH) / ROTARY_MAXVALUE * (cur_rot_strength) + MAX_STRENGTH; //core strength variations
          break;
      }
    }
    autoreset_counter = autoresetduration;
    updateDebugReport();
  }
  encLastState = encState; // Updates the previous state of the outputA with the current state
}

void updateDebugReport() {
  // Only if DEBUG is enabled
#ifdef DEBUG
  //Heatbeat on-board LED
  //digitalWrite(LED, HIGH); // LED on
  //delay(300);
  //digitalWrite(LED, LOW); // LED off
  //delay(300);
  //digitalWrite(LED, HIGH); // LED on
  //delay(200);
  //digitalWrite(LED, LOW); // LED off
  //delay(1200);
  //serial print current parameters
  Serial.print("  Frame Mode: "); //speed of animation
  Serial.println(mode);
  Serial.print("  Phase Shift: "); //speed of animation
  Serial.println(phase_shift);
  Serial.print("  Force: ");
  Serial.println(duty_mag);
  Serial.print("  Freq: ");
  Serial.println(frequency_mag);
  Serial.print("  Brightness: ");
  Serial.println(duty_led);
  Serial.println();
#endif
}

void mag_on() {
  TCCR2A = 0;
  TCCR2B = 0;
  TCCR2A = _BV(COM2A0) | _BV(COM2B1) | _BV(WGM21) | _BV(WGM20);
  TCCR2B = _BV(WGM22) | _BV(CS22) | _BV(CS21) | _BV(CS20);
}

void mag_off() {
  TCCR2A = 0;
  TCCR2B = 0;
  TCCR2A = _BV(COM2A0) | _BV(COM2B1);
  TCCR2B = _BV(CS22) | _BV(CS21) | _BV(CS20);
}

void led_on() {
  TCCR1A = 0;
  TCCR1B = 0;
  TCCR1A = _BV(COM1A0) | _BV(COM1B1) | _BV(WGM11) | _BV(WGM10);
  TCCR1B =  _BV(WGM13) | _BV(WGM12)  |  _BV(CS11);
}

void led_off() {
  TCCR1A = 0;
  TCCR1B = 0;
  TCCR1A = _BV(COM1A0) | _BV(COM1B1);
  TCCR1B =  _BV(CS11);
}
