/*
  Ultrasonic Sensor HC-SR04 and Arduino Tutorial

  Crated by Dejan Nedelkovski,
  www.HowToMechatronics.com

*/

// defines pins numbers
const int trigPin = 4;//10 //mega R2pro
const int echoPin = 5;//11 //mega R2pro

// defines variables
long duration;
int distance;

void setup() {

  // initialize digital pin LED_BUILTIN as an output.
  pinMode(13, OUTPUT);
  pinMode(9, OUTPUT);

  pinMode(2, OUTPUT);
  pinMode(3, OUTPUT);

  pinMode(8, OUTPUT);
  pinMode(12, OUTPUT);


  pinMode(trigPin, OUTPUT); // Sets the trigPin as an Output
  pinMode(echoPin, INPUT); // Sets the echoPin as an Input
  Serial.begin(9600); // Starts the serial communication
}

void loop() {
  // Clears the trigPin
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);

  // Sets the trigPin on HIGH state for 10 micro seconds
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);

  // Reads the echoPin, returns the sound wave travel time in microseconds
  duration = pulseIn(echoPin, HIGH);

  // Calculating the distance
  distance = duration * 0.034 / 2;

  // Prints the distance on the Serial Monitor
  Serial.print("Distance: ");
  Serial.println(distance);

  if (distance != 0) {
    if (distance > 130)
    {
      digitalWrite(9, HIGH);
      digitalWrite(13, HIGH);

      digitalWrite(2, LOW);
      digitalWrite(3, LOW);

      digitalWrite(8, LOW);
      digitalWrite(12, LOW);
    }
    else if (distance >= 60) {
      digitalWrite(9, LOW);
      digitalWrite(13, LOW);

      digitalWrite(2, HIGH);
      digitalWrite(3, HIGH);

      digitalWrite(8, LOW);
      digitalWrite(12, LOW);
    }
    else if (distance < 60) {

      digitalWrite(9, LOW);
      digitalWrite(13, LOW);

      digitalWrite(2, LOW);
      digitalWrite(3, LOW);

      digitalWrite(8, HIGH);
      digitalWrite(12, HIGH);
    }
  }

}
