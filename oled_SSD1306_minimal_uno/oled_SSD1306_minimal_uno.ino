#include "SSD1306_minimal_uno.h"
#include <avr/pgmspace.h>

#define DEG "\xa7" "C"

SSD1306_Mini_uno oled; // Declare the OLED object

void splash() {
 oled.startScreen();
 oled.clear(); // Clears the display

 oled.cursorTo(0, 0); // x:0, y:0
 oled.printString("Hello World!");
 oled.cursorTo(0, 10); // x:0, y:23
 oled.printString("ATtiny85!");
}

void setup() {
  
 oled.init(0x3C); // Initializes the display to the specified address
 oled.clear(); // Clears the display
 delay(500); // Delay for 1 second
 splash(); // Write something to the display (refer to the splash() method
}

void loop() {
}
