//Author: Gowrav
//More information welcome to : http://geeksynergy.com
#include "default_font.h"

#define NO_SLAB  8
#define NO_SLICE  4
#define NO_CHAR   8//4
#define CHAR_LINES   16//4
#define LED_BRIGHTNESS   300 // 0 to 1000 // 400 is optimum

String dispString = "GEEKSHOP";

int latchPin = 8; //LT
int clockPin = 12; //SK
int dataPin = 11; //R1

int en_74138 = 2;
int la_74138 = 3;
int lb_74138 = 4;
int lc_74138 = 5;
int ld_74138 = 6;

int k = 0;


void shiftOut(unsigned char dataOut)
{
//  for (int i = 7; i >= 0  ; i--)
//  {
//    PORTB &= ~(1 << (6));
//    if (dataOut & (0x01 << i))
//      PORTB &= ~(1 << (5));
//    else
//      PORTB |= 1 << (5);
//    PORTB |= 1 << (6);
//  }
  for (int i = 7; i >= 0  ; i--)
  {
    digitalWrite(clockPin, LOW);
    if (dataOut & (0x01 << i))
      digitalWrite(dataPin, LOW);
    else
      digitalWrite(dataPin, HIGH);
    digitalWrite(clockPin, HIGH);
  }
}


void cyclePower(int l)
{
  //delay(1);
  digitalWrite(en_74138, LOW);
  digitalWrite(la_74138, l % 2 > 0);
  digitalWrite(lb_74138, l % 4 > 1);
  digitalWrite(en_74138, HIGH);
  delayMicroseconds(LED_BRIGHTNESS);
  digitalWrite(en_74138, LOW);
}


uint8_t msg_board[] =
{
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
};



void display_msg(String eightchars)
{

  // Reduce string length to 8
  if (eightchars.length() > 8)
    eightchars = eightchars.substring(0, 8);

  // Optimize string with spaces to 8
  if (eightchars.length() < 8)
  {
    String temp = "";
    for (int i = 0; i < (8 - eightchars.length()) / 2; i++)
      temp += " ";
    temp += eightchars;
    for (int i = 0; i < (((8 - eightchars.length()) / 2) + eightchars.length() % 2); i++)
      temp += " ";
    eightchars = temp;
  }

  for (int y = 0; y < NO_CHAR; y++)
    for (int x = 0; x < CHAR_LINES; x++)
    {
      msg_board[(CHAR_LINES * y) + x] = pgm_read_word_near(font8x16_basic + (eightchars.charAt(y) * CHAR_LINES) + x); //font8x16_basic[(eightchars.charAt(y) * CHAR_LINES) + x];
    }


  digitalWrite(en_74138, LOW);//Turn off display
  for (int i = 0; i < NO_SLAB * NO_SLICE; i++)
  {
    for (int l = 3; l >= 0 ; l--)
    {
      for (int m = 0; m < NO_CHAR; m++)
      {
        shiftOut(msg_board[m * CHAR_LINES + ((4 * 3) + l)]);
        shiftOut(msg_board[m * CHAR_LINES + ((4 * 2) + l)]);
        shiftOut(msg_board[m * CHAR_LINES + ((4 * 1) + l)]);
        shiftOut(msg_board[m * CHAR_LINES + (l)]);
        digitalWrite(latchPin, LOW); digitalWrite(latchPin, HIGH); digitalWrite(latchPin, LOW);
      }
      cyclePower(l);
    }
  }
}



void displayScroll_msg(String longString)
{

  longString = "        " + longString;

  for (int m = 0; m < longString.length(); m ++)
  {
    String shortString = "";
    if (m + 8 < longString.length())
      shortString = longString.substring(m, m + 8);
    else
      shortString = longString.substring(m);


    // Set msgBoardData here
    for (int y = 0; y < NO_CHAR; y++)
      for (int x = 0; x < CHAR_LINES; x++)
      {
        msg_board[(CHAR_LINES * y) + x] = pgm_read_word_near(font8x16_basic + (shortString.charAt(y) * CHAR_LINES) + x);//font8x16_basic[(shortString.charAt(y) * CHAR_LINES) + x];
      }


    digitalWrite(en_74138, LOW);//Turn off display
    for (int i = 0; i < NO_SLAB * NO_SLICE; i++)
    {
      for (int l = 3; l >= 0 ; l--)
      {
        for (int m = 0; m < NO_CHAR; m++)
        {
          shiftOut(msg_board[m * CHAR_LINES + ((4 * 3) + l)]);
          shiftOut(msg_board[m * CHAR_LINES + ((4 * 2) + l)]);
          shiftOut(msg_board[m * CHAR_LINES + ((4 * 1) + l)]);
          shiftOut(msg_board[m * CHAR_LINES + (l)]);
          digitalWrite(latchPin, LOW); digitalWrite(latchPin, HIGH); digitalWrite(latchPin, LOW);
        }
        cyclePower(l);
      }
    }
  }
}


void display_flush()
{
  digitalWrite(en_74138, LOW);
  for (int l = 0; l < NO_SLAB * NO_SLICE ; l++)
  {
    shiftOut(0);
    digitalWrite(latchPin, LOW); digitalWrite(latchPin, HIGH); digitalWrite(latchPin, LOW);
  }
  digitalWrite(en_74138, HIGH);

}

void init_matrixDisplay()
{
  pinMode(latchPin, OUTPUT);
  pinMode(clockPin, OUTPUT);
  pinMode(dataPin, OUTPUT);
  pinMode(en_74138, OUTPUT);
  pinMode(la_74138, OUTPUT);
  pinMode(lb_74138, OUTPUT);
  pinMode(lc_74138, OUTPUT);
  pinMode(ld_74138, OUTPUT);
  digitalWrite(lc_74138, LOW);
  digitalWrite(ld_74138, LOW);
  digitalWrite(en_74138, LOW);
  display_flush();
}


void setup()
{
  Serial.begin(9600);
  init_matrixDisplay();
  display_msg(dispString);
  display_flush();
}

void loop()
{
  if (Serial.available()) {
    dispString = Serial.readString();
  }
  displayScroll_msg(dispString);
}
