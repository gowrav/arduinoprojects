
#define SCL_PORT PORTB
#define SCL_PIN 2

#define SDA_PORT PORTB
#define SDA_PIN 0

#define I2C_TIMEOUT 100
#define I2C_FASTMODE 0

#include <SoftWire.h>
#include <SoftwareSerial.h>

SoftWire Wire = SoftWire();
SoftwareSerial tinySerial(3, 4);

void setup()
{
  Wire.begin();

  tinySerial.begin(9600);
  tinySerial.println(F("\nI2C Scanner"));
}


void loop()
{
  byte error, address;
  int nDevices;

  tinySerial.println(F("Scanning I2C"));

  nDevices = 0;
  for (address = 1; address < 127; address++ )
  {
    // The i2c_scanner uses the return value of
    // the Write.endTransmisstion to see if
    // a device did acknowledge to the address.
    Wire.beginTransmission(address);
    error = Wire.endTransmission();

    if (error == 0)
    {
      tinySerial.println("");
      tinySerial.print(F("Device found @  0x"));
      if (address < 16)
        tinySerial.print(F("0"));
      tinySerial.print(address, HEX);
      tinySerial.println(F("  !"));

      nDevices++;
    }
    else if (error == 4)
    {
      tinySerial.print(F("Error at address 0x"));
      if (address < 16)
        tinySerial.print("0");
      tinySerial.println(address, HEX);
    }
    else
    {
      tinySerial.print(F("."));
    }
  }
  if (nDevices == 0)
    tinySerial.println("No I2C devices found\n");
  else
    tinySerial.println("done\n");

  delay(1000);           // wait 3 seconds for next scan
}
