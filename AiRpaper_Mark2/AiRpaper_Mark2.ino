#include <USIWire.h>
#include <SoftwareSerial.h>
#include "si5351-avr-tiny-minimal.h"
#include <EEPROM.h>
#include "EEPROMAnything.h"

#define DEBUG_MODE // Uncomment to Debug

SoftwareSerial tinySerial(3, 4);

// Frequnecy Limits for tuning
uint32_t tuningfreq_minval = 500000UL;
uint32_t tuningfreq_maxval = 20000000UL;

//int encoderpinA = 3;//Pin for the Current Board
//int encoderpinB = 2;//Pin for the Current Board
//int encoderSWpin = 4;//Pin for the Current Board
//int encoderMultiplier = 5;//SET as Default
//int encoderStepSize = 6;//SET as Default
//int encoderInterval = 2000;//SET as Default
//long changeMultiplier = 50000;// Custom Configuration set to 10000 for fine tuning

struct config_t
{
  uint32_t xtal_f;
  uint32_t ppm_corr;
  uint32_t default_vfo_f;
  uint32_t default_bfo_f;
  uint32_t lo_f = 0;//default_vfo_f + default_bfo_f;
} configuration;


bool valueChanged = true;//setup to default freq
bool runOnce = true;//initalize the xtal freq, correction, power

int pref_xtal_f_eepromadr = 1;
int pref_ppm_corr_eepromadr = 2;
int pref_bfo_eepromadr = 3;
int pref_vfo_eepromadr = 4;


String cmdStr;
int opListSize = 6;
char* opList[] = {"SXF", "SCF",  "SVF", "SBF", "RLO", "ZZZ"};

void initDefaultParams() {
  configuration.xtal_f = 0UL;
  configuration.ppm_corr = 238000UL;//232000UL;
  configuration.default_vfo_f =   626500UL;
  configuration.default_bfo_f = 44992000UL;
  configuration.lo_f = configuration.default_vfo_f + configuration.default_bfo_f;
  savePrefValues();
}

long rotaryDelay = 5;
long printDelay = 100000;
long counterTime = 0;

void setup()
{
  tinySerial.begin(115200);
  tinySerial.println(F("AiRpaper Mark 2"));
  initDefaultParams();
  loadPrefValues();
  printFreqValues();

  si5351_init(SI5351_CRYSTAL_LOAD_8PF, 0);
  si5351_set_freq(configuration.default_bfo_f, SI5351_CLK2);
//  si5351_set_freq(53000000UL, SI5351_CLK1);
  si5351_set_freq(configuration.lo_f, SI5351_CLK0);
  si5351_drive_strength(SI5351_CLK0, SI5351_DRIVE_6MA);
//  si5351_drive_strength(SI5351_CLK1, SI5351_DRIVE_6MA);
  si5351_drive_strength(SI5351_CLK2, SI5351_DRIVE_6MA);
//  si5351_set_correction(-900);

}


void loop()
{
  //  if (valueChanged) {
  //    valueChanged = false;
  //    counterTime = 0;
  //    printFreqValues();
  //    if (runOnce) {
  //      runOnce = false;
  //      si5351.init(SI5351_CRYSTAL_LOAD_8PF, configuration.xtal_f, configuration.ppm_corr);
  //      si5351.drive_strength(SI5351_CLK2, SI5351_DRIVE_2MA);
  //      si5351.drive_strength(SI5351_CLK0, SI5351_DRIVE_2MA);
  //    }
  //    si5351.set_freq(configuration.default_bfo_f, SI5351_CLK2);
  //    si5351.set_freq(configuration.lo_f, SI5351_CLK0);
  //  }

  //  fetchSerialCommand();
  //  rotaryReadOut();
  //  regularPrint();
  //  checkandSaveBtn();
}




//void checkandSaveBtn() {
//  if (!digitalRead(encoderSWpin)) {
//    while (!digitalRead(encoderSWpin))delay(10);
//    savePrefValues();
//  }
//}
//

void fetchSerialCommand() {
  if (tinySerial.available() > 0) cmdStr.remove(0); // Remove previous commands
  while (tinySerial.available() > 0)
  {
    char carbyte = tinySerial.read();
    if (carbyte == ',')
      break;
    else
      cmdStr += carbyte;
    delayMicroseconds(80);//best delay b/w bytes
  }

  if (cmdStr.length() >= 3)
  {
    char tempBuf [64];

    switch (IsArrayContains(cmdStr.substring(0, 3), opList))
    {
      case 0  :
        cmdStr.substring(4).toCharArray(tempBuf, cmdStr.substring(4).length());
        configuration.xtal_f = strtoul(tempBuf, NULL, 0) ;
        savePrefValues();
        break;

      case 1  :
        cmdStr.substring(4).toCharArray(tempBuf, cmdStr.substring(4).length());
        configuration.ppm_corr = strtoul(tempBuf, NULL, 0);
        savePrefValues();
        break;

      case 2  :
        cmdStr.substring(4).toCharArray(tempBuf, cmdStr.substring(4).length());
        configuration.default_vfo_f = strtoul(tempBuf, NULL, 0) ;
        configuration.lo_f = configuration.default_vfo_f + configuration.default_bfo_f;
        savePrefValues();
        break;

      case 3  :
        cmdStr.substring(4).toCharArray(tempBuf, cmdStr.substring(4).length());
        configuration.default_bfo_f = strtoul(tempBuf, NULL, 0) ;
        configuration.lo_f = configuration.default_vfo_f + configuration.default_bfo_f;
        savePrefValues();
        break;

      case 4  :
        printFreqValues();
        break;

      case 5  :
        initDefaultParams();
        break;

      default :
        cmdStr.remove(0);
        return;
    }

    cmdStr.remove(0);
    valueChanged = true;
  }
}


void savePrefValues() {
  EEPROM_writeAnything(0, configuration);
}

void loadPrefValues() {
  EEPROM_readAnything(0, configuration);
}

void printFreqValues() {
#ifdef DEBUG_MODE
  tinySerial.print("Xtl:");
  tinySerial.println(uint32ToString(configuration.xtal_f));
  tinySerial.print("PPM:");
  tinySerial.println(uint32ToString(configuration.ppm_corr));
  tinySerial.print("VFO:");
  tinySerial.println(uint32ToString(configuration.default_vfo_f));
  tinySerial.print("BFO:");
  tinySerial.println(uint32ToString(configuration.default_bfo_f));
  tinySerial.print("LO:");
  tinySerial.println(uint32ToString(configuration.lo_f));

#endif
}

//void regularPrint() { // Print once every 50ms approx
//  if (counterTime <= 0 )
//  {
//    tinySerial.println(uint32ToString(configuration.default_vfo_f));
//    counterTime = printDelay / rotaryDelay;
//  }
//  counterTime--;
//}


int IsArrayContains(String searchValue, char* searchList[])
{
  searchValue.toUpperCase();
  for (int i = 0; i < opListSize ; i++)
    if (searchValue == searchList[i])
      return i;
  return -1;
}

//
//String uint64ToString(uint64_t input) {
//
//  String result = "";
//  uint8_t base = 10;
//  do {
//    char c = input % base;
//    input /= base;
//    if (c < 10)
//      c += '0';
//    else
//      c += 'A' - 10;
//    result = c + result;
//  } while (input);
//  return result;
//}


String uint32ToString(uint32_t input) {
  String result = "";
  uint8_t base = 10;
  do {
    char c = input % base;
    input /= base;
    if (c < 10)
      c += '0';
    else
      c += 'A' - 10;
    result = c + result;
  } while (input);
  return result;
}
